const { createLogger, transports, format } = require('winston');

const logger = createLogger({
    transports: [
        new transports.File({
            filename: 'logfile.log'
        }),
        new transports.Console()
    ]
})

module.exports = logger;