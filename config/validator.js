const { check, validationResult, customValidators } = require('express-validator');

const myValidationResult = validationResult.withDefaults({
    formatter: (error) => {
        return {
            message: error.msg,
        };
    }
});

module.exports = function (req, res, next) {
    const errors = validationResult(req);
    // console.log(errors.result.errors)
    if (!errors.isEmpty()) {
        // var resultx = getFields(errors.array({ onlyFirstError: true }), "message");
        //  console.log(resultx)
        return res.status(400).json({ error: "bad_request", error_description: errors.array({ onlyFirstError: true }).map(i => ` ${i.msg}`).join(', ') })
    } else {
        return next()
    }
}