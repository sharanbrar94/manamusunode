//const mysql = require('mysql')
const mysql = require('mysql2')
const logger = require('./logger')
const toSendmail = require('./nodemailer');
const util = require('util');

function handleDisconnect() {
  connection = mysql.createPool({
    connectionLimit: 10,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT
  });

  //  connection.query = util.promisify(connection.query).bind(connection);

  connection.getConnection((err, connect) => {
    if (err) {
      logger.info(err)
      // toSendmail(err.message);
      setTimeout(handleDisconnect, 2000);
    }
    else {
      logger.info('Connected with db..!')
      connection.query = util.promisify(connection.query).bind(connection);

    }


  });

  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    logger.error('db error', err);
    // Connection to the MySQL server is usually lost due to either server restart, or a
    // connnection idle timeout (the wait_timeout server variable configures this)
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      logger.debug("database connection restarting");
      handleDisconnect();
    } else if (err.code === 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR') {
      logger.debug("database connection restarting");
      handleDisconnect();
    } else {
      throw err;
    }
  });
}

handleDisconnect();



