require("dotenv").config();
var createError = require("http-errors");
var socket = require('socket.io')
const morgan = require("morgan")
//for cross-origin-resource-sharing
var cors = require("cors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
const compression = require("compression"); ////  for gzip compression

//var logger = require('morgan');

var indexRouter = require("./routes/index");

const logger = require("./config/logger"); // importiing winston logger module
const swaggerUi = require("swagger-ui-express");

var app = express();
const toSendmail = require("./config/nodemailer");

require("./config/connection"); //importing db connection module.....

const loginRoutes = require("./modules/login/login.routes");
const teacherRoutes = require("./modules/teacher/teacher.routes");
const studentRoutes = require("./modules/student/student.routes");
const chatRoutes = require("./modules/chat/chat.routes");
const uploadRoutes = require("./modules/upload/upload.routes");
const adminRoutes = require("./modules/admin/admin.routes");
const notitficationRoutes = require("./modules/notification/notification.routes");
//using swagger.json file

var swaggermerge = require("swagger-merge");
const loginSwagger = require("./modules/login/login.swagger.json");
const chatSwagger = require("./modules/chat/chat.swagger.json");
const teacherSwagger = require("./modules/teacher/teacher.swagger.json");
const notificationSwagger = require("./modules/notification/notification.swagger.json");
const studentSwagger = require("./modules/student/student.swagger.json");
const adminSwagger = require("./modules/admin/admin.swagger.json");
const uploadSwagger = require("./modules/upload/upload.swagger.json");

var info = {
  version: "0.0.1",
  title: "ManaMusu",
  description: "ManaMusu Api's",
};

if (process.env.SSL == "true") {
  var scheme = ["https"];
} else {
  var scheme = ["http"];
}

console.log(process.env.API_BASE_PATH);
merged = swaggermerge.merge(
  [
    loginSwagger,
    chatSwagger,
    teacherSwagger,
    notificationSwagger,
    studentSwagger,
    adminSwagger,
    uploadSwagger,
    notitficationRoutes
  ],
  info,
  "/api",
  process.env.DOMAIN + ":" + process.env.APP_PORT,
  scheme
);
app.use(morgan('tiny'))
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(merged));

//app.use("/api-docs", swaggerUi.serveFiles(swaggerBuild1, option1));

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

//app.use(logger('dev'));

app.use(compression());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "db")));
app.use(express.static(path.join(__dirname, "public")));



app.use(
  "/api",
  loginRoutes,
  teacherRoutes,
  studentRoutes,
  chatRoutes,
  uploadRoutes,
  adminRoutes,
  notitficationRoutes
);
app.use("/api/admin", adminRoutes);

app.use("/", indexRouter);

app.use("/api", loginRoutes);
app.use("/api", studentRoutes);
app.use("/api", teacherRoutes);
app.use("/api", uploadRoutes);
app.use("/api", chatRoutes);
app.use("/api", notitficationRoutes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500).send({
    status: err.status || 500,
    message: err.message || "Internal Server Error",
  });
});

process.on("uncaughtException", (ex) => {
  logger.error(`message - ${ex.message}, stack trace - ${ex.stack}`);
  //toSendmail(ex)
  //process.exit(1);
});

// app.get("/:slug/teacher/", (req, res) => {
//   console.log("t");
//   app.render(req, res, "/teacher");
// });

// process.on('exit', function(code) {
//   return logger.info(`About to exit with code ${code}`);
// });

module.exports = app;
//throw new Error('something failed')
