-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: sharednodejs
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `phone_number` bigint(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `country_code` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_us_ibfk_1` (`user_id`),
  CONSTRAINT `contact_us_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_us`
--

LOCK TABLES `contact_us` WRITE;
/*!40000 ALTER TABLE `contact_us` DISABLE KEYS */;
INSERT INTO `contact_us` VALUES (1,16,'hello',NULL,NULL,NULL,''),(2,16,'hello',NULL,NULL,NULL,''),(3,48,'string',NULL,NULL,NULL,''),(4,48,'string',12345678,'string','string','+91');
/*!40000 ALTER TABLE `contact_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` varchar(255) NOT NULL,
  `storage_type` tinyint(4) NOT NULL COMMENT '1:DO, 2: AWS, 3:Azure, 4:GC, 5:Local',
  `environment` tinyint(4) NOT NULL COMMENT '1:live, 2:test, 3:dev, 4:local',
  `is_default_asset` tinyint(4) NOT NULL COMMENT '0:yes, 1:no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES ('1599909574248-background-ppt-128984-4971491.jpg',5,4,1,'2020-09-12 11:19:34','2020-09-12 11:19:34'),('1599909599046-background-ppt-128984-4971491.jpg',5,4,1,'2020-09-12 11:19:59','2020-09-12 11:19:59');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_images`
--

DROP TABLE IF EXISTS `project_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `project_images_ibfk_1` (`project_id`),
  CONSTRAINT `project_images_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_images`
--

LOCK TABLES `project_images` WRITE;
/*!40000 ALTER TABLE `project_images` DISABLE KEYS */;
INSERT INTO `project_images` VALUES (25,21,'string','2020-09-19 17:08:18','2020-09-19 17:08:18'),(26,20,'string','2020-10-15 11:07:43','2020-10-15 11:07:43');
/*!40000 ALTER TABLE `project_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `price` decimal(10,2) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0 - pending, 1 - completed, 2 - rejected ',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (20,16,'string','string',10.00,'string','2020-09-11 13:51:02',NULL,1),(21,16,'string','string',20000.00,'string','2020-09-11 13:53:18',NULL,0),(22,16,'samebeef 1','song',10000000.00,'moosewala 1 1','2020-09-11 13:55:19',NULL,2);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `static_pages`
--

DROP TABLE IF EXISTS `static_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static_pages`
--

LOCK TABLES `static_pages` WRITE;
/*!40000 ALTER TABLE `static_pages` DISABLE KEYS */;
INSERT INTO `static_pages` VALUES (1,'About','abc.jpg','The main purpose of your About Us page is to give your visitors a glimpse into who you are as a person or a business (or sometimes both).\r\n'),(2,'Home','pqr.jpg','The main purpose of your About Us page is to give your visitors a glimpse into who you are as a person or a business (or sometimes both).\r\n'),(3,'string',NULL,'string');
/*!40000 ALTER TABLE `static_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_logins`
--

DROP TABLE IF EXISTS `user_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `fcm_id` text,
  `device_id` varchar(250) NOT NULL,
  `expired_at` date DEFAULT NULL,
  `device_type` tinyint(4) NOT NULL COMMENT '1-Android, 2-ios',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_id` (`device_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_logins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_logins`
--

LOCK TABLES `user_logins` WRITE;
/*!40000 ALTER TABLE `user_logins` DISABLE KEYS */;
INSERT INTO `user_logins` VALUES (10,48,'string','string',NULL,1,'2020-11-03 06:00:22');
/*!40000 ALTER TABLE `user_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `profile_image` varchar(250) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `fb_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `apple_id` varchar(255) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `country_code` varchar(250) DEFAULT NULL,
  `country_abbr` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `address` text,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `reset_pass_token` varchar(100) DEFAULT NULL,
  `email_verify_token` varchar(100) DEFAULT NULL,
  `is_email_verified` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1:yes, 2:no',
  `phone_verify_code` varchar(35) DEFAULT NULL,
  `is_phone_verified` tinyint(4) NOT NULL DEFAULT '2',
  `is_user_verified` tinyint(4) NOT NULL DEFAULT '2',
  `is_deactivated` int(11) NOT NULL DEFAULT '2' COMMENT '1:yes, 2:no',
  `is_blocked` tinyint(4) DEFAULT '2' COMMENT '1:yes,2:no',
  `deactivate_reason` text,
  `is_profile_completed` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1:yes, 2:no',
  `description` varchar(10000) DEFAULT NULL,
  `user_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:user, 2:admin, 3:staff',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (8,'string','string','string','muskan@gmail.com',NULL,NULL,NULL,92386554,NULL,NULL,NULL,NULL,NULL,NULL,'$2b$10$RbYJ733BPrBsw8gf8wa8BedGBcz7TThFWcWRu8MvROc7XwLPLzB7e','string',NULL,NULL,NULL,'1597747532707',NULL,2,NULL,2,2,2,NULL,NULL,2,NULL,2,'2020-08-13 13:38:02','2020-09-18 18:28:30'),(9,'Shared1',NULL,NULL,'shared1@gmail.com',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'$2b$10$4hbiE24ay28l3GnphUO.yOqs.HQwiB0Qk9UtpmzCTAfPiXiw3OjFG','string',NULL,NULL,NULL,NULL,NULL,2,NULL,2,2,2,NULL,NULL,2,NULL,1,'2020-08-18 10:01:04','2020-08-18 10:01:04'),(10,'string',NULL,NULL,'string12@gmail.com','string',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'string@12',NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,2,2,2,NULL,NULL,2,NULL,1,'2020-08-21 05:36:42','2020-10-28 05:23:53'),(11,'string',NULL,NULL,'string123@gmail.com','string12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,2,2,2,2,NULL,2,NULL,1,'2020-08-21 05:57:12','2020-10-20 12:49:47'),(12,'string',NULL,NULL,'string124@gmail.com','string122',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,2,2,2,NULL,NULL,2,NULL,1,'2020-08-21 06:15:38','2020-08-21 06:15:38'),(15,'string',NULL,NULL,'string1233@gmail.com',NULL,NULL,NULL,9287348,NULL,NULL,NULL,NULL,NULL,NULL,'$2b$10$.KEB3OIKvLZwaz1VsDaXZehg/s/Y2xNuaSwfxdPzqhPSDbmJolzWa','string',NULL,NULL,NULL,NULL,'1597990827210',2,NULL,2,2,2,NULL,NULL,2,NULL,1,'2020-08-21 06:20:27','2020-08-21 06:20:27'),(16,'Jeevanjot','singh','nhi hai','jivanjot1001@gmail.com',NULL,NULL,NULL,9138448778,'91',NULL,'ontario','Canada','Canada',134003,'$2b$10$lQ7607G6iCQMMQmyO4rEF.Rr/f0U6uC/V38IS5xElAC9zSrwuxdTa','Kaziwara',23,22,NULL,NULL,'1599563876121',2,NULL,2,2,2,2,NULL,2,NULL,1,'2020-09-08 11:17:56','2020-09-08 11:17:56'),(17,'string','string234','string','admin@gmail.com',NULL,NULL,NULL,9138448778,'91',NULL,'Ambala','Haryana','India',134003,'$2b$10$w.T7I8v80a9iSa010OCeLu5FTflWYPxbErqDCejimxyVhfARoScWq','Kaziwara, Ambala city',88,77,NULL,NULL,NULL,2,NULL,2,2,2,2,NULL,2,NULL,2,'2020-09-16 06:01:34','2020-10-27 09:18:50'),(20,'jj','singh','string','jivanjot1001+2@gmail.com',NULL,NULL,NULL,8929729459,'91',NULL,'string','string','string',NULL,'$2b$10$YdafrYNBmTb/OgnZBa24ke8Srjb6trWGnZ73DYIVKL2AqzfXN/u/K','ambala',NULL,NULL,NULL,NULL,NULL,2,NULL,2,2,2,2,NULL,2,NULL,3,'2020-09-18 07:36:54','2020-09-18 07:36:54'),(21,'jj','singh','string','jivanjot1001+3@gmail.com',NULL,NULL,NULL,8929729459,'91',NULL,'string','string','string',NULL,'$2b$10$PhRBK7v7lfjQ5wKVnHRIkupIHTEI7IkbDoFfoXZ4PzsL85k5Co4mi','ambala',NULL,NULL,NULL,NULL,NULL,2,NULL,2,2,2,2,NULL,2,NULL,3,'2020-09-18 07:42:11','2020-09-18 07:42:11'),(22,'string','string','string','string@gmail.com',NULL,NULL,NULL,0,'string',NULL,'string','string','string',NULL,'$2b$10$RZaNwl.bw7EsdP0y.t7xa.b6x8wIS8zsCqZtgWn/cWdkdotKgT/7C','string',NULL,NULL,NULL,NULL,NULL,2,NULL,2,2,2,2,NULL,2,NULL,3,'2020-10-15 11:09:44','2020-10-15 11:09:44'),(23,'string','string','string','princebhatia15@gmail.com',NULL,NULL,NULL,0,'string',NULL,'string','string','string',NULL,'$2b$10$avc576DPJNxeK4uML2TtTOSfx76HVvbJIyuU69Zag8dgju.DiZ3wG','string',NULL,NULL,NULL,NULL,NULL,2,NULL,2,2,2,2,NULL,2,NULL,3,'2020-10-15 11:16:45','2020-10-15 12:55:42'),(24,'string','string',NULL,'prince.bhatia.henceforth+3@gmail.com',NULL,NULL,NULL,1234567890,NULL,NULL,NULL,NULL,NULL,NULL,'$2b$10$8j46paUsrJwct/7R6ZCfruuDtNiQkITW4CWGzsoOpayLThaNZ7edW',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,2,2,2,2,NULL,2,NULL,1,'2020-11-02 07:38:03','2020-11-03 07:38:25'),(25,'string','string',NULL,'prince.bhatia.henceforth+4@gmail.com',NULL,NULL,NULL,1234567898,NULL,NULL,NULL,NULL,NULL,NULL,'$2b$10$zZa72Nt5gG8PpEtssTFfAutix3He0W/QA7yfuUD4AMnbPkBT7D3lK',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,2,2,2,2,NULL,2,NULL,1,'2020-11-02 07:50:54','2020-11-03 07:39:50'),(26,'string','string',NULL,'prince.bhatia.henceforth+5@gmail.com',NULL,NULL,NULL,8920039612,NULL,NULL,NULL,NULL,NULL,NULL,'$2b$10$TQrtp/JR94TEl3Mt895UzuLK9KCVGmIBa8YisEwvEZ2S6Lv9OQhFa',NULL,NULL,NULL,NULL,NULL,'1604381790209',2,'371155',2,2,2,2,NULL,2,NULL,1,'2020-11-03 05:36:30','2020-11-03 05:36:30'),(27,'string','string',NULL,'prince.bhatia.henceforth+6@gmail.com',NULL,NULL,NULL,8920039612,NULL,NULL,NULL,NULL,NULL,NULL,'$2b$10$Hg/Ff6TBaJSIvsVglrusm.BfMVcNXo4z0BtweXYn8ALg6fMUOEpDy',NULL,NULL,NULL,NULL,NULL,'1604381923540',2,'472261',2,2,2,2,NULL,2,NULL,1,'2020-11-03 05:38:43','2020-11-03 05:38:43'),(30,'string','string',NULL,'string+15@gmail.com',NULL,NULL,NULL,918920039612,NULL,NULL,NULL,NULL,NULL,NULL,'$2b$10$T6bIgab3W6XFc41J4VqmyuI.eM/fu1VkBsCG8yOdkraTpyQP.SNka',NULL,NULL,NULL,NULL,NULL,'1604383221995',2,NULL,1,2,2,2,NULL,2,NULL,1,'2020-11-03 06:00:22','2020-11-03 06:00:56'),(31,'prince','bhatia',NULL,'prince@gmail.com',NULL,NULL,NULL,8920039612,NULL,NULL,NULL,NULL,NULL,NULL,'$2b$10$B.XSC9pVPhWV2EyqJM0fouV0IifRnIxIpFjrVdnOiKWHOxfFEyonm',NULL,NULL,NULL,NULL,NULL,'1604575031915',2,'195465',2,1,2,2,NULL,2,NULL,1,'2020-11-05 11:17:12','2020-11-05 11:47:29'),(32,'prince','bhatia',NULL,'princebh@gmail.com',NULL,NULL,NULL,8920039612,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$gO6e4qJ0u3SP45Sn./uTJOvvVspLnK5i9BvVzZWxI5kTYUHdZEs8y',NULL,NULL,NULL,NULL,NULL,'1604575334355',2,'722529',2,2,2,2,NULL,2,NULL,2,'2020-11-05 11:22:14','2020-11-05 11:42:58'),(33,'string','string',NULL,'prince.bhatia.henceforth+20@gmail.com',NULL,NULL,NULL,123456789,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$dngumDIhjRqvq/TXs5r.6OBunUfsEjsVAomADCF2uWc2Qp1PSMNNO',NULL,NULL,NULL,NULL,NULL,'I9EnmyFN2kEsXtj',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 07:57:48','2020-11-07 06:25:28'),(34,'string','string',NULL,'prince.bhatia.henceforth+21@gmail.com',NULL,NULL,NULL,1234567898,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$meqSk/2IXjKu7bmPPz3iouDLu4pZaaErNSpQ4vRdy7TE0UdtI1r5O',NULL,NULL,NULL,NULL,NULL,'awjlQwh8SndArSL',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 07:59:20','2020-11-06 07:59:20'),(35,'string','string',NULL,'prince.bhatia.henceforth+22@gmail.com',NULL,NULL,NULL,12345678,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$C56L3uoteVyeDln56JMvbuvl3yC6Yb7.nNiHPTc74T.Jw8oxihMEm',NULL,NULL,NULL,NULL,NULL,'ZMrCPWbcpKktGKG',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 08:00:08','2020-11-06 08:00:08'),(36,'string','string',NULL,'prince.bhatia.henceforth+23@gmail.com',NULL,NULL,NULL,123456728,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$e71R7EwaUHrrybr8ez8sFOkMqwezHG3tixRuvPLVTj6yT5b6uu5iO',NULL,NULL,NULL,NULL,NULL,'wfsSLAad5heYGe2',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 08:01:27','2020-11-06 08:01:27'),(37,'string','string',NULL,'prince.bhatia.henceforth+24@gmail.com',NULL,NULL,NULL,1234567282,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$8ss3nbFtFZ48vp0W7WKXTOXhMvlPSs4OCi.vfe5xjIs5PRT2cR1oa',NULL,NULL,NULL,NULL,NULL,'l0MseO2RhysD693',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 08:03:37','2020-11-06 08:03:37'),(38,'string','string',NULL,'prince.bhatia.henceforth+25@gmail.com',NULL,NULL,NULL,123456327282,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$ETzDLbxdjzO874pxmwvcXu7bnh6Dzii1///4tf1p9hX5VZqRsPwmS',NULL,NULL,NULL,NULL,NULL,'KseYgXRHqaprQfp',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 09:09:58','2020-11-06 09:09:58'),(39,'string','string',NULL,'prince.bhatia.henceforth+26@gmail.com',NULL,NULL,NULL,12345632722,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$dQqJgCG3963h/C92QqRGsO/ov1mJQQRRbV3lSGXK7UhyzGZxv/YzO',NULL,NULL,NULL,NULL,NULL,'48uKFhklwjyf2As',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 09:10:48','2020-11-06 09:10:48'),(40,'string','string',NULL,'prince.bhatia.henceforth+27@gmail.com',NULL,NULL,NULL,123456322,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$m80OGy.DZqziC5D4YaxA4e3zGgtkJHyUGgvx0lG0m3QMxafMtDcYm',NULL,NULL,NULL,NULL,NULL,'4JPgXTRD2qcwXwL',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 09:15:46','2020-11-06 09:15:46'),(41,'string','string',NULL,'prince.bhatia.henceforth+28@gmail.com',NULL,NULL,NULL,123456222,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$3.JS5GDh3OtRc5J5NplF2u/.zDtfd.yUmTOCJtAGPDfpdJxGlC1Z2',NULL,NULL,NULL,NULL,NULL,'wb5WlXRZWAHBup1',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 09:25:31','2020-11-06 09:25:31'),(42,'string','string',NULL,'prince.bhatia.henceforth+29@gmail.com',NULL,NULL,NULL,12345622,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$HQtKmFpXW/Qp2YJe4URTkOGhPgPadQbDzTdubFmIeuOU1ehvrfbBa',NULL,NULL,NULL,NULL,NULL,'sCjlv8ytLafW4ZM',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 09:42:31','2020-11-06 09:42:31'),(43,'string','string',NULL,'prince.bhatia.henceforth+30@gmail.com',NULL,NULL,NULL,123452622,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$OqUZhWXTlHqYNoUVhVDtwegCWwwoMLq3a.3mXcwR3T0N1AN0jIcp6',NULL,NULL,NULL,NULL,NULL,'BpUeCDXLkYAchvf',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 09:55:14','2020-11-06 09:55:14'),(44,'string','string',NULL,'prince.bhatia.henceforth+31@gmail.com',NULL,NULL,NULL,1234526322,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$KROsONrmumupb29mOdxyPuMdOIOIEjWLLjK07t9biiKkbx5st4dkW',NULL,NULL,NULL,NULL,NULL,'LcK46ClsRuE255G',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 09:56:11','2020-11-06 09:56:11'),(45,'string','string',NULL,'prince.bhatia.henceforth+32@gmail.com',NULL,NULL,NULL,12345263223,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$7hnrWHWWZh2madmaq0xN9eezkAZ0DU27A6kZsEVvHWknJQ6Ipx4Su',NULL,NULL,NULL,NULL,NULL,'0Ed4mWJ89cjZVlV',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 09:58:41','2020-11-06 09:58:41'),(46,'string','string',NULL,'prince.bhatia.henceforth+33@gmail.com',NULL,NULL,NULL,12345263221,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$N5.C/C2R1I7ILYxkQoizIej0xR/n7d6m9TCYQRC8pqOxzAV6VySti',NULL,NULL,NULL,NULL,NULL,'WkxmlSb5pGINHfF',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 10:00:04','2020-11-06 10:00:04'),(47,'string','string',NULL,'prince.bhatia.henceforth+34@gmail.com',NULL,NULL,NULL,1234526321,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$OPBHkAqqFHNpIbQi2IwlVeRtlXv7r8Orkbi.bxdZ9RyjtvS9djAn6',NULL,NULL,NULL,NULL,NULL,'mpXSbNXEAWeF11f',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 10:00:40','2020-11-06 10:00:40'),(48,'string','string',NULL,'prince.bhatia.henceforth+35@gmail.com',NULL,NULL,NULL,12345263,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$UkjvwK.0vbz8EHDvKBKRKeEINlVG6sEL8olITAU3ypAV95bxWBMti',NULL,NULL,NULL,NULL,NULL,'LoHnkOcpfXGGIGP',2,NULL,1,2,2,2,NULL,2,NULL,1,'2020-11-06 10:02:16','2020-11-07 06:35:12'),(49,'string','string',NULL,'prince.bhatia.henceforth+36@gmail.com',NULL,NULL,NULL,1234526311,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$Q0o9suwYhrgiZBw6l3bHOOYE.W4pvHeLvje38ctrsW/lH/U.Rapky',NULL,NULL,NULL,NULL,NULL,'LhMOoHSpeNqCcM5',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 10:02:46','2020-11-06 10:02:46'),(50,'string','string',NULL,'prince.bhatia.henceforth+37@gmail.com',NULL,NULL,NULL,1234526312,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$W816Lj2sQ77P8yM3s1m73esppNWFL05I4g91MAFPaFSD7yd379hfy',NULL,NULL,NULL,NULL,NULL,'UoOWEBGVHPQNnRy',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 10:03:12','2020-11-06 10:03:12'),(51,'string','string',NULL,'prince.bhatia.henceforth+38@gmail.com',NULL,NULL,NULL,12345263123,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$qH3qutFhiuDgRkvJEBoMlOnmgX1SvrjZqQZB1uaphzeXLUoTiQFJi',NULL,NULL,NULL,NULL,NULL,'BlvZt2tymlccqSt',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 10:17:14','2020-11-06 10:17:14'),(52,'string','string',NULL,'prince.bhatia.henceforth+39@gmail.com',NULL,NULL,NULL,12345263121,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$VQZYapFs8bT3gLG.70pcj.ha.g918YnRlPQfvvTLOd69dTmOA6cVe',NULL,NULL,NULL,NULL,NULL,'riZN1WH5hHqjSU3',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 10:18:01','2020-11-06 10:18:01'),(53,'string','string',NULL,'prince.bhatia.henceforth+40@gmail.com',NULL,NULL,NULL,123452631291,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$M/.XV8iEQORqPEeLxe88VOPdzBuyeQRv2eSzlVD1Rtmp.4Jt474qG',NULL,NULL,NULL,NULL,NULL,'uv9fDhbbti5WZBa',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 10:19:34','2020-11-06 10:19:34'),(54,'string','string',NULL,'prince.bhatia.henceforth+41@gmail.com',NULL,NULL,NULL,123452631298,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$4LwXV3rvq36jiG8epSA67OemAIRlI6PONv4pDv67FEeY56I6W.cU.',NULL,NULL,NULL,NULL,NULL,'1wTE9y59EiarQeM',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 10:20:47','2020-11-06 10:20:47'),(55,'string','string',NULL,'prince.bhatia.henceforth+42@gmail.com',NULL,NULL,NULL,579867877,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$wVGOtPXHFxMoDRnjDMIbVOmf2SFmnZerRn1BpzbEdcLQXBfTaWtHu',NULL,NULL,NULL,NULL,NULL,'lhacB54k60woNLH',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:04:56','2020-11-06 11:04:56'),(56,'string','string',NULL,'prince.bhatia.henceforth+45@gmail.com',NULL,NULL,NULL,5779867877,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$HRo13KUBSZo6FKLeRo/G4OD3/JbYd/OFTTkdphv2jCEzbGieAcRPK',NULL,NULL,NULL,NULL,NULL,'6AloFKVCLyLY5u1',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:08:47','2020-11-06 11:08:47'),(57,'string','string',NULL,'prince.bhatia.henceforth+46@gmail.com',NULL,NULL,NULL,57798677877,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$2.r.mcUNbIss3kgfw5iPIe0Nvkmtnj3Ct3YhR/6beCICA8/iHsUc.',NULL,NULL,NULL,NULL,NULL,'6aDOSvkavB0iuNT',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:09:36','2020-11-06 11:09:36'),(58,'string','string',NULL,'prince.bhatia.henceforth+47@gmail.com',NULL,NULL,NULL,577986777,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$TFUHevDwyF4V.3iiNHpwgOeltBNabrlqnVq6HE5JZbHhcRGUG5e02',NULL,NULL,NULL,NULL,NULL,'cEmWF1Uzh7HmCmi',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:10:12','2020-11-06 11:10:12'),(59,'string','string',NULL,'prince.bhatia.henceforth+48@gmail.com',NULL,NULL,NULL,5779867779,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$.teQQwE.7r8tgXzJm8NJCeou10jLYllToj2k6qdpamPRrpEIHAnGy',NULL,NULL,NULL,NULL,NULL,'UFSs3GqQbLiLYpQ',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:22:25','2020-11-06 11:22:25'),(60,'string','string',NULL,'prince.bhatia.henceforth+49@gmail.com',NULL,NULL,NULL,57798678779,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$/OE0.0KuIM2o79zv0kmlweStL9rn.nat2rWlW3IYIwLG4OAfcxuIm',NULL,NULL,NULL,NULL,NULL,'HMYVc4EQdpk3mLc',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:22:47','2020-11-06 11:22:47'),(61,'string','string',NULL,'prince.bhatia.henceforth+50@gmail.com',NULL,NULL,NULL,577986787795,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$ftR3eQ9MgIgTutLxaD2IgeMnERXP1tqSRwKnjMZh5uGrlSfq.YL82',NULL,NULL,NULL,NULL,NULL,'dbmYoOLFZusAYJ8',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:23:30','2020-11-06 11:23:30'),(62,'string','string',NULL,'prince.bhatia.henceforth+51@gmail.com',NULL,NULL,NULL,57798678795,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$YekIBh5Aayrx/StgUrBlcu3l55061zW74MUEylhJrSEfIESD9TsK.',NULL,NULL,NULL,NULL,NULL,'oHsyfBdBZrmocq6',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:24:23','2020-11-06 11:24:23'),(63,'string','string',NULL,'prince.bhatia.henceforth+52@gmail.com',NULL,NULL,NULL,577986787915,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$t4K9q.3ZPIra8dfFyxKnkuJt1CgearZpd6A.zGCAtMnEuRAzyWgSu',NULL,NULL,NULL,NULL,NULL,'xf6PCqSdUbjrzIz',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:25:41','2020-11-06 11:25:41'),(64,'string','string',NULL,'prince.bhatia.henceforth+53@gmail.com',NULL,NULL,NULL,57798787915,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$Ze4LWNpyAxCvbVwGtC7iBO6ZA65WzbsmvA0rKyN2zxfskNHmP2m5u',NULL,NULL,NULL,NULL,NULL,'1iAmlWoYIL6Sudf',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:26:40','2020-11-06 11:26:40'),(65,'string','string',NULL,'prince.bhatia.henceforth+54@gmail.com',NULL,NULL,NULL,5779878795,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$FLnuxg1w/4o0gqoiPGeOTukrIStliQE9w2HPuctiACAJHdaSxKXvW',NULL,NULL,NULL,NULL,NULL,'3ILagf2ICLtm9BG',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:27:20','2020-11-06 11:27:20'),(66,'string','string',NULL,'prince.bhatia.henceforth+55@gmail.com',NULL,NULL,NULL,579878795,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$5eBwKy6Cn9rjCjozG6ZxP.G/uUXNHDwaoUOLBNUBgQ6/SLjq9TApu',NULL,NULL,NULL,NULL,NULL,'03HMwpVjqTocJ8p',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:28:25','2020-11-06 11:28:25'),(67,'string','string',NULL,'prince.bhatia.henceforth+56@gmail.com',NULL,NULL,NULL,57878795,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$ZCmxJmUvipCqwloxzhEXJur1PzNtWLY8quQM87.3jsQ1ZPagsywfq',NULL,NULL,NULL,NULL,NULL,'KyiBphbCBSk5iWp',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:30:05','2020-11-06 11:30:05'),(68,'string','string',NULL,'prince.bhatia.henceforth+57@gmail.com',NULL,NULL,NULL,5787879,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$gVn6t1WLaFa82MTwV2g1UOBGNkAclravBm/q5sRPz5zLuljhU8cQy',NULL,NULL,NULL,NULL,NULL,'4PJn8Gb0WU2c1uq',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:30:58','2020-11-06 11:30:58'),(69,'string','string',NULL,'prince.bhatia.henceforth+58@gmail.com',NULL,NULL,NULL,57878479,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$RhDZYISstbx4ZcQhE7kvUOhk.KK8LyDIXZMQVDlNOBtTVh2Fn7zSa',NULL,NULL,NULL,NULL,NULL,'D8yUwaD6457tRsS',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:46:54','2020-11-06 11:46:54'),(70,'string','string',NULL,'prince.bhatia.henceforth+59@gmail.com',NULL,NULL,NULL,578784791,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$wggjuIrS0ImKDa9xF5eVAe.rIlHh38Gk5/NEGhI7QhtmSgoXDgVEC',NULL,NULL,NULL,NULL,NULL,'e7ZGx5zdAeEJC6Y',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:47:40','2020-11-06 11:47:40'),(71,'string','string',NULL,'prince.bhatia.henceforth+60@gmail.com',NULL,NULL,NULL,5787847911,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$3GJQByyE2Rv2KC3o1SO2pOWAF6qh78ESfHuKfR5Kvc4z2iaM7zlwG',NULL,NULL,NULL,NULL,NULL,'MD9PkqCD0XqyQau',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:48:36','2020-11-06 11:48:36'),(72,'string','string',NULL,'prince.bhatia.henceforth+61@gmail.com',NULL,NULL,NULL,577847911,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$mmAMaQbS2YvyGG4IUDEzCuUD4eyDMMNDqvo40nF4aqVuyB8MUMNia',NULL,NULL,NULL,NULL,NULL,'L3RScLCY35xVtCQ',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 11:56:43','2020-11-06 11:56:43'),(73,'string','string',NULL,'prince.bhatia.henceforth+62@gmail.com',NULL,NULL,NULL,57847911,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$nauWOY5SGqr8oRLq9Dor4.2eavTTqbQIvJmUJVObZfoRD5LVCAYd2',NULL,NULL,NULL,NULL,NULL,'9aqErv9RE4gr9J1',2,'111111',2,2,2,2,NULL,2,NULL,1,'2020-11-06 12:00:26','2020-11-06 12:00:26'),(74,'string','string',NULL,'prince.bhatia.henceforth+63@gmail.com',NULL,NULL,NULL,7847911,'+91',NULL,NULL,NULL,NULL,NULL,'$2b$10$fbMKR0Vofp77s4b9UeDrDOfPEFN4klOA4hI5CcLmLJ5DP9AvLx/Rm',NULL,NULL,NULL,NULL,NULL,'afomfHDOkiEJhxN',2,NULL,1,2,2,2,NULL,2,NULL,1,'2020-11-06 12:02:50','2020-11-06 12:25:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-07 15:29:48
