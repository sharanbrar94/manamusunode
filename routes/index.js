var express = require('express');
var router = express.Router();

//////  express validator
const { checkSchema } = require('express-validator');
const validator = require('../config/validator');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
