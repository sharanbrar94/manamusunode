
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {first_name: 'rowValue1',last_name: 'rowValue1'},
        { first_name: 'rowValue2',last_name: 'rowValue1'},
        { first_name: 'rowValue3',last_name: 'rowValue1'}
      ]);
    });
};
