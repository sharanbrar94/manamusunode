#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require("./app");
var fs = require("fs");
var debug = require("debug")("sharedmodulesnodejs:server");
var http = require("http");
var https = require("https");
const logger = require("./config/logger");
const listEndpointsExpress = require("list-endpoints-express");
/**
 * Get ports from environment and store in Express.
 */

var port = normalizePort(process.env.APP_PORT);

app.set("port", port);

/**
 * Create HTTPS & HTTP servers.
 */

/**
 * Listen on provided port, on all network interfaces.
 */
if (process.env.SSL == "true") {
  var server = https.createServer(
    {
      key: fs.readFileSync(process.env.SSL_KEY),
      cert: fs.readFileSync(process.env.SSL_CERT),
    },
    app
  );
} else {
  var server = http.createServer(app);
}

// server.listen(port);
// server.on("error", onError);
// server.on("listening", onListening);

//const socket = require("socket.io");
// //const io = require('socket.io')(port);
//const io = socket.listen(port);
server.listen(port);
var io = require('socket.io')(server);
require("./modules/chat/socket/index")(io);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP & HTTPS servers "error" event.
 */

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP & HTTPS server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  debug("Listening on " + bind);
  logger.info(`server listening on port ${port}`);
}

module.exports = io;
process.on('SIGTERM', () => {
  logger.info('SIGTERM signal received.');
  logger.info('Closing http server.');
  server.close(() => {
    logger.info('Http server closed.');
    connection.end(function (err) {
      if (err) {
        return logger.error('error:' + err.message);
      }
      logger.info('Close the database connection.');
      process.exit(0)

    });

  });
});

process.on('SIGINT', () => {
  logger.info('SIGINT signal received.');
  logger.info('Closing http server.');
  server.close(() => {
    logger.info('Http server closed.');

    connection.end(function (err) {
      if (err) {
        return logger.error('error:' + err.message);
      }
      logger.info('Close the database connection.');
      process.exit(0)
    });
  });
});

process.on('message', function (message) {
  logger.info('Hey you sent ' + message);
  process.send('Hey you sent ' + message);
});


