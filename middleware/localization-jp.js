class LocalizationJp {
    static async lang() {
        var langMsg = {}
        langMsg['request_created'] = '作成されました'
        langMsg['request_updated'] = '更新されました'
        langMsg['request_deleted'] = '削除されました'
        langMsg['delete_booking'] = '削除しました'
        langMsg['response_declined'] = '授業が確定されませんでした'
        langMsg['response_accepted'] = '授業が確定されました'
        langMsg['booking_successfully'] = '応募が完了しました'
        langMsg['request_send_successfully'] = '応募しました'
        langMsg['class_cancelled_successfully'] = '授業がキャンセルされました'
        langMsg['hourly_rate_changed'] = '時給が変更されました'
        langMsg['error_changing_hourly_rate'] = 'エラー：時給変更に失敗しました'
        langMsg['invalid_amount'] = '正しい金額を入力してください'
        langMsg['payment_done'] = '支払いが完了されました'
        langMsg['complain_registered'] = 'リポートが送信されました'
        langMsg['review_added'] = 'レビューを追加しました'
        langMsg['review_updated'] = 'レビューが更新されました'
        langMsg['booking_completed'] = '予約が完了されました'

        langMsg['account_connected'] = '口座情報が登録されました'
        langMsg['edit_account_connected'] = '口座情報が修正されました。'
        langMsg['bank_details_added'] = '口座情報が追加されました'
        langMsg['bank_details_updated'] = '口座情報が更新されました'
        langMsg['post_created'] = '募集が作成されました'
        langMsg['post_updated'] = '募集が更新されました'
        langMsg['post_deleted'] = '募集が削除されました'
        langMsg['booking_accepted'] = '授業が確定されました'
        langMsg['booking_declined'] = '授業が確定されませんでした'
        langMsg['time_slot_accepted'] = '授業が確定されました'
        langMsg['time_slot_declined'] = '授業が確定されませんでした'
        langMsg['response_send_successfully'] = '応募しました'
        langMsg['response_declined'] = '採用を見送りました'
        langMsg['date_blocks'] = '日付をブロックしました'
        langMsg['time_logged'] = '記録された時間'

        langMsg['email_exist'] = 'こちらのメールアドレスは既に使用されています'
        langMsg['email_not_match'] = "メールアドレスが一致しません"
        langMsg['password_not_match'] = "パスワードが一致しません"
        langMsg['welcome'] = 'ようこそ'
        langMsg['login_successfull'] = '様'
        langMsg['email_not_exist'] = 'こちらのメールアドレスに一致するアカウントが見つかりません'
        langMsg['password_wrong'] = '正しいパスワードを入力してください'
        langMsg['correct_old_password'] = '正しい前回のパスワードを入力してください'
        langMsg['password_changed'] = 'パスワードが変更されました'
        langMsg['profile_updated'] = 'プロフィールが更新されました'
        langMsg['status_update'] = '更新されました'
        langMsg['academic_added'] = '学歴が追加されました'
        langMsg['certificate_added'] = '資格・免許が追加されました'
        langMsg['certificate_updated'] = '資格・免許が更新されました'
        langMsg['academic_updated'] = '学歴が更新されました'
        langMsg['email_sent'] = 'メールが送信されました'
        langMsg['wrong_otp'] = '確認コードが一致しません'
        langMsg['invalid_user'] = '無効なユーザー'
        langMsg['class_already_started'] = '授業は既に開始されています'
        langMsg['verification_pending'] = 'アカウントが認証されていません。認証メールのURLからアカウントの認証をしてください。'

        langMsg['hourly_rate_max_check_one'] = '最大時給は'
        langMsg['hourly_rate_max_check_two'] = 'です。'
        langMsg['hourly_rate_min_check'] = ' The minimum allowed hourly rate is'

        return langMsg
    }
}
module.exports = LocalizationJp;