class LocalizationEn {
    static async lang() {
        var langMsg = {}
        langMsg['request_created'] = 'Request created'
        langMsg['request_updated'] = 'Request updated'
        langMsg['request_deleted'] = 'Request deleted'
        langMsg['delete_booking'] = 'Request deleted successfully'
        langMsg['response_declined'] = 'Application declined'
        langMsg['response_accepted'] = 'Application accepted'
        langMsg['booking_successfully'] = 'Application submitted successfully'
        langMsg['request_send_successfully'] = 'Request sent successfully'
        langMsg['class_cancelled_successfully'] = 'Class cancelled successfully'
        langMsg['hourly_rate_changed'] = 'Hourly rate changed'
        langMsg['error_changing_hourly_rate'] = 'Error while changing hourly rate'
        langMsg['invalid_amount'] = 'Please enter a valid amount'
        langMsg['payment_done'] = 'Payment completed'
        langMsg['complain_registered'] = 'Complaint submitted'
        langMsg['review_added'] = 'Review submitted'
        langMsg['review_updated'] = 'Review updated'
        langMsg['booking_completed'] = 'Course successfully completed'

        langMsg['account_connected'] = 'Bank account connected'
        langMsg['edit_account_connected'] = 'Bank account details resubmitted'
        langMsg['bank_details_added'] = 'Bank account details added'
        langMsg['bank_details_updated'] = 'Bank account details updated'
        langMsg['post_created'] = 'Post created'
        langMsg['post_updated'] = 'Post updated'
        langMsg['post_deleted'] = 'Post deleted'
        langMsg['booking_accepted'] = 'Booking accepted'
        langMsg['booking_declined'] = 'Booking declined'
        langMsg['time_slot_accepted'] = 'Booking confirmed'
        langMsg['time_slot_declined'] = 'Application declined'
        langMsg['response_send_successfully'] = 'Response sent successfully'
        langMsg['response_declined'] = 'Response declined successfully'
        langMsg['date_blocks'] = 'Date is blocked in My Calendar'
        langMsg['time_logged'] = 'Time logged'

        langMsg['email_exist'] = 'Email is already in use!'
        langMsg['email_not_match'] = "Emails do not match"
        langMsg['password_not_match'] = "Passwords do not match"
        langMsg['welcome'] = 'Welcome'
        langMsg['login_successfull'] = ', your login was successful.'
        langMsg['email_not_exist'] = 'No account found with that email address'
        langMsg['password_wrong'] = 'Please enter the correct password'
        langMsg['correct_old_password'] = 'Please enter the correct old password'
        langMsg['password_changed'] = 'Your password has been changed successfully'
        langMsg['profile_updated'] = 'Your profile has been updated successfully'
        langMsg['status_update'] = 'Your status has been updated successfully'
        langMsg['academic_added'] = 'Education Background added'
        langMsg['certificate_added'] = 'Academic Qualification/Certification added'
        langMsg['certificate_updated'] = 'Academic Qualification/Certification updated'
        langMsg['academic_updated'] = 'Education Background updated'
        langMsg['email_sent'] = 'An email has been sent'
        langMsg['wrong_otp'] = "You've entered an incorrect OTP"
        langMsg['invalid_user'] = 'Invalid user'
        langMsg['class_already_started'] = 'Class already started'
        langMsg['verification_pending'] = ' You need to verify your account first via the link sent to your email address.'

        langMsg['hourly_rate_max_check'] = 'The maximum allowed hourly rate is'
        langMsg['hourly_rate_min_check'] = 'The minimum allowed hourly rate is'

        //Connected account error
        
        
        
        return langMsg
    }
}
module.exports = LocalizationEn;