const jwt = require("jsonwebtoken");
class Auth {
  static async checkTeacherToken(req, res, next) {
    const token = req.header("authorization");
    if (!token) {
      return res.status(401).json({
        error: "unauthorized",
        error_description: "Access denied. No token provided",
      });
    }
    try {
      const decoded = jwt.verify(token, "secretkey");
      req.user = decoded.user;
      if (req.user.user_type != 2) {
        return res.status(401).json({
          error: "unauthorized",
          error_description: "Access denied.",
        });
      }

      next();
    } catch (err) {
      res.status(401).send({
        error: "unauthorized",
        error_description: "Access denied . Invalid token provided",
      });
    }
  }
  static async checkStudentToken(req, res, next) {
    const token = req.header("authorization");
    if (!token) {
      return res.status(401).json({
        error: "unauthorized",
        error_description: "Access denied. No token provided",
      });
    }
    try {
      const decoded = jwt.verify(token, "secretkey");
      req.user = decoded.user;
      if (req.user.user_type != 1) {
        return res.status(401).json({
          error: "unauthorized",
          error_description: "Access denied.",
        });
      }

      next();
    } catch (err) {
      res.status(401).send({
        error: "unauthorized",
        error_description: "Access denied . Invalid token provided",
      });
    }
  }
  static async checkToken(req, res, next) {
    const token = req.header("authorization");
    
    if (!token) {
      return res.status(401).json({
        error: "unauthorized",
        error_description: "Access denied. No token provided",
      });
    }

    try {
      const decoded = jwt.verify(token, "secretkey");
      req.user = decoded.user;

      next();
    } catch (err) {
      res.status(401).send({
        error: "unauthorized",
        error_description: "Access denied . Invalid token provided",
      });
    }
  }
  static async checkOptionalToken(req, res, next) {
    const token = req.header("authorization");
    try {
      if (token) {
        const decoded = jwt.verify(token, "secretkey");
        console.log(decoded.user);
        req.user = decoded.user;
      }
      next();
    } catch (err) {
      res.status(401).send({
        error: "unauthorized",
        error_description: "Access denied . Invalid token provided",
      });
    }
  }

}
module.exports = Auth;
