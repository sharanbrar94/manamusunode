const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')
//const FCM = require('fcm-node')
// var serverKey = 'AAAAhiv3_GE:APA91bF49fXdpiIvJY49UxPX8nscslDHw7dbGN2nObklSNBMhhizZGORhLzoZcRZQ5XvX7rmhpsiTCr9GT6CQjGFfTd_C0cH2vcKnPkyqrfqvB8FjMORirBSgUOJab_B-umq8AYGKTVI';
var serverKey = require("../config/packag-file.json");
var twilio = require('twilio');
var path = require('path')
var FCM = require("fcm-notification");
var fcm = new FCM(serverKey);
const moment  = require('moment');
//attach the plugin to the nodemailer transporter
console.log(path.join(__dirname, "../public/views/email-verification.hbs"))
const client = new twilio(process.env.TWILIO_SID, process.env.TWILIO_TOKEN);
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'contact@manamusu.com',
    pass: 'vbjclvkkkqiwzwsn'
  }
});
const options = {
  viewEngine: {
    partialsDir: path.join(__dirname, "../public/views/partials"),
    layoutsDir: path.join(__dirname, "../public/views/layouts"),
    extname: ".hbs"
  },
  extName: ".hbs",
  viewPath: ""
};
transporter.use("compile", hbs(options));

class Common {
  static async sendMail(email, subject, text,otp) {
    try {
      var mailOptions = {

        from: 'gurjeevan.kaur.henceforth@gmail.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/views/email-verification"),
        context: {
          // Data to be sent to template engine.
          url: 'https://henceforthsolutions.com/',
          text: text,
          otp:otp,
          subject: subject,
        }
      }

      let mail = await transporter.sendMail(mailOptions)
      console.log(mail);

    } catch (err) {
      console.log(err);
      throw err;
    }

    return "success"
  }
  static async adminNotification(email,content) {
    var subject = "Notification"
    try {
      var mailOptions = {

        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../emails/admin-email"),
        context: {
          // Data to be sent to template engine.
          content:content
          
        }
      }

      let mail = await transporter.sendMail(mailOptions)
      console.log(mail);

    } catch (err) {
      console.log(err);
      throw err;
    }

    return "success"
  }
  static async forgotPassword(email,getUrl,otp,username) {
    var subject = "パスワードの変更のお知らせ / Password Reset for Manamusu Account"
    try {
      var mailOptions = {

        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../emails/password-reset"),
        context: {
          // Data to be sent to template engine.
          getUrl:getUrl,
          otp:otp,
          email:email,
          username:username,
          subject: subject
        }
      }

      let mail = await transporter.sendMail(mailOptions)
      console.log(mail);

    } catch (err) {
      console.log(err);
      throw err;
    }

    return "success"
  }
  /*======Verification email=======*/
  static async userTeacherVerification(email, getUrl,url,first_name){
    let subject = "メール認証のお願い / Request for Email verification"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../emails/request-teacher-verification"),
        context: {
          // Data to be sent to template engine.
          url: 'https://manamusu.com',
          subject: subject,
          url:url,
          getUrl:getUrl,
          first_name:first_name
         
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  static async userStudentVerification(email, getUrl,url,first_name){
    let subject = "メール認証のお願い / Request for Email verification"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../emails/request-student-verification"),
        context: {
          // Data to be sent to template engine.
          url: 'https://manamusu.com',
          subject: subject,
          url:url,
          getUrl:getUrl,
          first_name:first_name
          
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  /*=========After verification email======*/
  static async verificationStudent(email,getUrl,first_name){
    let subject = "メールアドレスの認証が完了されました。ようこそ、マナムスへ / Your email has been verified"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../emails/email-verification"),
        context: {
          // Data to be sent to template engine.
          subject: subject,
          getUrl:getUrl,
          first_name:first_name
         
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  static async verificationTeacher(email, getUrl,first_name){
    let subject = "メールアドレスの認証が完了されました。ようこそ、マナムスへ / Your email has been verified"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../emails/email-verification2"),
        context: {
          // Data to be sent to template engine.
          subject: subject,
          getUrl:getUrl,
          first_name:first_name
          
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }

  /*==============Tutor emails=========*/

  //post apply by student / email to tutor//
  static async postApplyByStudent(teacherData,emailData,total_amount,getUrl){
    let subject = "授業依頼のお知らせ / Someone has applied to your post"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: teacherData.email,
        subject: subject,
        template: path.join(__dirname, "../emails/applied-post"),
        context: {
          // Data to be sent to template engine.
          url: 'https://manamusu.com',
          subject: subject,
          emailData:emailData,
          total_amount:total_amount,
          getUrl:getUrl,
          teacherData:teacherData
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }

  //student make payment / email to tutor//
  static async makePaymentByStudent(teacherData,emailData,currency,amount,getUrl,studentName){
    let subject = "授業の支払いが完了されました / Your student has deposited their class fees "
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: teacherData.email,
        subject: subject,
        template: path.join(__dirname, "../emails/deposited-email"),
        context: {
          // Data to be sent to template engine.
          url: 'https://manamusu.com',
          emailData:emailData,
          currency:currency,
          amount:amount,
          getUrl:getUrl,
          subject: subject,
          teacherData:teacherData,
          studentName:studentName
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  //email to student
  static async makePaymentEmailToStudent(studentData,emailData,currency,amount,getUrl){
    let subject = "授業の支払いが完了されました / Confirmation of Class Fees Deposit"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: studentData.email,
        subject: subject,
        template: path.join(__dirname, "../emails/deposited-fees"),
        context: {
          // Data to be sent to template engine.
          url: 'https://manamusu.com',
          emailData:emailData,
          currency:currency,
          amount:amount,
          getUrl:getUrl,
          subject: subject,
          studentData:studentData
          
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  //accept response by student / email to tutor//
  static async acceptResponseByStudent(studentData,teacherData,emailData,getUrl){
    let subject = "授業が確定されました / Someone has accepted your application"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: teacherData.email,
        subject: subject,
        template: path.join(__dirname, "../emails/accpted-email"),
        context: {
          // Data to be sent to template engine.
          teacherData:teacherData,
          subject: subject,
          studentData:studentData,
          getUrl:getUrl,
          emailData:emailData
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  //accept booking by teacher / email to student//
  static async acceptBookingByTeacher(studentData,teacherData,emailData,getUrl){
    let subject = "授業が確定されました / Someone has accepted your application"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: studentData.email,
        subject: subject,
        template: path.join(__dirname, "../emails/accpted-email2"),
        context: {
          // Data to be sent to template engine.
          teacherData: teacherData,
          emailData:emailData,
          getUrl:getUrl,
          subject: subject,
          studentData:studentData
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  //decline response by student / email to tutor//
  static async declineResponseByStudent(teacherData,emailData,getUrl){
    let subject = "授業が確定されませんでした / Someone has declined your application "
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: teacherData.email,
        subject: subject,
        template: path.join(__dirname, "../emails/decline-email"),
        context: {
          // Data to be sent to template engine.
          emailData:emailData,
          getUrl:getUrl,
          subject: subject,
          teacherData:teacherData
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }


  static async bookingEmail(email,subject,text){
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/views/email"),
        context: {
          // Data to be sent to template engine.
          url: 'https://henceforthsolutions.com/',
          text: text,
          subject: subject,
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }

  

  static async bookingAcceptEmail(email) {
    try {
      let subject = "Booking Accepted"
      let text = "Booking Accepted"
      var mailOptions = {

        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/views/email"),
        context: {
          // Data to be sent to template engine.
          url: 'https://henceforthsolutions.com/',
          text: text,
          subject: subject,
        }
      }

      let mail = await transporter.sendMail(mailOptions)
      console.log(mail);

    } catch (err) {
      console.log(err);
      throw err;
    }

    return "success"
  }

  static async bookingDeclineEmail(email) {
    try {
      let subject = "Booking Declined"
      let text = "Booking Declined"
      var mailOptions = {

        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/views/email"),
        context: {
          // Data to be sent to template engine.
          url: 'https://henceforthsolutions.com/',
          text: text,
          subject: subject,
        }
      }

      let mail = await transporter.sendMail(mailOptions)
      console.log(mail);

    } catch (err) {
      console.log(err);
      throw err;
    }

    return "success"
  }

  /*=======Student email======*/

  //send request by teacher / email to student//
  static async postApplyByTeacher(studentData,teacherName,emailData,getUrl){
    let subject = "授業依頼のお知らせ / Someone has applied to your post"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: studentData.email,
        subject: subject,
        template: path.join(__dirname, "../emails/applied-post2"),
        context: {
          // Data to be sent to template engine.
          url: 'https://manamusu.com',
          subject: subject,
          emailData:emailData,
          teacherName:teacherName,
          studentData:studentData,
          getUrl:getUrl
          
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  //decline response by teacher / email to student//
  static async declineResponseByTeacher(studentData,emailData,getUrl){
    let subject = "授業が確定されませんでした / Someone has declined your application "
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: studentData.email,
        subject: subject,
        template: path.join(__dirname, "../emails/decline-email2"),
        context: {
          // Data to be sent to template engine.
          subject: subject,
          getUrl:getUrl,
          emailData:emailData,
          studentData:studentData
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  static async connectAccountRestrict(email, getUrl,first_name,restrict_issue){
    let subject = "（重要）口座の登録ができませんでした / Bank account registration unsuccessful"
    try {
      var mailOptions = {
        from: {
          name: 'Manamusu',
          address: 'contact@manamusu.com'
        },
        to: email,
        subject: subject,
        template: path.join(__dirname, "../emails/connect-account"),
        context: {
          // Data to be sent to template engine.
          url: 'https://manamusu.com',
          subject: subject,
          getUrl:getUrl,
          first_name:first_name,
          restrict_issue:restrict_issue
         
        }
      }

      let mail = await transporter.sendMail(mailOptions)
    } catch (err) {
      throw err;
    }
    return "success"
  }
  
  /*==========Student email=========*/
    static async classNotificationEmail(getUrl,studentData,teacherData,start_time){
      let subject = "リマインド）授業のお知らせ / You have a class coming up in 1 hour!"
      try {
        var mailOptions = {
          from: {
            name: 'Manamusu',
            address: 'contact@manamusu.com'
          },
          to: studentData.email,
          subject: subject,
          template: path.join(__dirname, "../emails/class-email"),
          context: {
            // Data to be sent to template engine.
            subject: subject,
            getUrl:getUrl,
            start_time:start_time,
            studentData:studentData,
            teacherData:teacherData
          }
        }

        let mail = await transporter.sendMail(mailOptions)
      } catch (err) {
        throw err;
      }
      return "success"
    }

 

  //Generate Random String
  static async generateRandomString(length) {
    var text = '';
    const possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  

  //common function for push notification
  static async push(fcm_ids, notification, data, collapse_key = null) {
    console.log("push start")

    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
      // to: fcm_ids[0],
      registration_ids: fcm_ids,
      notification: notification,
      data: data
    };

    fcm.send(message, function (err, response) {
      if (err) {
        console.log("Something has gone wrong!");
      } else {
        console.log("Successfully sent with response: ", response);
      }
    });
  }

  static async fcmPush(type, fcm_ids, title, message) {
    var message = {
      data: {
        type: type
      },
      notification: {
        title: title,
        body: message,
      },
      android: {
        ttl: 3600 * 1000,
        notification: {
          icon: 'stock_ticker_update',
          color: '#f45342',
        },
      },
      apns: {
        payload: {
          aps: {
            badge: 42,
          },
        },
      }
    };

    fcm.sendToMultipleToken(message, fcm_ids, function (err, response) {
      if (err) {
        console.log('error found', err);
      } else {
        console.log('response here', response);
      }
    })
  }

  //send messages
  static async sendMessage(number, body) {
    client.messages.create(
      {
        to: number,
        from: process.env.TWILIO_FROM,
        body: body,
      },
      function (error, message) {
        if (!error) {
          console.log('Success! The SID for this SMS message is:');
          console.log(message.sid);
          console.log('Message sent on:');
          console.log(message.dateCreated);
        } else {

          console.log('Oops! There was an error.');
          console.log(error)
          throw ('Oops! There was an error.'
          );
          //return error.Error;
        }
      },
    );
  }
  static async calculateOffset(page, limit) {
    var offset = 0
    if (page > 0 && limit) {
      var offset = (page - 1) * limit;
    } else if (limit) {
      var limit = limit
    } else {
      var limit = 20
    }
    return { offset, limit }
  }

  static async generateRandomNumber(length) {
    var text = "";
    const possible = "123456789";

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    var random = parseInt(text);
    return random;
  }

  static async calculateSkipOffset(page = 1, limit = 20, default_limit = null) {
    console.log("page limit:", page, limit);
    let offset = await this.minPage(page) * await this.minLimit(limit, default_limit);
    console.log("offset ", offset);
    return offset
  }

  static async minPage(page = 1) {
    if (!page || page <= 1) {
      page = 0;
    }
    console.log("page ", page);
    return page;
  }

  static async minLimit(limits = 20, default_limit = null) {
    var limit = parseInt(limits, 10);
    if (isNaN(limit)) {
      limit = 10;
    } else if (limit < 1) {
      limit = 1;
    }
    console.log("limit ", limit);
    return limit
  }

  static getData(data, limit, offset) {
    let start = offset;
    let end = start + limit;
    let result = data.slice(start, end);
    return result;
  }
  /*=======user academics======*/
  static async getAcademics(user_id){
    let cer = `select * from academics where user_id=?`
    const getCer = await connection.query(cer, [user_id])
    return getCer
  }
  /*=====user data=====*/
  static async userData(user_id){
    let users = `select * from users where id=?`
    const getUser = await connection.query(users, [user_id])
    return getUser[0]
  }
  /*=====get booking detail=====*/
  static async bookingDetail(booking_id){
    let booking = `select * from booking_requests where id=?`
    const getBooking = await connection.query(booking, [booking_id])
    return getBooking[0]
  }
  /*======get booking review======*/
  static async bookingReviews(booking_id){
    let bookingReviews = `select booking_reviews.*,b.booking_unique_id,p.title,p.id as post_id,fu.first_name as student_first_name,fu.last_name as student_last_name,fu.profile_image as student_profile_image,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.profile_image as teacher_profile_image from booking_reviews left join users as fu on booking_reviews.from_user_id=fu.id left join users as tu on booking_reviews.to_user_id=tu.id left join booking_requests as b on booking_reviews.booking_id = b.id left join posts as p on b.post_id = p.id where booking_reviews.booking_id=?`
    const getReview = await connection.query(bookingReviews, [booking_id])
    return getReview[0]
  }
  static async reviewById(review_id){
    let bookingReviews = `select booking_reviews.*,fu.first_name as student_first_name,fu.last_name as student_last_name,fu.profile_image as student_profile_image,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.profile_image as teacher_profile_image from booking_reviews left join users as fu on booking_reviews.from_user_id=fu.id left join users as tu on booking_reviews.to_user_id=tu.id where booking_reviews.id=?`
    const getReview = await connection.query(bookingReviews, [review_id])
    return getReview[0]
  }
  static async getRequestDetail(request_id){
    let sqlQuery = `select requests.*,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id  where requests.id=?`
    var post = await connection.query(sqlQuery, [request_id])
    return post[0]
}
  /*=======get currency detail=====*/
  static async getCurrencyRate(from,to,amount){
    if(from == 'USD'){
      let getRate = await this.getExchangeRate(from,to)
      var getAmount = getRate*amount
    }
    if(from == 'JPY' ){
      if(to == 'USD'){
        let getRate = await this.getExchangeRate('USD','JPY')
        var getAmount = (1/getRate)*amount
      }
      if(to == 'SGD'){
        let jpyUsd = await this.getExchangeRate('USD','JPY')
        let usdSgd = await this.getExchangeRate('USD','SGD')
        let jpyUsdAmount = (1/jpyUsd)
        let usdSgdAmount = usdSgd
        var getAmount = (jpyUsdAmount*usdSgdAmount)*amount
      }
    }
    if(from == 'SGD' ){
      if(to == 'USD'){
        let getRate = await this.getExchangeRate('USD','SGD')
        var getAmount = (1/getRate)*amount
      }
      if(to == 'JPY'){
        let sgdUsd = await this.getExchangeRate('USD','SGD')
        let usdJpy = await this.getExchangeRate('USD','JPY')
        let sgdUsdAmount = (1/sgdUsd)
        let usdJpyAmount = usdJpy
        var getAmount = (sgdUsdAmount*usdJpyAmount)*amount
      }
    }
    return getAmount

  }
  static async getExchangeRate(from,to){
    let query = `select * from exchange_rates where from_currency = '${from}' and to_currency = '${to}'`
    let exchangeRate = await connection.query(query)
    return exchangeRate[0].rate
  }

  /*=======call localization file according to language======*/

  static dbColumnLang(col_name,lang){
    return lang!="en"?col_name+"_"+lang:col_name
  }

  static async localization(lang){
    if(lang == "jp"){
      var localFile = require("./localization-jp.js");
      var getMessage = await localFile.lang();
    }
    else{
      var localFile = require("./localization-en.js");
      var getMessage = await localFile.lang();
    }
    return getMessage
  }
  /*====Get date before 24 hours and convert into utc=====*/
  static async utcDate(startTime,date){
    console.log("booking slot date",date)
    var getDate = await this.changeDateFormat(date)
    var datetime = new Date(getDate+" "+startTime+":00")//change date time into millisecond
    var ts = new Date(datetime.getTime() - (24 * 60 * 60 * 1000));//date before 24 hours
    console.log("24 hour before",ts)
    return ts
  }
  static async classStartTime(startTime,date){
    console.log("booking slot date",date)
    var getDate = await this.changeDateFormat(date)
    var datetime = new Date(getDate+" "+startTime+":00")//change date time into millisecond
    var ts = new Date(datetime);//date before 24 hours
    return ts
  }
  //change date format Y-m-d
  static async changeDateFormat(date_ob){
    var day = ("0" + date_ob.getDate()).slice(-2);
    var month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    var year = date_ob.getFullYear();
    var date = year + "-" + month + "-" + day;
    return date
  }
  //get utc current time
  static async getCurrentDate(){
    var now_utc = moment().utc()
    return now_utc
  }
  static async changeCurrentDateFormat(){
    var now_utc = moment().tz('UTC').format("YYYY-MM-DD HH:mm:ss")
    return now_utc
  }
  //get hours difference between start time and end time
  static async calculateHours(start_time,end_time){
    var startTime=moment(start_time, "HH:mm");
    var endTime=moment(end_time, "HH:mm");
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseInt(duration.asHours());
    var minutes = parseInt(duration.asMinutes())-hours*60;
    var calulateMin = minutes/60
    var totalHours = hours+calulateMin
    return totalHours
  }
  static async checkValidAmount(currency,hourly_rate,lang){
    let getLangMsg = await Common.localization(lang)
    if(currency == 'JPY'){
        if(hourly_rate < parseInt(1000)){
          throw("Amount not less than ¥1000")
        }
        if(hourly_rate > parseFloat(11304.71)){
          console.log("lang",getLangMsg)
          if(lang == 'jp'){
            throw(getLangMsg.hourly_rate_max_check_one+' ¥11,304.71 '+getLangMsg.hourly_rate_max_check_two)
          }
          else{
            throw(getLangMsg.hourly_rate_max_check+' ¥11,304.71')
          }
        }
    }
    if(currency == 'SGD'){
        if(hourly_rate < parseFloat(12.35)){
            throw("Amount not less than S$12.35")
        }
        if(hourly_rate > parseFloat(134.01)){
          if(lang == 'jp'){
            throw(getLangMsg.hourly_rate_max_check_one+' S$134.01 '+getLangMsg.hourly_rate_max_check_two)
          }
          else{
            throw(getLangMsg.hourly_rate_max_check+' S$134.01')
          }
        }
    }
    if(currency == 'USD'){
        if(hourly_rate < parseInt(9)){
            throw("Amount not less than $9")
        }
        if(hourly_rate > parseInt(99)){
          if(lang == 'jp'){
            throw(getLangMsg.hourly_rate_max_check_one+' $99 '+getLangMsg.hourly_rate_max_check_two)
          }
          else{
            throw(getLangMsg.hourly_rate_max_check+' $99')
          }
        }
    }
  }
  static async timeSince (date,lang)  {  
    var utc_date =  moment.utc(date) 
    var currentDate = await this.getCurrentDate()
    var seconds = Math.floor((currentDate - utc_date) / 1000);  
    console.log("seconds...",seconds)  
    var interval = seconds / 31536000;    if (interval > 1) {
      return Math.floor(interval) + " years ago";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
      if(lang == 'en'){
        return Math.floor(interval) + " months ago";
      }
      else{
        return Math.floor(interval) + " 数か月前";
      }
    }
    interval = seconds / 86400;
    if (interval > 1) {
      if(lang == 'en'){
        return Math.floor(interval) + " days ago";
      }
      else{
        return Math.floor(interval) + " 日前";
      }
    }
    interval = seconds / 3600;
    if (interval > 1) {
      if(lang == 'en'){
        return Math.floor(interval) + " hour(s) ago";
      }
      else{
        return Math.floor(interval) + " 時間前";
      }
    }
    interval = seconds / 60;
    if (interval > 1) {
      if(lang == 'en'){
        return Math.floor(interval) + " minutes ago";
      }
      else{
        return Math.floor(interval) + " 分前";
      }
    }
    if(lang == 'en'){
      return Math.floor(seconds) + " seconds ago";
    }
    else{
      return Math.floor(interval) + " 秒前";
    }
    
  }
  static async updateTeacherLogs(teacher_id,hours){
    let query = `select * from users where id = ${teacher_id}`
    let totalHours = await connection.query(query)
    let getTotalHours = totalHours[0].total_hours
    console.log("teacher detail",getTotalHours)
    
    let finalHours = getTotalHours+hours
    console.log("teacher_id",finalHours)
    let sqlQuery = 'update users set total_hours=? where id=?'
    let params = [finalHours,teacher_id]
    return await connection.query(sqlQuery, params)
  }
  
}
module.exports = Common;