module.exports.getProduct=function(productId){
    return {id:productId,price:10}
}

module.exports.registerUser = function(username){
    if(!username) throw new Error('username is required');
    return { id: new Date().getTime(), username: username   }
}

module.exports.getCurrencies=function(){
    return ['USD','AUD','EUR']
}