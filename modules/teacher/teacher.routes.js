var express = require('express');
const { checkSchema } = require('express-validator');
let TeacherSchema = require("./teacher.validation.json")
var router = express.Router();
const valid = require('../../config/validator')
const TeacherController = require("./teacher.controller");
let auth = require("../../middleware/auth")

//stripe module
router.post('/teacher/connect-account',auth.checkToken, TeacherController.connectAccount);
router.put('/teacher/connect-account',auth.checkToken, TeacherController.editConnectAccount);
router.post('/teacher/connect-account-webhook',auth.checkToken, TeacherController.connectAccountWebhook);
router.post('/teacher/add-bank-details',auth.checkToken, TeacherController.addBankDetails);
router.put('/teacher/add-bank-details',auth.checkToken, TeacherController.editBankDetail);

//cron
router.get('/teacher/transfer',TeacherController.transferBalance);
router.get('/teacher/complete-classes',TeacherController.cancelOldClasses);
router.get('/teacher/class-notification',TeacherController.classNotification);

//post module
router.get('/teacher/subjects', TeacherController.getSubjects);
router.post('/teacher/post', auth.checkTeacherToken,TeacherController.createPost);
router.put('/teacher/post', auth.checkTeacherToken,TeacherController.updatePost);
router.get('/teacher/post', auth.checkTeacherToken,TeacherController.getPosts);
router.get('/teacher/post-detail', TeacherController.getPostDetail);
router.post('/teacher/delete-post', auth.checkTeacherToken,TeacherController.deletePost);
router.get('/teacher/academicList', auth.checkTeacherToken,TeacherController.getAcademics);
router.get('/teacher/certificateList', auth.checkTeacherToken,TeacherController.getCertificates);
//Booking module
router.get('/teacher/booking', auth.checkTeacherToken,TeacherController.getBookings);
router.get('/teacher/booking-detail', auth.checkTeacherToken,TeacherController.bookingDetail);
router.post('/teacher/accept-booking', auth.checkTeacherToken,TeacherController.acceptBooking);
router.post('/teacher/decline-booking', auth.checkTeacherToken,TeacherController.declineBooking);
router.post('/teacher/accept-time-slot', auth.checkTeacherToken,TeacherController.acceptTimeSlot);
router.post('/teacher/decline-time-slot', auth.checkTeacherToken,TeacherController.declineTimeSlot);
//Request module
router.post('/teacher/send-response', auth.checkTeacherToken,TeacherController.sendResponse);
router.post('/teacher/decline-response', auth.checkTeacherToken,TeacherController.declineResponse);
//Response module
router.get('/teacher/request-responses', auth.checkTeacherToken,TeacherController.requestResponses);
router.post('/teacher/delete-booking', auth.checkTeacherToken,TeacherController.deleteBooking);

//calendar module
router.get('/teacher/calendar', auth.checkTeacherToken,TeacherController.teacherCalendar);
router.get('/teacher/block-dates',TeacherController.getBlockDates);
router.post('/teacher/block-dates', auth.checkTeacherToken,TeacherController.addBlockDate);

/*=====payment module=====*/
router.get('/teacher/payments', auth.checkTeacherToken,TeacherController.getPayments);
router.get('/teacher/payment-detail', auth.checkTeacherToken,TeacherController.getPaymentDetail);

/*=====logs module====*/
router.post('/teacher/save-log',auth.checkTeacherToken,TeacherController.saveLog);






module.exports = router;