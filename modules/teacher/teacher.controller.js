const logger = require("../../config/logger"); // importiing winston logger module
const TeacherService = require("./teacher.service");
const moment  = require('moment');
class TeacherController {
  static async connectAccountWebhook(req, res, next){
    try {
      let data = req.body.data
      let result  = await TeacherService.connectAccountWebhook(data);
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async connectAccount(req, res, next){
    try {
      const lang = req.header("lang");
      let account_number = req.body.account_number
      let account_holder_name = req.body.account_holder_name
      let routing_number = req.body.routing_number
      let user_id = req.user.id 
      let first_name = req.body.first_name
      let first_name_kanji = req.body.first_name_kanji
      let last_name = req.body.last_name
      let last_name_kanji = req.body.last_name_kanji
      let country = req.body.country
      let postal_code = req.body.postal_code 
      let state = req.body.state
      let city = req.body.city
      let town = req.body.town
      let line1 = req.body.line1 
      let line2 = req.body.line2 
      let gender = req.body.gender 
      let phone = req.body.phone 
      let file = req.body.file 
      let getDay = req.body.date 
      let getMonth = req.body.month 
      let getYear = req.body.year 
      
      let subjects  = await TeacherService.connectAccount(getDay,getMonth,getYear,account_number,account_holder_name,routing_number,user_id,first_name,first_name_kanji,last_name,last_name_kanji,country,postal_code,state,city,town,line1,line2,gender,phone,file,lang);
      return res.status(200).json(subjects)
    } 
    catch (err) {
      console.log("err"+err)
      logger.error(`message - ${err.raw.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err.raw.message?err.raw.message :err.raw.code });
    }

  }
  static async editConnectAccount(req, res, next){
    try {
      let lang = req.header("lang");
      let user_id = req.user.id 
      
      let account_number = req.body.account_number
      let account_holder_name = req.body.account_holder_name
      let routing_number = req.body.routing_number
      let first_name = req.body.first_name
      let first_name_kanji = req.body.first_name_kanji
      let last_name = req.body.last_name
      let last_name_kanji = req.body.last_name_kanji
      let country = req.body.country
      let postal_code = req.body.postal_code 
      let state = req.body.state
      let city = req.body.city
      let town = req.body.town
      let line1 = req.body.line1 
      let line2 = req.body.line2 
      let gender = req.body.gender 
      let phone = req.body.phone 
      let getFile = req.body.file 
      let getDay = req.body.date 
      let getMonth = req.body.month 
      let getYear = req.body.year 
      console.log(req.user)
      
      let connectAccounts  = await TeacherService.editConnectAccount(getDay,getMonth,getYear,user_id,account_number,account_holder_name,routing_number,first_name,first_name_kanji,last_name,last_name_kanji,country,postal_code,state,city,town,line1,line2,gender,phone,getFile,lang);
      return res.status(200).json(connectAccounts)
    } 
    catch (err) {
      console.log("err"+err)
      logger.error(`message - ${err.raw.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err.raw.message?err.raw.message :err.raw.code });
    }
  }
  static async addBankDetails(req, res, next){
    try {
      const lang = req.header("lang");
      let account_number = req.body.account_number
      let account_holder_name = req.body.account_holder_name
      let bank_name = req.body.bank_name
      let routing_number = req.body.routing_number
      let user_id = req.user.id
      let result  = await TeacherService.addBankDetails(account_number,account_holder_name,bank_name,routing_number,user_id,lang);
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async editBankDetail(req, res, next){
    try {
      const lang = req.header("lang");
      let id = req.body.id
      let account_number = req.body.account_number
      let account_holder_name = req.body.account_holder_name
      let bank_name = req.body.bank_name
      let routing_number = req.body.routing_number
      let result  = await TeacherService.editBankDetail(id,account_number,account_holder_name,bank_name,routing_number,lang);
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async transferBalance(req, res, next){
    try {
      let result  = await TeacherService.transferBalance();
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }

  }
  static async cancelOldClasses(req, res, next){
    try {
      let result  = await TeacherService.cancelOldClasses();
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async classNotification(req, res, next){
    try {
      let result  = await TeacherService.classNotification();
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }

  }
  /*======Post module======*/
  static async getSubjects(req, res, next){
    try {
      let subjects  = await TeacherService.getSubjects();
      return res.status(200).json(subjects)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async createPost(req, res, next) {
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let title = req.body.title
      let subject = req.body.subject
      let teaching_standards = req.body.teaching_standards
      let language  = req.body.language
      let currency = req.body.currency
      let hourly_rate = req.body.hourly_rate
      let description = req.body.description
      let time_slot = req.body.time_slot
      let user = await TeacherService.createPost(user_id,title,subject,teaching_standards,language,hourly_rate,currency,description,time_slot,lang);
      return res.status(200).json(user)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async updatePost(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let post_id = req.body.post_id
      let title = req.body.title
      let subject = req.body.subject
      let teaching_standards = req.body.teaching_standards
      let language  = req.body.language
      let hourly_rate = req.body.hourly_rate
      let currency = req.body.currency
      let description = req.body.description
      let time_slot = req.body.time_slot
      let user = await TeacherService.updatePost(user_id,post_id,title,subject,teaching_standards,language,hourly_rate,currency,description,time_slot,lang);
      return res.status(200).json(user)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getPosts(req, res, next){
    try {
      let limit = req.query.limit
      let page = req.query.page
      let user_id = req.user.id
      let currency = req.query.currency
      let post  = await TeacherService.getPosts(limit,page,user_id,currency);
      return res.status(200).json(post)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getPostDetail(req, res, next){
    try {
      const lang = req.header("lang");
      let post_id = req.query.post_id
      let currency = req.query.currency
      let post = {}
      post.postDetail = await TeacherService.getPostDetail(post_id,currency,lang)
      post.language = await TeacherService.getPostLanguage(post_id)
      post.timeSlot = await TeacherService.getPostTimeSlot(post_id)
      return res.status(200).json(post)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async deletePost(req, res, next){
    try {
      const lang = req.header("lang");
      let post_id = req.body.post_id
      var deletePost = await TeacherService.deletePost(post_id,lang)
      return res.status(200).json(deletePost)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getAcademics(req, res, next){
    try {
      let user_id = req.user.id
      var result = await TeacherService.getAcademics(user_id)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getCertificates(req, res, next){
    try {
      let user_id = req.user.id
      var result = await TeacherService.getCertificates(user_id)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  /*===========Booking module=========*/
  static async getBookings(req, res, next){
    try {
      let user_id = req.user.id;
      let status = req.query.status
      let page = req.query.page
      let limit = req.query.limit
      var requests = await TeacherService.getBookings(user_id,status,page,limit)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async bookingDetail(req, res, next){
    try {
      const lang = req.header("lang");
      let booking_id = req.query.booking_id
      let user_id = req.user.id
      var requests = await TeacherService.bookingDetail(booking_id,lang,user_id)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async acceptBooking(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let booking_id = req.body.booking_id
      var result = await TeacherService.acceptBooking(user_id,booking_id,lang)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async declineBooking(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let booking_id = req.body.booking_id
      var result = await TeacherService.declineBooking(user_id,booking_id,lang)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async acceptTimeSlot(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let booking_id = req.body.booking_id
      var result = await TeacherService.acceptTimeSlot(user_id,booking_id,lang)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async declineTimeSlot(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let booking_id = req.body.booking_id
      var result = await TeacherService.declineTimeSlot(user_id,booking_id,lang)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  /*========request module========*/
  static async sendResponse(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let request_id = req.body.request_id
      let time_slot = req.body.time_slot
      var result = await TeacherService.sendResponse(user_id,request_id,time_slot,lang)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async declineResponse(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let request_id = req.body.request_id
      var result = await TeacherService.declineResponse(user_id,request_id,lang)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async requestResponses(req, res, next){
    try {
      let user_id = req.user.id
      let status = req.query.status
      let limit = req.query.limit
      let page = req.query.page
      let currency = req.query.currency
      var result = await TeacherService.requestResponses(user_id,status,limit,page,currency)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async deleteBooking(req, res, next){
    try {
      const lang = req.header("lang");
      let booking_id = req.body.booking_id
      var requests = await TeacherService.deleteBooking(booking_id,lang)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  /*=====Calendar module====*/
  static async teacherCalendar(req, res, next){
    try {
      let month = req.query.month
      let year = req.query.year
      let user_id = req.user.id
      var result = await TeacherService.teacherCalendar(month,year,user_id)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getBlockDates(req, res, next){
    try {
      let month = req.query.month
      let year = req.query.year
      let teacher_id = req.query.teacher_id
      var result = await TeacherService.getBlockDates(month,year,teacher_id)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async addBlockDate(req, res, next){
    try {
      const lang = req.header("lang");
      let dates = req.body.days
      let user_id = req.user.id
      var result = await TeacherService.addBlockDate(dates,user_id,lang)
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getPayments(req, res, next){
    try {
      let user_id = req.user.id
      let page = req.query.page
      let limit = req.query.limit
      var requests = await TeacherService.getPayments(user_id,page,limit)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getPaymentDetail(req, res, next){
    try {
      let payment_id = req.query.payment_id
      var requests = await TeacherService.getPaymentDetail(payment_id)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  /*======Log module======*/
  static async saveLog(req, res, next){
    try {
      const lang = req.header("lang");
      var datetime = moment().tz('UTC').format("YYYY-MM-DD H:m:s")
      let type = req.body.type
      let time_slot_id = req.body.time_slot_id
      let url = req.body.url
      let meeting_id = req.body.meeting_id
      let teacher_id = req.user.id
      var result = await TeacherService.saveLog(teacher_id,datetime, type,time_slot_id,url,lang,meeting_id)
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  
} 

module.exports = TeacherController;
