const { sign } = require('jsonwebtoken');
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const Common = require('../../middleware/common')
const path = require('path');
const fs = require('fs')
const axios = require('axios')
const logger = require('../../config/logger');
const stripe = require('stripe')('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');
//const stripe = require('stripe')('sk_test_51JAVmXG8oPs9xsvBJhSw3b1t8rIj5ytAmWnXYVPTBHRPa0XAgAUogARsw4Z0R1rhGWWIavikDiMMPgtRfxY9ZESP008bXrCUSx');

//const stripeTranslation = require('stripe-i18n');
const moment = require('moment-timezone')

class TeacherService {
    static async connectAccountWebhook(data){

    }
    static async connectAccount(getDay,getMonth,getYear,account_number,account_holder_name,routing_number,user_id,first_name,first_name_kanji,last_name,last_name_kanji,country,postal_code,state,city,town,line1,line2,gender,phone,file,lang){
        let getLangMsg = await Common.localization(lang)

        var response = await stripe.tokens.create({
            bank_account: {
                country: 'JP',
                currency: 'JPY',
                account_holder_name: account_holder_name,
                account_holder_type: 'individual',
                routing_number:  routing_number,
                account_number: account_number,
            },
        });
        let userSqlQuery = `select * from users where id = ${user_id}`
        let getUserEmail = await connection.query(userSqlQuery)
        var bank_account_id = response.id
        let email = getUserEmail[0].email
        let date = getDay
        let month = getMonth
        let year =  getYear
        
        let sqlQuery = `insert into bank_details(user_id,account_number,account_holder_name,routing_number,type,file) values(?,?,?,?,?,?)`
        let params = [user_id,account_number, account_holder_name, routing_number,1,file]
        await connection.query(sqlQuery, params)

        let personalDetailQuery = `insert into stripe_personal_details
        (user_id,first_name,first_name_kanji,last_name,last_name_kanji,country,postal_code,state,city,town,line1,line2,gender,phone,date,month,year) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`
        let parsonalParams = [user_id,first_name,first_name_kanji,last_name,last_name_kanji,country,postal_code,state,city,town,line1,line2,gender,phone,date,month,year]
        await connection.query(personalDetailQuery, parsonalParams)
        
        var file = await stripe.files.create({
            purpose: 'identity_document',
            file: {
                data: fs.readFileSync(path.join(__dirname, `../../public/uploads/orig/${file}`)),
                name: 'success.png',
                type: 'application/octet-stream',
            },
        });
        

        
        let customAccount = await stripe.accounts.create({
            type: 'custom',
            country: 'JP',
            email: email,
            capabilities: {
                card_payments: { requested: true },
                transfers: { requested: true },
            },
            business_type: 'individual',
            business_profile: {
                url: 'https://www.manamusu.com/',
                product_description: 'Manamusu',
                name: email,
                support_email: email,
                support_phone: phone,
                support_url: 'https://www.manamusu.com/',
                support_address: {
                    city: town,
                    country: country,
                    line1: line1,
                    line2: line2,
                    postal_code: postal_code,
                    state: state
                },
                mcc: '8999'        
            },
            individual: {
                address_kana: {
                    country: country,
                    postal_code: postal_code, 
                    state:state,
                    city:city,
                    town: town,
                    line1: line1,
                    line2: line2,
                },
                address_kanji: {
                    line1: line1,
                },
                
                dob: {
                    day: date,
                    month: month,
                    year: year
                },
                
                first_name_kana: first_name,
                first_name_kanji: first_name_kanji,
                last_name_kana:last_name,
                last_name_kanji: last_name_kanji,
                phone: phone,
                email: email,
                gender : gender,
                verification: {
                    additional_document: {
                        back: null,
                        front: null
                    },
                    document: {
                        front: file.id
                    }
                }
            },        
            tos_acceptance: {
                date: 1574863976,
                ip: '73.85.204.126'
            },
            external_account: bank_account_id,      
        })
        console.log("customAccount"+customAccount)

        let updateUser = `update users set connect_account_id='${customAccount.id}',bank_status=1 where id=${user_id}`
        await connection.query(updateUser)
        return ({message:getLangMsg.account_connected})    
    }
    static async editConnectAccount(getDay,getMonth,getYear,user_id,account_number,account_holder_name,routing_number,first_name,first_name_kanji,last_name,last_name_kanji,country,postal_code,state,city,town,line1,line2,gender,phone,getFile,lang){
        let getLangMsg = await Common.localization(lang)
        var getUserData = await Common.userData(user_id)
        var connectAccId = getUserData.connect_account_id
        let date = getDay
        let month = getMonth
        let year =  getYear
        var response = await stripe.tokens.create({
            bank_account: {
                country: 'JP',
                currency: 'JPY',
                account_holder_name: account_holder_name,
                account_holder_type: 'individual',
                routing_number:  routing_number,
                account_number: account_number,
            },
        });
        var bank_account_id = response.id
        var updateFile = await stripe.files.create({
            purpose: 'identity_document',
            file: {
                data: fs.readFileSync(path.join(__dirname, `../../public/uploads/orig/${getFile}`)),
                name: 'success.png',
                type: 'application/octet-stream',
            },
        });
        const account = await stripe.accounts.update(
            connectAccId,
            {
                business_profile: {
                    url: 'https://www.manamusu.com/',
                    product_description: 'Manamusu',
                    support_phone: phone,
                    support_url: 'https://www.manamusu.com/',
                    support_address: {
                        city: town,
                        country: country,
                        line1: line1,
                        line2: line2,
                        postal_code: postal_code,
                        state: state
                    },
                    mcc: '8999'        
                },
                individual: {
                    address_kana: {
                        country: country,
                        postal_code: postal_code, 
                        state:state,
                        city:city,
                        town: town,
                        line1: line1,
                        line2: line2,
                    },
                    address_kanji: {
                        line1: line1,
                    },
                    
                    dob: {
                        day: date,
                        month: month,
                        year: year
                    },
                    
                    first_name_kana: first_name,
                    first_name_kanji: first_name_kanji,
                    last_name_kana:last_name,
                    last_name_kanji: last_name_kanji,
                    phone: phone,
                    gender : gender,
                    verification: {
                        additional_document: {
                            back: null,
                            front: null
                        },
                        document: {
                            front: updateFile.id
                        }
                    }
                },
                external_account: bank_account_id 
            },
            
        );
        let personalDetailQuery = `update stripe_personal_details
        set first_name=?,first_name_kanji=?,last_name=?,last_name_kanji=?,country=?,postal_code=?,state=?,city=?,town=?,line1=?,line2=?,gender=?,phone=?,date=?,month=?,year=? where user_id = ?`
        let parsonalParams = [first_name,first_name_kanji,last_name,last_name_kanji,country,postal_code,state,city,town,line1,line2,gender,phone,date,month,year,user_id]

        let updateBank = `update bank_details set file='${getFile}' where user_id=${user_id}`
        await connection.query(updateBank)

        await connection.query(personalDetailQuery, parsonalParams)
        
        return ({message:getLangMsg.edit_account_connected})  
    }
    static async addBankDetails(account_number,account_holder_name,bank_name,routing_number,user_id){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = `insert into bank_details(user_id,account_number,account_holder_name,bank_name,routing_number) values(?,?,?,?,?)`
        let params = [user_id,account_number, account_holder_name, bank_name, routing_number]
        await connection.query(sqlQuery, params)

        let updateUser = `update users set bank_status=1 where id=${user_id}`
        await connection.query(updateUser)

        return ({message:getLangMsg.bank_details_added})
    }
    static async editBankDetail(id,account_number,account_holder_name,bank_name,routing_number,lang){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = 'update bank_details set account_number=?,account_holder_name=?,bank_name=?,routing_number=? where id=?'
        let params = [account_number,account_holder_name,bank_name,routing_number,id]
        await connection.query(sqlQuery, params)
        return ({message:getLangMsg.bank_details_updated})
    }
    static async transferBalance(){
        let balance  = stripe.balance.retrieve();
        let sqlQuery = `select users.connect_account_id,users.email,p.*,b.id as booking_id from users inner join posts as p on users.id = p.user_id inner join booking_requests as b on p.id = b.post_id where users.user_type = 2 and (b.status=4 or b.status=5) and users.connect_account_id != ''`
        let getTeachers = await connection.query(sqlQuery)
        if(getTeachers.length > 0){
            getTeachers.forEach(async x=>{
                var paymentDetail = await connection.query(`select sum(jpy_amount) as totalAmount from booking_request_slots where status = 3 and payment_status = 2 and booking_request_id = ${x.booking_id} and jpy_amount != '' and transfer_status = 0`)

                if(paymentDetail[0].totalAmount != null){
                    var getTransferAmount = parseInt(paymentDetail[0].totalAmount)
                   // var getTransferAmount = paymentDetail[0].totalAmount
                    const transfer = await stripe.transfers.create({
                        amount: getTransferAmount,
                        currency: 'jpy',
                        destination: x.connect_account_id,
                        transfer_group: 'ORDER_95',
                    });
                    //if(transfer.id){
                        await connection.query(`update booking_request_slots set transfer_status=1 where status = 3 and payment_status = 2 and booking_request_id = ${x.booking_id} and jpy_amount != ''`)
                    //}
                    var emailData = await this.getBookingDetaill(x.booking_id)
                    let getUrl = process.env.WEBSITE_URL
                    let teacherData = await Common.userData(x.user_id)
                    await Common.teacherPayoutEmail(studentEmail,teacherData,emailData.booking_detail,getUrl)
                }
            })
            return getTeachers
        }
        
    }
    static async cancelOldClasses(){
        var currentTime = await Common.getCurrentDate()
        var currentUTcFormatTime = moment().tz('UTC').format("YYYY-MM-DD H:m:s")
        // let getBookingDetail = await connection.query(`select * from booking_request_slots where payment_status = 1`)
        // if(getBookingDetail.length > 0){
        //     await Promise.all(getBookingDetail.map(async (x) => {
        //         var classTime = await Common.classStartTime(x.start_time,x.date)
        //         if(currentTime > classTime ){
        //             console.log("cron status success old classes")
        //             await connection.query(`update booking_request_slots set status=4 where id = ${x.id}`)
        //         }
        //     }));
        // }
        console.log("gurjeevan")
        let getClassBookingDetail = await connection.query(`select booking_request_slots.*,b.currency,p.user_id as teacher_id from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id left join posts as p on b.post_id = p.id where booking_request_slots.start_log_time IS NOT NULL and booking_request_slots.end_log_time is null`)
        console.log("getClassBookingDetail",getClassBookingDetail)
        if(getClassBookingDetail.length > 0){
            await Promise.all(getClassBookingDetail.map(async (x) => {
                var getStartTimeUtc = moment(x.start_log_time).utc()
                var totalHours = await Common.calculateHours(getStartTimeUtc,currentTime)
                var classHours =  x.hours_difference+parseFloat(0.25)
                // console.log("start_log_time",getStartTimeUtc)
                // console.log("currentTime",currentTime)
                
                console.log("totalHours",totalHours)
                console.log("hours_difference",classHours)
                // console.log("meeting_id1",x.meeting_id)
                console.log("time_slot_id",x.id)
                if(totalHours >= classHours){
                    await this.vectera(x.meeting_id)
                    var jpyrate = x.hourly_rate
                    var jpyrateHrs = x.hours_difference
                    var jpyTotalAmount = jpyrate*jpyrateHrs
                    if(x.currency != 'JPY'){
                        console.log("test",x.currency)
                        var jpyFrom = x.currency
                        var jpyTo = 'JPY'
                        jpyTotalAmount = await Common.getCurrencyRate(jpyFrom,jpyTo,jpyTotalAmount)
                    }
                    var jpyAmount = jpyTotalAmount.toFixed(2)
                    
                    //let sqlQuery =
                    await connection.query(`update booking_request_slots set end_log_time='${currentUTcFormatTime}',status = 3,total_log_hour=${totalHours},jpy_amount=${jpyAmount} where id=${x.id}`)
                    await this.classEndMessage(x.id,x.teacher_id,x.user_id,x.booking_request_id)
                    
                }
            }));
        }
    }
    static async classEndMessage(time_slot_id,user_from,user_to,booking_id){
        console.log("jeevan")
        // console.log("time_slot_id",time_slot_id)
        // console.log("user_from",user_from)
        // console.log("user_to",user_to)
        // console.log("booking_id",booking_id)
        let firstNumber = await Math.min(user_from, user_to)
        let secondNumber = await Math.max(user_from, user_to)
        let conversation_id = `${firstNumber}_${secondNumber}`

        const query = `INSERT  INTO message (user_to, user_from,conversation_id,booking_id) values (?,?,?,?)`;
        var chat = await connection.query(query, [user_to,user_from,conversation_id,booking_id]);

        //console.log("chat",chat)

        const timeSloQuery = `select * from booking_request_slots where id=?`;
        const getTimeSlot = await connection.query(timeSloQuery, [time_slot_id]);
        var start_time = getTimeSlot[0].start_time
        var end_time = getTimeSlot[0].end_time
        var end_log_time = getTimeSlot[0].end_log_time
        var total_log_hour = getTimeSlot[0].total_log_hour

        var getEndTime = await this.convertLocalToTimezones(end_log_time);

        let addMonth = getTimeSlot[0].date.getMonth() + 1;
        let getStartDate = await Common.changeDateFormat(getTimeSlot[0].date)
       
        var convertStartTime = getStartDate+' '+start_time+':00'
        var convertEndTime = getStartDate+' '+end_time+':00'

        var getBookingStartTime = await this.convertLocalToTimezones(convertStartTime); 
        var getBookingEndTime = await this.convertLocalToTimezones(convertEndTime); 

        let getDuration = await this.calculateDuration(total_log_hour)

        var message = "Today's "+getBookingStartTime+" - "+getBookingEndTime+" (UTC) class has ended at "+getEndTime+" (UTC). Duration of the class was "+getDuration+"."

        var message_jp = getBookingStartTime+"-"+getBookingEndTime+" (UTC) の授業が "+getEndTime+" (UTC) に終了しました。総授業時間は "+getDuration+" です。"

        const updateQuery = `UPDATE message SET booking_type = ?,message=?,message_jp=? WHERE id = ?`;
        await connection.query(updateQuery, [9,message,message_jp, chat.insertId]);
    }
    static async formatAMPM(date) {
        
        console.log("dd",date)
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
      }
    static async convertLocalToTimezones(localDt) {
        console.log("sd",localDt)
        return moment(localDt).tz('UTC').format("hh:mm A")
       // return moment(localDt).format("hh:mm A")
    }
    static async calculateDuration(mins){
        let getMin = mins*parseInt(60)
        var h = Math.floor(getMin / 60);          
        var m = getMin % 60;
        
        h = h < 10 ? '0' + h : h;
        m = m < 10 ? '0' + m : m;
   
        const duration_time = h+':'+m
        return duration_time
    }
    static async vectera(meeting_id){
        console.log("meeting_id",meeting_id)
        var response = await axios.delete(`https://www.vectera.com/api/v1/meetings/${meeting_id}`, {
            headers: {
                Authorization: 'Token 53ff0ece43d0bc4e9b81fba8443ebf1de159f945',
            }
        })
        console.log("vectera",response)
    }
    static async classNotification(){
        var currentTime = await Common.getCurrentDate()
        let getClassBookingDetail = await connection.query(`select booking_request_slots.*,p.user_id as teacher_id,b.currency from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id left join posts as p on b.post_id = p.id where booking_request_slots.status=1 and booking_request_slots.payment_status = 2`)
        if(getClassBookingDetail.length > 0){
            await Promise.all(getClassBookingDetail.map(async (x) => {
                var getDate = await Common.changeDateFormat(x.date)
                var datetime = getDate+" "+x.start_time+":00"
                var utcClassTime = moment(datetime).subtract(1,'hour').utc()
                if(currentTime == utcClassTime){
                    console.log("notification cron")
                    //send email
                    let getUrl = process.env.WEBSITE_URL
                    let studentData = await Common.userData(x.user_id)
                    let teacherData = await Common.userData(x.teacher_id)
                    await Common.classNotificationEmail(getUrl,studentData,teacherData,x.start_time)
                }
               
               
            }));
        }
        return getClassBookingDetail
    }
    /*=======post module======*/
    static async getSubjects(){
        let sqlQuery = `select * from subjects where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async createPost(user_id,title,subject,teaching_standards,language,hourly_rate,currency,description,time_slot,lang) {
        var getUserData = await Common.userData(user_id)
        let getLangMsg = await Common.localization(lang)
        let certificates = await Common.getAcademics(user_id)
        if(getUserData.is_user_verified == 1){
            if(certificates.length > 0){
                await Common.checkValidAmount(currency,hourly_rate,lang)
            
                let sqlQuery = `insert into posts(user_id,title,subject,teaching_standard,hourly_rate,currency,description) values(?,?,?,?,?,?,?)`
                let params = [user_id,title, subject, teaching_standards, hourly_rate,currency, description]
                let posts = await connection.query(sqlQuery, params)
                if(language){
                    for (var i in language) {
                        await connection.query('insert into post_languages (post_id,language_id) values (?,?)', [posts.insertId, language[i]])
                    }
                }
                if(time_slot){
                    for (var j in time_slot) {
                        var postDays = await connection.query('insert into post_days (post_id,day) values (?,?)', [posts.insertId, time_slot[j].day])
                        for (var k in time_slot[j].time) {
                            console.log(time_slot[j].time[k].start_time)
                            await connection.query('insert into post_day_slots (post_id,post_day_id,start_time,end_time) values (?,?,?,?)', [posts.insertId,postDays.insertId,time_slot[j].time[k].start_time,time_slot[j].time[k].end_time])
                        }
                    }
                }
                let getQuery = `select posts.*,s.name as subject_name,IFNULL((select Avg(rating) from booking_reviews where booking_reviews.to_user_id=posts.user_id),0) as user_rating,u.first_name,u.last_name,u.profile_image from posts left join users as u on posts.user_id = u.id left join subjects as s on posts.subject = s.id  where posts.id=?`
                var getPostDetail = await connection.query(getQuery, [posts.insertId])
                return ({ message: getLangMsg.post_created,postDetail:getPostDetail[0] })
            }
            else{
                throw("Upload academics")
            }
            
        }
        else{
            throw(getLangMsg.verification_pending)
        }
    }
    static async updatePost(user_id,post_id,title,subject,teaching_standard,language,hourly_rate,currency,description,time_slot,lang){
        let getLangMsg = await Common.localization(lang)
        await Common.checkValidAmount(currency,hourly_rate,lang)

        let sqlQuery = 'update posts set user_id=?,title=?,subject=?,teaching_standard=?,hourly_rate=?,currency=?,description=? where id=?'
        let params = [user_id,title,subject,teaching_standard,hourly_rate,currency,description,post_id]
        await connection.query(sqlQuery, params)
        if(language){
            await connection.query('delete from post_languages where post_id = ?', [post_id])
            for (var i in language) {
                await connection.query('insert into post_languages (post_id,language_id) values (?,?)', [post_id, language[i]])
            }
        }
        if(time_slot){
            await connection.query('delete from post_days where post_id = ?', [post_id])
            await connection.query('delete from post_day_slots where post_id = ?', [post_id])
            for (var j in time_slot) {
                var postDays = await connection.query('insert into post_days (post_id,day) values (?,?)', [post_id, time_slot[j].day])
                for (var k in time_slot[j].time) {
                    console.log(time_slot[j].time[k].start_time)
                    await connection.query('insert into post_day_slots (post_id,post_day_id,start_time,end_time) values (?,?,?,?)', [post_id,postDays.insertId,time_slot[j].time[k].start_time,time_slot[j].time[k].end_time])
                }
            }
        }
        let getQuery = `select posts.*,s.name as subject_name,IFNULL((select Avg(rating) from booking_reviews where booking_reviews.to_user_id=posts.user_id),0) as user_rating,u.first_name,u.last_name,u.profile_image from posts left join users as u on posts.user_id = u.id left join subjects as s on posts.subject = s.id  where posts.id=?`
        var getPostDetail = await connection.query(getQuery, [post_id])
        return ({ message: getLangMsg.post_updated,postDetail:getPostDetail[0] })
    }
    static async getPosts(limit,page,user_id,currency){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select posts.*,s.name as subject_name,IFNULL((select Avg(rating) from booking_reviews where booking_reviews.to_user_id=posts.user_id),0) as post_rating,u.first_name,u.last_name,u.profile_image from posts left join users as u on posts.user_id = u.id left join subjects as s on posts.subject = s.id where posts.user_id=? and posts.type = ? and posts.deleted_at is null `
        sqlQuery += ` order by posts.id DESC LIMIT ${limit} OFFSET ${offset}`
        let posts = await connection.query(sqlQuery, [user_id,'post'])
        
        let countSqlQuery = `select posts.*,s.name as subject_name,(select Avg(rating) from reviews where reviews.post_id=posts.id) as post_rating,u.first_name,u.last_name,u.profile_image from posts left join users as u on posts.user_id = u.id left join subjects as s on posts.subject = s.id where posts.user_id=? and posts.type = ? and posts.deleted_at is null `
        let count = await connection.query(countSqlQuery, [user_id,'post'])
        return {count:count.length,posts:posts}
    }
    static async getPostDetail(post_id,currency,lang){
        let sqlQuery = `select posts.*,s.name as subject_name,IFNULL((select Avg(rating) from booking_reviews where booking_reviews.to_user_id=posts.user_id),0) as user_rating,u.first_name,u.last_name,u.profile_image from posts left join users as u on posts.user_id = u.id left join subjects as s on posts.subject = s.id  where posts.id=?`
        var post = await connection.query(sqlQuery, [post_id])
        post[0].original_currency = post[0].currency
        post[0].original_hourly_rate = post[0].hourly_rate
        if(currency != post[0].currency){
            var from = post[0].currency
            let to = currency
            let amount = post[0].hourly_rate
            post[0].hourly_rate = await Common.getCurrencyRate(from,to,amount)
            post[0].currency = currency
            
        }
        post[0].created_at = await Common.timeSince(post[0].created_at,lang)
        console.log("postjeevan",post[0])
        return post[0]
    }
    static async getPostLanguage(post_id){
        let sqlQuery = `select l.name,l.id from post_languages left join languages as l on post_languages.language_id=l.id where post_languages.post_id=?`
        return await connection.query(sqlQuery, [post_id])
    }
    static async getPostTimeSlot(post_id){
        let sqlQuery = `select * from post_days where post_id=?`
        var post_days =  await connection.query(sqlQuery, [post_id])
        var arr = []
        await Promise.all(post_days.map(async (x) => {
            var post = {}
            let optionQuery = `select * from post_day_slots where post_day_id = ${x.id}`
            const options = await connection.query(optionQuery)
            
            post.days = x.day;
            post.time_slot = options
            arr.push(post)
        }));
        return arr
    }
    static async deletePost(post_id,lang){
        var current_date = new Date()
        let getLangMsg = await Common.localization(lang)
        let sqlQuery = `update posts set deleted_at=? where id=?`
        await connection.query(sqlQuery, [current_date,post_id])
        return { message: getLangMsg.post_deleted }
        // await connection.query('delete from posts where id = ?', [post_id])
        // await connection.query('delete from post_languages where post_id = ?', [post_id])
        // await connection.query('delete from post_days where post_id = ?', [post_id])
        // await connection.query('delete from post_day_slots where post_id = ?', [post_id])
    }
    static async getAcademics(user_id){
        let query = `select academics.*,l.name as level_education,c.name as country_name,y.year as year_name from academics left join level_educations as l on academics.level = l.id left join counties as c on academics.country = c.id left join year_educations as y on academics.graduation_year = y.id where user_id = ${user_id}`
        return await connection.query(query)
    }
    static async getCertificates(user_id){
        let query = `select certificates.*,y.year as year_name from certificates left join year_educations as y on certificates.year = y.id where user_id = ${user_id}`
        return await connection.query(query)
    }
    /*==========Booking module========*/
    static async getBookings(user_id,status,page,limit){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let query = `select booking_requests.*,p.title as post_title,u.first_name as student_first_name,u.last_name as student_last_name,u.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on booking_requests.user_id = u.id where p.user_id = ${user_id} and p.deleted_at is null`
        if(status){
            if(status == 1){
                query += ` and booking_requests.status = 1`
            }
            if(status == 2){
                query += ` and booking_requests.status = 2`
            }
            else if(status == 3){
                query += ` and booking_requests.status = 3`
            }
            else if(status == 4){
                query += ` and booking_requests.status = 4`
            }
            else if(status == 5){
                query += ` and booking_requests.status = 5`
            }
        }
        query += ` order by booking_requests.updated_at DESC LIMIT ${limit} OFFSET ${offset}`
        let bookings = await connection.query(query)
        var arr = []
        await Promise.all(bookings.map(async (x) => {
            var booking = {}
            let optionQuery = `select * from booking_request_slots where booking_request_id = ${x.id}`
            const options = await connection.query(optionQuery)
            
            booking = x;
            booking.time_slots = options
            arr.push(booking)
        }));

        let countQuery = `select booking_requests.*,p.hourly_rate,p.title as post_title,u.first_name as student_first_name,u.last_name as student_last_name,u.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on booking_requests.user_id = u.id where p.user_id = ${user_id} and p.deleted_at is null`
        if(status){
            if(status == 1){
                countQuery += ` and booking_requests.status = 1`
            }
            if(status == 2){
                countQuery += ` and booking_requests.status = 2`
            }
            else if(status == 3){
                countQuery += ` and booking_requests.status = 3`
            }
            else if(status == 4){
                countQuery += ` and booking_requests.status = 4`
            }
            else if(status == 5){
                countQuery += ` and booking_requests.status = 5`
            }
        }
        let count = await connection.query(countQuery)
        return {count:count.length,booking:arr}
    }
    static async acceptBooking(user_id,booking_id,lang){
        let getLangMsg = await Common.localization(lang)
        var current_date = new Date()
        var getDate = moment(current_date).format('YYYY-MM-DD hh:mm:ss');
        let bookingsqlQuery = `select * from booking_requests where id=?`
        let getBooking = await connection.query(bookingsqlQuery, [booking_id])
        if(getBooking[0].status == 1){
            let sqlQuery = `update booking_requests set status=2,updated_at='${getDate}' where id=${booking_id}`
            await connection.query(sqlQuery)
        }
        let bookingSqlQuery = `update booking_request_slots set status=1 where booking_request_id=${booking_id} and status = 0`
        await connection.query(bookingSqlQuery)

        //send email//
        var emailData = await this.getBookingDetaill(booking_id)
        let getUrl = process.env.WEBSITE_URL
        let studentEmail = await Common.userData(getBooking[0].user_id)
        let teacherData = await Common.userData(user_id)
        await Common.acceptBookingByTeacher(studentEmail,teacherData,emailData.booking_detail,getUrl)

        return { message: getLangMsg.booking_accepted }
    }
    static async declineBooking(user_id,booking_id,lang){
        let getLangMsg = await Common.localization(lang)
        var current_date = new Date()
        var getDate = moment(current_date).format('YYYY-MM-DD hh:mm:ss');

        let bookingsqlQuery = `select * from booking_requests where id=?`
        let getBooking = await connection.query(bookingsqlQuery, [booking_id])
        let sqlQuery = `update booking_requests set status=3 , updated_at='${getDate}' where id=${booking_id}`
        await connection.query(sqlQuery)
        let bookingSqlQuery = `update booking_request_slots set status=4 where booking_request_id=${booking_id} and status = 0`
        await connection.query(bookingSqlQuery)

        //send email//
        var emailData = await this.getBookingDetaill(booking_id)
        let getUrl = process.env.WEBSITE_URL
        let studentEmail = await Common.userData(getBooking[0].user_id)
        await Common.declineResponseByTeacher(studentEmail,emailData.booking_detail,getUrl)
        return { message: getLangMsg.booking_declined }
    }
    static async acceptTimeSlot(user_id,booking_id,lang){
        let getLangMsg = await Common.localization(lang)

        let bookingsqlQuery = `select * from booking_requests where id=?`
        let getBooking = await connection.query(bookingsqlQuery, [booking_id])

        let bookingSqlQuery = `update booking_request_slots set status=1 where booking_request_id=${booking_id} and status = 0`
        await connection.query(bookingSqlQuery)

        //send email//
        var emailData = await this.getBookingDetaill(booking_id)
        let getUrl = process.env.WEBSITE_URL
        let studentEmail = await Common.userData(getBooking[0].user_id)
        let teacherData = await Common.userData(user_id)
        await Common.acceptBookingByTeacher(studentEmail,teacherData,emailData.booking_detail,getUrl)

        //send email//
        // let studentEmail = await Common.userData(getBooking[0].user_id)
        // let subject = "Time slot accepted"
        // let text = "Time slot accepted"
        // await Common.bookingEmail(studentEmail.email,subject,text)

        return { message: getLangMsg.time_slot_accepted }
    }
    static async declineTimeSlot(user_id,booking_id,lang){
        let getLangMsg = await Common.localization(lang)

        let bookingsqlQuery = `select * from booking_requests where id=?`
        let getBooking = await connection.query(bookingsqlQuery, [booking_id])

        let bookingSqlQuery = `update booking_request_slots set status=4 where booking_request_id=${booking_id} and status = 0`
        await connection.query(bookingSqlQuery)

        //send email//
        // let studentEmail = await Common.userData(getBooking[0].user_id)
        // let subject = "Time slot declined"
        // let text = "Time slot declined"
        // await Common.bookingEmail(studentEmail.email,subject,text)

        return { message: getLangMsg.time_slot_declined }
    }
    static async bookingDetail(booking_id,lang,user_id){
        let getLangMsg = await Common.localization(lang)
        let query = `select booking_requests.*,p.user_id as teacher_id,p.title as post_title,p.teaching_standard,p.subject,p.type as post_type,u.first_name as student_first_name,u.last_name as student_last_name,u.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on booking_requests.user_id = u.id where (p.user_id = ${user_id} or booking_requests.user_id = ${user_id})`
        
        if(booking_id){
            query += ` and booking_requests.id = ${booking_id}`
        }
        let bookingDetail = await connection.query(query)
        if(bookingDetail.length > 0){
            let accpetedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 2`
            const getAcceptedTimeSlots = await connection.query(accpetedTimeSlots)

            let getPaymentPendingTimeslots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 1`
            const paymentPendingTimeSlots = await connection.query(getPaymentPendingTimeslots)

            let pendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 0`
            const getPendingTimeSlots = await connection.query(pendingTimeSlots)

            let cancelledTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (status = 2 or status = 4) and payment_status=2 and refund_status=1`
            const getCancelledTimeSlots = await connection.query(cancelledTimeSlots)

            let declinedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (((status = 2 or status = 4) and payment_status = 1) or ((status = 2 or status = 4) and payment_status=2 and refund_status=0))`
            const getDeclinedTimeslots = await connection.query(declinedTimeSlots)

            let completedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 3`
            const getCompletedTimeSlots = await connection.query(completedTimeSlots)

            let postLang = `select l.name,l.id from post_languages left join languages as l on post_languages.language_id=l.id where post_languages.post_id=?`
            const getPostLan = await connection.query(postLang, [bookingDetail[0].post_id])
            
            let convId = ''
            let conId = `select * from message where booking_id=?`
            const conversation_id = await connection.query(conId, [booking_id]) 
            if(conversation_id.length != 0){
                convId = conversation_id[0].conversation_id
            }

            let bookingReviews = await Common.bookingReviews(booking_id)

            return {booking_detail:bookingDetail[0],accepted_time_slots:getAcceptedTimeSlots,payment_pending_timeslots:paymentPendingTimeSlots,pending_time_slots:getPendingTimeSlots,cancelled_time_slots:getCancelledTimeSlots,declined_time_slots:getDeclinedTimeslots,completed_time_slots:getCompletedTimeSlots,post_language:getPostLan,conversation_id:convId,reviews:bookingReviews}
        }
        else{
            throw(getLangMsg.delete_booking)
        }
    }
    static async sendResponse(user_id,request_id,time_slots,lang){
        let getLangMsg = await Common.localization(lang)

        var getUserData = await Common.userData(user_id)
        if(getUserData.is_user_verified == 1){
            if(getUserData.connect_account_id == null && getUserData.paypal_username == null){
                throw("Please connect your bank account first")
            }
            else{
                let getRequestQuery = `select * from requests where id=?`
                const hourly_rate = await connection.query(getRequestQuery, [request_id]) 
                if(hourly_rate[0].deleted_at == null)
                {
                    if(hourly_rate[0].status == 0){
                        let sqlQuery = `insert into teacher_responses(user_id,request_id,hourly_rate) values(?,?,?)`
                        let params = [user_id,request_id, hourly_rate[0].hourly_rate]
                        await connection.query(sqlQuery, params)
                        var time = []
                        for (var i in time_slots) {
                            time.push([request_id,user_id,time_slots[i].time_slot_id])
                        }
                        const result = await connection.query('insert into teacher_accepted_time_slots (request_id,teacher_id,request_time_slot_id) values ?', [time])

                        //send email
                        let studentData = await Common.userData(hourly_rate[0].user_id)
                        let teacherData = await Common.userData(user_id)
                        let getUrl = process.env.WEBSITE_URL
                        await Common.postApplyByTeacher(studentData,teacherData.first_name,hourly_rate[0],getUrl)
                        
                        return { message: getLangMsg.response_send_successfully }
                    }
                    else{
                        throw("Already apllied by another teacher")
                    }
                }
                else{
                    throw("Request deleted")
                }
            }
        }
        else{
            throw(getLangMsg.verification_pending)
        }
        
    }
    static async declineResponse(user_id,request_id,lang){
        let getLangMsg = await Common.localization(lang)
        await connection.query('delete from teacher_responses where user_id = ? and request_id = ?', [user_id,request_id])
        return { message: getLangMsg.response_declined }
    }
    static async requestResponses(user_id,status,limit,page,currency){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select teacher_responses.*,r.currency,r.title,r.subject,r.hourly_rate,r.teaching_standard,r.description,u.first_name,u.last_name,u.profile_image from teacher_responses left join requests as r on teacher_responses.request_id = r.id left join users as u on r.user_id = u.id where teacher_responses.user_id=${user_id}`
        if(status == 1){
            sqlQuery += ` and teacher_responses.status = 0`
        }
        if(status == 2){
            sqlQuery += ` and teacher_responses.status = 1`
        }
        if(status == 3){
            sqlQuery += ` and teacher_responses.status = 2`
        }
        sqlQuery += ` order by teacher_responses.id DESC LIMIT ${limit} OFFSET ${offset}`
        const responses = await connection.query(sqlQuery)
        responses.forEach(async x=>{
            if(currency != x.currency){
                var from = x.currency
                let to = currency
                let amount = x.hourly_rate
                x.hourly_rate = await Common.getCurrencyRate(from,to,amount)
                x.currency = currency
            }
        })

        let countSqlQuery = `select teacher_responses.*,r.title,r.subject,r.teaching_standard,r.description,u.first_name,u.last_name,u.profile_image from teacher_responses left join requests as r on teacher_responses.request_id = r.id left join users as u on r.user_id = u.id where teacher_responses.user_id=${user_id}`
        if(status == 1){
            countSqlQuery += ` and teacher_responses.status = 0`
        }
        if(status == 2){
            countSqlQuery += ` and teacher_responses.status = 1`
        }
        if(status == 3){
            countSqlQuery += ` and teacher_responses.status = 2`
        }
        const count = await connection.query(countSqlQuery)
        return {count:count.length,responses:responses}
    }

    static async deleteBooking(booking_id,lang){
        let getLangMsg = await Common.localization(lang)
        await connection.query('delete from booking_requests where id = ? and (status = ? or status = ?) ', [booking_id,1,2])
        return ({ message: getLangMsg.delete_booking})
    }

    /*=====Calendar module=====*/
    static async getBlockDates(month,year,user_id){
        let blockDates = 'select * from block_days where YEAR(`date`) = ? AND MONTH(`date`) = ? AND user_id = ?'
        let blockParams = [year,month,user_id]
        const getBlockDates = await connection.query(blockDates,blockParams)
        return getBlockDates
    }
    static async addBlockDate(days,id,lang) {
        let getLangMsg = await Common.localization(lang)

        const dates = await connection.query('select * from block_days where user_id=?', [id])
        var date = []
        for (var i in dates) {
            date.push(dates[i].id)
        }
        if (date.length > 0) {
            var response = await connection.query('delete from block_days where id in (?)', [date])
        }
        var data = []
        if(days.length > 0){
            for (var i in days) {
                data.push([days[i],id])
            }
            const result = await connection.query('insert into block_days (date,user_id) values ?', [data])
        }
        
        return ({message:getLangMsg.date_blocks})
    }
    
    static async teacherCalendar(month,year,user_id){
        let sqlQuery = 'select DATE(booking_request_slots.date) DateOnly from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id left join posts as p on b.post_id = p.id where YEAR(`date`) = ? AND MONTH(`date`) = ? and p.user_id = ? GROUP BY DateOnly ORDER BY DateOnly ASC'
        let params = [year,month,user_id]
        let getDates = await connection.query(sqlQuery, params)
        var arr = []
        await Promise.all(getDates.map(async (x) => {
            var request = {}
            let bookingDates = 'select booking_request_slots.booking_request_id,booking_request_slots.date,b.status,p.title as post_title from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id left join posts as p on b.post_id = p.id where booking_request_slots.date = ? and p.user_id = ? Group BY booking_request_slots.booking_request_id'
            let datesParams = [x.DateOnly,user_id]
            const datesOptions = await connection.query(bookingDates,datesParams)

            

            request.date = x.DateOnly;
            request.classes = datesOptions

            
            await Promise.all(datesOptions.map(async (y) => {
                let classesQuery = 'select * from booking_request_slots where booking_request_slots.booking_request_id = ? and booking_request_slots.date = ?'
                let classParams = [y.booking_request_id,y.date]
                const options = await connection.query(classesQuery,classParams)
                
                y.slots = options
            }));
            arr.push(request)
        }));
        return arr
    }
    static async saveLog(teacher_id,datetime, type,time_slot_id,url,lang,meeting_id){
        let getLangMsg = await Common.localization(lang)

        let getTimeSlots = await this.getBookingRequest(time_slot_id)

        let bookingR = 'update booking_requests set url=? where id=?'
        let bookingP = [url,getTimeSlots[0].booking_request_id]
        await connection.query(bookingR, bookingP)

        if(type == 'start' && getTimeSlots[0].start_log_time == null){
            let sqlQuery = 'update booking_request_slots set meeting_id=?, start_log_time=? where id=?'
            let params = [meeting_id,datetime,time_slot_id]
            await connection.query(sqlQuery, params)
           
        }
        if(type == 'end' && getTimeSlots[0].end_log_time == null){
            console.log("jeevan")
            let getStartLogTime = await this.getBookingRequest(time_slot_id)
            //var startTimeUtc = moment(getStartLogTime[0].start_log_time).utc()
            datetime = await Common.changeCurrentDateFormat()
            let startTimeUtc = moment(getStartLogTime[0].start_log_time).tz('UTC').format("YYYY-MM-DD HH:mm:ss")
            console.log("starttime",startTimeUtc)
            console.log("currentTime",datetime)
            
            let total_hours = await Common.calculateHours(startTimeUtc,datetime)

            var jpyrate = getStartLogTime[0].hourly_rate
            var jpyrateHrs = getStartLogTime[0].hours_difference
            var jpyTotalAmount = jpyrate*jpyrateHrs
            
            if(getStartLogTime[0].currency != 'JPY'){
                var jpyFrom = getStartLogTime[0].currency
                var jpyTo = 'JPY'
                jpyTotalAmount = await Common.getCurrencyRate(jpyFrom,jpyTo,jpyTotalAmount)
            }
            var jpyAmount = Number((jpyTotalAmount).toFixed(2))
            
            let sqlQuery = 'update booking_request_slots set end_log_time=? , status = 3,total_log_hour=?,jpy_amount=? where id=?'
            let params = [datetime,total_hours,jpyAmount,time_slot_id]
            await connection.query(sqlQuery, params)

            await Common.updateTeacherLogs(teacher_id,total_hours)
        }
        return ({ message: getLangMsg.time_logged})
    }
    static async calculateTotalDuration(start_time,end_time){
        var startTime=moment(start_time, "HH:mm");
        var endTime=moment(end_time, "HH:mm");
        var duration = moment.duration(endTime.diff(startTime));
        var hours = parseInt(duration.asHours());
        var getHour = hours*parseInt(60)
        var minutes = parseInt(duration.asMinutes())-hours*60;
        var calulateMin = getHour+minutes
        return calulateMin
    }
    
   
    static async getBookingRequest(time_slot_id){
        let bookingsqlQuery = `select booking_request_slots.*,b.currency from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id where booking_request_slots.id=?`
        let getBooking = await connection.query(bookingsqlQuery, [time_slot_id])
        return getBooking
    }
    static async getPayments(user_id,page,limit){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let query = `select payments.*,b.id as booking_id,b.booking_unique_id,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.profile_image as teacher_profile_image,bu.first_name as student_first_name,bu.last_name as student_last_name,bu.profile_image as student_profile_image from payments left join booking_requests as b on payments.booking_id = b.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as bu on b.user_id = bu.id where payments.deleted_at is null and p.user_id = ${user_id} `
        query += ` order by payments.id DESC LIMIT ${limit} OFFSET ${offset}`
        let payments = await connection.query(query)
        return ({count:payments.length,payments:payments})
    }
    static async getPaymentDetail(payment_id){
        let query = `select payments.*,p.title as post_title,b.created_at as booking_date,b.booking_unique_id as booking_id,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.country as teacher_country,bu.first_name as student_first_name,bu.last_name as student_last_name,bu.country as student_country from payments left join booking_requests as b on payments.booking_id = b.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as bu on b.user_id = bu.id where payments.id = ${payment_id} `
        let payments = await connection.query(query)
        let timeSlotQuery = `select booking_request_slots.*,p.title as post_title,b.created_at as booking_date,py.invoice_id,py.created_at as payment_date,tu.country as teacher_country,su.country as student_country from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id left join payments as py on booking_request_slots.payment_id = py.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as su on b.user_id = su.id where booking_request_slots.payment_id = ${payment_id} `
        let timeSlotDetail = await connection.query(timeSlotQuery)
        return ({payment_detail:payments[0],time_slot_detail:timeSlotDetail})
    }
    static async convertLocalToTimezone(localDt, localDtFormat, timezone) {
        return moment(localDt, localDtFormat).tz(timezone).format('hh:mm A');
    }
    static async getBookingDetaill(booking_id,post_id,user_id,lang){
        let query = `select booking_requests.*,p.user_id as teacher_id,p.id as post_id,p.title as post_title,p.teaching_standard,p.subject,p.type as post_type,u.id as teacher_id,us.first_name as student_first_name,us.last_name as student_last_name,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join users as us on booking_requests.user_id = us.id `
        if(post_id){
            query += ` and booking_requests.post_id = ${post_id} and booking_requests.user_id = ${user_id}`
        }
        if(booking_id){
            query += ` where booking_requests.id = ${booking_id}`
        }
        let bookingDetail = await connection.query(query)
        if(bookingDetail.length > 0){
            let accpetedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 2`
            const getAcceptedTimeSlots = await connection.query(accpetedTimeSlots)

            let paymentPendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 1`
            const getPaymentPendingTimeSlots = await connection.query(paymentPendingTimeSlots)

            let pendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 0`
            const getPendingTimeSlots = await connection.query(pendingTimeSlots)

            let cancelledTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (status = 2 or status = 4) and payment_status=2 and refund_status=1`
            const getCancelledTimeSlots = await connection.query(cancelledTimeSlots)

            let declinedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (((status = 2 or status = 4) and payment_status = 1) or ((status = 2 or status = 4) and payment_status=2 and refund_status=0))`
            const getDeclinedTimeslots = await connection.query(declinedTimeSlots)

            let completedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 3`
            const getCompletedTimeSlots = await connection.query(completedTimeSlots)

            let postLang = `select l.name,l.id from post_languages left join languages as l on post_languages.language_id=l.id where post_languages.post_id=?`
            const getPostLan = await connection.query(postLang, [post_id])

            let convId = ''
            let conId = `select * from message where booking_id=?`
            const conversation_id = await connection.query(conId, [booking_id]) 
            if(conversation_id.length != 0){
                convId = conversation_id[0].conversation_id
            }

            let bookingReviews = await Common.bookingReviews(booking_id)

            return {booking_detail:bookingDetail[0],accepted_time_slots:getAcceptedTimeSlots,payment_pending_timeslots:getPaymentPendingTimeSlots,pending_time_slots:getPendingTimeSlots,cancelled_time_slots:getCancelledTimeSlots,declined_time_slots:getDeclinedTimeslots,completed_time_slots:getCompletedTimeSlots,post_languages:getPostLan,conversation_id:convId,reviews:bookingReviews}
        }
        
    }
    
    
    
    
}

module.exports = TeacherService;