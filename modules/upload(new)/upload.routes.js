var express = require('express');
var router = express.Router();
const multer = require('multer');
var cors = require("cors")
const { checkSchema } = require('express-validator');
let uploadSchema = require("./upload.validation.json")
const valid = require('../../config/validator')
const UploadController = require('./upload.controller')
const upload = multer({
    fileFilter(req, file, cb) {
      cb(null, true);
    }
  });

//upload image

router.post('/upload/aws', upload.single('file'), UploadController.uploadImageToAws)

module.exports = router;
