const sharp = require('sharp');
require('dotenv').config();
const path = require('path');
const PIXELS_SMALL = 100;
const PIXELS_MEDIUM = 250;
const fs = require('fs');
const FOLDER_ORIG = "orig";
const FOLDER_SMALL = "small";
const FOLDER_MEDIUM = "medium";
const util = require('util');
const getStream = require('into-stream');
let connection = require('../../config/connection')

class UploadService {

  //Create random filename
  static async createRandomFilename(file) {
    let name = file.originalname.split(' ').join('-');
    return Date.now() + '-' + name;
  }

  // Sharp image
  static async sharpFile(file, pixels = null) {
    if (pixels) {
      return await sharp(file.buffer).resize(pixels).rotate().toBuffer();
    } else {
      return await sharp(file.buffer).rotate().toBuffer();
    }
  }

  //configuration
  static async awsConfig() {
    const AWS = require('aws-sdk');
    AWS.config.update({
      accessKeyId: process.env.DO_ACCESS_KEY,
      secretAccessKey: process.env.DO_SECRET_ACCESS_KEY,
      region: process.env.DO_REGION
    });

    const s3 = new AWS.S3({
      endpoint: process.env.DO_ENDPOINT
    });

    return s3;
  }

  static async azureConfig() {
    var azureStorageConfig = {
      accountName: process.env.AZURE_STORAGE_CONNECTION_STRING,
      accountKey: process.env.AZURE_ACCOUNT_KEY,
      blobURL: ""
    };
    var azure = require('azure-storage');
    var blobService = azure.createBlobService(
      azureStorageConfig.accountName,
      azureStorageConfig.accountKey
    );
    return blobService;
  }

  static async gcConfig() {
    const projectId = process.env.GC_PROJECT_ID
    const keyFilename = process.env.GC_KEY_FILENAME
    const { Storage } = require('@google-cloud/storage');
    const storage = new Storage({ projectId, keyFilename });
    return storage
  }

  static async s3PutObject(data, filename_with_path,) {
    // Digital Ocean and AWS Cloud S3 upload
    let s3 = await this.awsConfig();
    await s3.putObject({
      Body: data,
      Bucket: process.env.BUCKET_NAME,
      Key: filename_with_path,
      ACL: 'public-read'
    }).promise();
  }

  static async azurePutObject(data, folder, filename) {
    const stream = await getStream(data);
    var streamLength = data.length;

    let blobService = await this.azureConfig();

    //  await blobService.createContainerIfNotExists(folder, {
    //     publicAccessLevel: 'blob',
    //   })

    //Create container if not exists
    await util.promisify(blobService.createContainerIfNotExists.bind(blobService))(folder, {
      publicAccessLevel: 'blob',
    })

    //Create blob

    await util.promisify(blobService.createBlockBlobFromStream.bind(blobService))(folder, filename, stream, streamLength)
  }

  //Upload File To Local
  static async uploadImageToLocal(res,file) {
    if (!file) {
      return res.status(400).json({ error: 'bad_request', message: 'Image should not be empty.' });
    }
    const filename = await this.createRandomFilename(file);
    console.log(filename);
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
        
    //upload origional image
    await sharp(file.buffer).rotate().toFile(path.join(__dirname, '../../public/uploads/' + FOLDER_ORIG + '/' + filename))

    console.log("Path:: ", path.join(__dirname, '../../public/uploads/' + FOLDER_ORIG + '/' + filename))
    //upload small image
    await sharp(file.buffer).resize(PIXELS_SMALL).rotate().toFile(path.join(__dirname, '../../public/uploads/' + FOLDER_SMALL + '/' + filename))

    //upload medium image
    await sharp(file.buffer).resize(PIXELS_MEDIUM).rotate().toFile(path.join(__dirname, '../../public/uploads/' + FOLDER_MEDIUM + '/' + filename))
    await this.saveFileInDb(filename, 5, 4, 1)
    return ({
      filename: filename, message: 'File uploaded successfully'
    });
    }else {
      return {message:"saved"}
    }
  }


  static async getFromLocal(res, filename, folder) {
  
    folder = folder || "orig";
    console.log(folder);
    return res.sendFile(path.join(__dirname, `../../public/uploads/` + `${folder}` + `/${filename}`));
  }

  //Upload image on AWS
  static async uploadImageToAws(file) {
    if (!file) {
      return ({ error: 'bad_request', message: 'Image should not be empty.' });
    }
    let filename = await this.createRandomFilename(file);
    const data = await this.sharpFile(file);
    await this.s3PutObject(data, "Uploads/Images" + '/' + filename)
    const data1 = await this.sharpFile(file, PIXELS_SMALL);
    await this.s3PutObject(data1, "Uploads/Images/Small" + '/' + filename)
    const data2 = await this.sharpFile(file, PIXELS_MEDIUM);
    await this.s3PutObject(data2, "Uploads/Images/Medium" + '/' + filename)
    //await this.saveFileInDb(filename, 5, 2, 1)
    return ({ message: "Successfully uploaded image", filename: filename })
  }

  //Get image from AWS
  static async getFromAws(filename, folder) {
    let s3 = await this.awsConfig();
    folder = folder || FOLDER_ORIG;
    const params = {
      Bucket: process.env.BUCKET_NAME,
      Key: folder + '/' + filename
    };
    return await s3.getObject(params).promise();
  }


  //upload on Azure

  static async uploadImageToAzure(file, res) {
    if (!file) {
      return res.status(400).json({ error: 'bad_request', message: 'Image should not be empty.' });
    }
    const filename = await this.createRandomFilename(file);
    console.log(filename)
    const data = await this.sharpFile(file);
    await this.azurePutObject(data, FOLDER_ORIG, filename);

    const data1 = await this.sharpFile(file, PIXELS_SMALL);
    await this.azurePutObject(data1, FOLDER_SMALL, filename);

    const data2 = await this.sharpFile(file, PIXELS_MEDIUM);
    await this.azurePutObject(data2, FOLDER_MEDIUM, filename);
    await this.saveFileInDb(filename, 3, 4, 1)
    return res.status(200).json({ filename: filename, message: 'file Uploaded' });
  }

  // static async getAzure(folder, filename) {
  //   let blobService = await this.azureConfig();

  //   var startDate = new Date();
  //   var expiryDate = new Date(startDate);
  //   expiryDate.setMinutes(startDate.getMinutes() + 100);
  //   startDate.setMinutes(startDate.getMinutes() - 100);

  //   var sharedAccessPolicy = {
  //     AccessPolicy: {
  //       Permissions: azureStorage.BlobUtilities.SharedAccessPermissions.READ,
  //       Start: startDate,
  //       Expiry: expiryDate,
  //     },
  //   };
  //   var token = blobService.generateSharedAccessSignature(
  //     folder,
  //     filename,
  //     sharedAccessPolicy,
  //   );
  //   var sasUrl = blobService.getUrl(folder, filename);
  //   console.log(sasUrl);
  //   return sasUrl || 'gfhggyjgjy';

  // }


  static async uploadImageToGc(file) {
    if (!file) {
      return ({ error: 'bad_request', message: 'Image should not be empty.' });
    }
    const filename = await this.createRandomFilename(file);
    const data = await this.sharpFile(file);
    await this.gcSharpUpload(data, FOLDER_ORIG, filename);

    const data1 = await this.sharpFile(file, PIXELS_SMALL);
    await this.gcSharpUpload(data1, FOLDER_SMALL, filename);

    const data2 = await this.sharpFile(file, PIXELS_MEDIUM);
    await this.gcSharpUpload(data2, FOLDER_MEDIUM, filename);

    return ({ message: "File Uploaded", filename: filename });
  }

  static async gcSharpUpload(data, folder, filename) {
    let storage = await this.gcConfig();

    fs.mkdirSync(folder, { recursive: true })

    await fs.promises.writeFile(folder + '/' + filename, data);

    //await fs.writeFile(folder+'/' + filename,data)
    await storage.bucket(process.env.GC_BUCKET).upload(folder + '/' + filename, {
      // Support for HTTP requests made with `Accept-Encoding: gzip`
      gzip: true,
      destination: folder + '/' + filename,
      metadata: {
        cacheControl: 'public, max-age=31536000',
      },
    });
  }


  static async getFromGc(filename, folder, res) {
    if (!folder) {
      folder = FOLDER_ORIG
    }
    let data = await fs.promises.readFile(folder + filename)
    return data;
  }


  static async saveFileInDb(filename, storage_type, environment, is_default_asset) {
    var sqlQuery = 'insert into files(id, storage_type, environment, is_default_asset) values (?,?,?,?)';
    let params = [filename, storage_type, environment, is_default_asset]
    return await connection.query(sqlQuery, params)

  }
}
module.exports = UploadService;