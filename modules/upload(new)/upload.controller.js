require('dotenv').config();
const multer = require('multer');
const sharp = require('sharp');
//const path = require('path');
var AWS = require('aws-sdk');
// const {
//     BlobServiceClient,
//     StorageSharedKeyCredential,
//     newPipeline
// } = require('@azure/storage-blob');

// const sharedKeyCredential = new StorageSharedKeyCredential(
//     process.env.AZURE_STORAGE_ACCOUNT_NAME,
//     process.env.AZURE_STORAGE_ACCOUNT_ACCESS_KEY);

// const pipeline = newPipeline(sharedKeyCredential);

// const blobServiceClient = new BlobServiceClient(
//     `https://${process.env.AZURE_STORAGE_ACCOUNT_NAME}.blob.core.windows.net`,
//     pipeline
// );

const containerName = 'files'

const { Storage } = require('@google-cloud/storage');
const logger = require('../../config/logger')
const UploadService = require('./upload.service')
const path = require('path');
const fs = require('fs');
const { dirname } = require('path');
const BUCKET_NAME = process.env.BUCKET_NAME;

const storage = new Storage();

//const account = "";
// const defaultAzureCredential = new DefaultAzureCredential();

// const blobServiceClient = new BlobServiceClient(
//     `https://${account}.blob.core.windows.net`,
//     defaultAzureCredential
// );

AWS.config.update({
    accessKeyId: process.env.DO_ACCESS_KEY,
    secretAccessKey: process.env.DO_SECRET_ACCESS_KEY,
    region: process.env.DO_REGION
});
var s3 = new AWS.S3({
    endpoint: "https://sgp1.digitaloceanspaces.com"
});

class UploadController {

    
    

    

    static async uploadImageToAws(req, res, next) {
        try {
            var file = req.file
            console.log("File: ", file)

            const result = await UploadService.uploadImageToAws(file);
            if (result) {
                return res.status(200).json(result);
            }
        }
        catch (err) {
            console.log(err)
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }
}
module.exports = UploadController;
