require('dotenv').config();
const logger = require('../../config/logger'); // importiing winston logger module
const jwt = require('jsonwebtoken')
const AdminService = require("./admin.service");
const path = require("path")
class AdminController {
  static async login(req, res, next) {
    try {
      let email = req.body.email;
      let password = req.body.password
      let user = await AdminService.login(email, password);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async dashboard(req, res, next) {

    try {
      let students = await AdminService.getSudents(null, null, null,null)
      let teachers = await AdminService.getTeachers(null, null, null,null);
      let requests = await AdminService.getRequests(null, null, null);
      let nationalities = await AdminService.getNationalities(null, null, null);
      let languages = await AdminService.getLanguages(null, null, null);
      let contacts = await AdminService.getContacts(null, null, null,null);
      let pending = await AdminService.getBookingList(null, null, null,1)
      let accepted = await AdminService.getBookingList(null, null, null,2)
      let declined = await AdminService.getBookingList(null, null, null,3)
      let paymentDone = await AdminService.getBookingList(null, null, null,4)
      let completed = await AdminService.getBookingList(null, null, null,5)
      
      return res.status(200).json({ student_count: students.count, teacher_count: teachers.count, requests_count: requests.count, nationalities_count: nationalities.count,languages_count:languages.count,contact_count:contacts.count,pending_bookings:pending.count,accepted_bookings:accepted.count,declined_booking:declined.count,paymentDone_booking:paymentDone.count,completed_bookings:completed.count })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }

  }
  static async getAllUsers(req, res, next){
    try {
      const result = await AdminService.getAllUsers()
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async updateProfile(req, res, next) {
    try {
      let first_name = req.body.first_name;
      let last_name = req.body.last_name;
      let profile_image = req.body.profile_image;
      let logged_id = req.user.id
      const result = await AdminService.updateProfile(logged_id, first_name, last_name, profile_image)
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async changePassword(req, res, next) {
    try {
      let logged_id = req.body.id;
      let old_password = req.body.old_password;
      let new_password = req.body.new_password;
      console.log(logged_id)
      const result = await AdminService.changePassword(new_password, old_password, logged_id);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getProfile(req, res, next) {
    try {
      let user_id = req.user.id;
      let user = await AdminService.getProfile(user_id)
      return res.status(200).json(user[0])
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  // User Managemant 
  static async getUsers(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      let type = req.query.type;
      let is_blocked = req.query.is_blocked;
      console.log(limit + ' ' + page);
      let users = await AdminService.getUsers(keyword, page, limit, type, is_blocked)

      return res.status(200).json(users)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //User management
  static async getSudents(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      let is_blocked = req.query.is_blocked;
      const user = await AdminService.getSudents(keyword,limit,page,is_blocked);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addStudent(req, res, next){
    try {
      let first_name = req.body.first_name;
      let last_name = req.body.last_name;
      let email = req.body.email;
      let password = req.body.password;
      let user_type = 1;
      let fcm_id = req.body.fcm_id;
      let device_id = req.body.device_id;
      let device_type = req.body.device_type;
      const user = await AdminService.addStudent(first_name,last_name,email,user_type,fcm_id,device_id,device_type,password);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addTeacher(req, res, next){
    try {
      let first_name = req.body.first_name;
      let last_name = req.body.last_name;
      let email = req.body.email;
      let password = req.body.password;
      let user_type = 2;
      let fcm_id = req.body.fcm_id;
      let device_id = req.body.device_id;
      let device_type = req.body.device_type;
      const user = await AdminService.addTeacher(first_name,last_name,email,user_type,fcm_id,device_id,device_type,password);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getTeachers(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      let is_blocked = req.query.is_blocked;
      const user = await AdminService.getTeachers(keyword,limit,page,is_blocked);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  
  static async getUserDetail(req, res, next){
    try {
      let id = req.query.id
      const user = await AdminService.getUserDetail(id);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async blockUser(req, res, next) {
    try {
      let id = req.body.id;
      let unblock = req.body.unblock;
      const result = await AdminService.blockUser(id, unblock);
      if (result) {
        return res.status(200).json({ message: 'block status updated' })
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async actDeactUser(req, res, next) {
    try {
      let id = req.body.id;
      let deactivate = req.body.deactivate;
      const response = await AdminService.actDeactUser(id,deactivate);
      if (response) {
        return res.status(200).json({ message: 'deactivate status updated' })
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteUser(req, res, next) {
    try {
      let id = req.body.id;
      const response = await AdminService.deleteUser(id);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async verifyTeacher(req, res, next){
    try {
      let teacher_id = req.body.id;
      const response = await AdminService.deleteUser(id);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  /*======Post management======*/
  static async getPosts(req, res, next){
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      let teacher_id = req.query.teacher_id;
      const lan = await AdminService.getPosts(keyword,limit,page,teacher_id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getPostDetail(req, res, next){
    try {
      let post_id = req.query.post_id
      let post = {}
      post.postDetail = await AdminService.getPostDetail(post_id)
      post.language = await AdminService.getPostLanguage(post_id)
      post.timeSlot = await AdminService.getPostTimeSlot(post_id)
      return res.status(200).json(post)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async blockPost(req, res, next){
    try {
      let post_id = req.body.post_id
      let block = req.body.block
      let request  = await AdminService.blockPost(post_id,block);
      return res.status(200).json(request)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }

  /*======Request management======*/
  static async getRequests(req, res, next){
    try {
      let limit = req.query.limit;
      let page = req.query.page;
      let keyword = req.query.keyword;
      let student_id = req.query.student_id;
      let request  = await AdminService.getRequests(limit,page,keyword,student_id);
      return res.status(200).json(request)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getRequestDetail(req, res, next){
    try {
      let request_id = req.query.request_id
      let request = {}
      request.requestDetail = await AdminService.getRequestDetail(request_id)
      request.language = await AdminService.getRequestLanguage(request_id)
      request.timeSlot = await AdminService.getRequestTimeSlot(request_id)
      request.responses = await AdminService.getResponses(request_id)
      return res.status(200).json(request)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async blockRequest(req, res, next){
    try {
      let request_id = req.body.request_id
      let block = req.body.block
      let request  = await AdminService.blockRequest(request_id,block);
      return res.status(200).json(request)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }

  /*========Popular filter management=======*/
  static async getAllStaticPagesData(req, res, next){
    try {
      let staticData = {}
      staticData['nationalities'] = await AdminService.getNationality();
      staticData['languages'] = await AdminService.getLanguage();
      staticData['subjects'] = await AdminService.getSubject();
      staticData['teaching_standards'] = await AdminService.getTeachingStandard();
      return res.status(200).json(staticData)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addPopular(req, res, next){
    try {
      let parent = req.body.parent
      let name = req.body.name
      let id = req.body.id
      let status = req.body.status
      let request  = await AdminService.addPopular(parent,name,id,status);
      return res.status(200).json(request)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }

  /*=======Language management=========*/
  static async getLanguages(req, res, next){
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const lan = await AdminService.getLanguages(keyword,limit,page);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getLanDetail(req, res, next){
    try {
      let language_id = req.query.language_id;
      const lan = await AdminService.getLanDetail(language_id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addLanguage(req, res, next){
    try {
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.addLanguage(name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editLanguage(req, res, next){
    try {
      let id = req.body.id;
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.editLanguage(id,name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteLanguage(req, res, next){
    try {
      let id = req.body.id;
      const lan = await AdminService.deleteLanguage(id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  /*========Occupation management========*/
  static async getOccupations(req, res, next){
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const result = await AdminService.getOccupations(keyword,limit,page);
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getOccupationDetail(req, res, next){
    try {
      let occupation_id = req.query.occupation_id;
      const lan = await AdminService.getOccupationDetail(occupation_id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addOccupation(req, res, next){
    try {
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.addOccupation(name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editOccupation(req, res, next){
    try {
      let id = req.body.id;
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.editOccupation(id,name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteOccupation(req, res, next){
    try {
      let id = req.body.id;
      const lan = await AdminService.deleteOccupation(id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  /*=========Nationalitites management=========*/
  static async getNationalities(req, res, next){
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const result = await AdminService.getNationalities(keyword,limit,page);
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getNationalityDetail(req, res, next){
    try {
      let nationality_id = req.query.nationality_id;
      const nat = await AdminService.getNationalityDetail(nationality_id);
      return res.status(200).json(nat)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addNationality(req, res, next){
    try {
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.addNationality(name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editNationality(req, res, next){
    try {
      let id = req.body.id;
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.editNationality(id,name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteNationality(req, res, next){
    try {
      let id = req.body.id;
      const lan = await AdminService.deleteNationality(id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  /*=======Teaching Standards management=======*/
  static async getTeachingStandards(req, res, next){
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const result = await AdminService.getTeachingStandards(keyword,limit,page);
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getTeachingStandardDetail(req, res, next){
    try {
      let teaching_id = req.query.teaching_id;
      const nat = await AdminService.getTeachingStandardDetail(teaching_id);
      return res.status(200).json(nat)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addTeachingStand(req, res, next){
    try {
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.addTeachingStand(name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editTeachingStand(req, res, next){
    try {
      let id = req.body.id;
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.editTeachingStand(id,name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteTeachingStandard(req, res, next){
    try {
      let id = req.body.id;
      const lan = await AdminService.deleteTeachingStandard(id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  /*=======Residing Country management=======*/
  static async getResidingCountries(req, res, next){
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const result = await AdminService.getResidingCountries(keyword,limit,page);
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getResidingCountry(req, res, next){
    try {
      let country_id = req.query.country_id;
      const nat = await AdminService.getResidingCountry(country_id);
      return res.status(200).json(nat)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addResidingCountry(req, res, next){
    try {
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.addResidingCountry(name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editResidingCountry(req, res, next){
    try {
      let id = req.body.id;
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.editResidingCountry(id,name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteResidingCountry(req, res, next){
    try {
      let id = req.body.id;
      const lan = await AdminService.deleteResidingCountry(id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  /*=======Subject management=======*/
  static async getSubjects(req, res, next){
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const result = await AdminService.getSubjects(keyword,limit,page);
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getSubjectDetail(req, res, next){
    try {
      let subject_id = req.query.subject_id;
      const nat = await AdminService.getSubjectDetail(subject_id);
      return res.status(200).json(nat)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addSubject(req, res, next){
    try {
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.addSubject(name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editSubject(req, res, next){
    try {
      let id = req.body.id;
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.editSubject(id,name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteSubject(req, res, next){
    try {
      let id = req.body.id;
      const lan = await AdminService.deleteSubject(id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  
  /*=======Education level management=======*/
  static async getEducationLevels(req, res, next){
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const result = await AdminService.getEducationLevels(keyword,limit,page);
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getEducationLevelDetail(req, res, next){
    try {
      let education_id = req.query.education_id;
      const nat = await AdminService.getEducationLevelDetail(education_id);
      return res.status(200).json(nat)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addEducationLevel(req, res, next){
    try {
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.addEducationLevel(name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editEducationLevel(req, res, next){
    try {
      let id = req.body.id;
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.editEducationLevel(id,name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteEducationLevel(req, res, next){
    try {
      let id = req.body.id;
      const lan = await AdminService.deleteEducationLevel(id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  /*=======Complain subject management=======*/
  static async getComplainSubjects(req, res, next){
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const result = await AdminService.getComplainSubjects(keyword,limit,page);
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getComplainSubjectDetail(req, res, next){
    try {
      let subject_id = req.query.subject_id;
      const nat = await AdminService.getComplainSubjectDetail(subject_id);
      return res.status(200).json(nat)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addComplainSubject(req, res, next){
    try {
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.addComplainSubject(name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editComplainSubject(req, res, next){
    try {
      let id = req.body.id;
      let name = req.body.name;
      let name_jp = req.body.name_jp;
      const lan = await AdminService.editComplainSubject(id,name,name_jp);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteComplainSubject(req, res, next){
    try {
      let id = req.body.id;
      const lan = await AdminService.deleteComplainSubject(id);
      return res.status(200).json(lan)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  /*======Booking management======*/
  static async getBookingComplains(req, res, next){
    try {
      let page = req.query.page
      let limit = req.query.limit
      let keyword = req.query.keyword
      var requests = await AdminService.getBookingComplains(page,limit,keyword)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getBookingTimeSlotComplains(req, res, next){
    try {
      let page = req.query.page
      let limit = req.query.limit
      let keyword = req.query.keyword
      var requests = await AdminService.getBookingTimeSlotComplains(page,limit,keyword)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getBookings(req, res, next){
    try {
      let page = req.query.page
      let limit = req.query.limit
      let keyword = req.query.keyword
      let status = req.query.status
      var requests = await AdminService.getBookingList(page,limit,keyword,status)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getBookingDetail(req, res, next){
    try {
      let booking_id = req.query.booking_id
      var requests = await AdminService.getBookingDetail(booking_id)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }

  /*=======transaction module========*/
  static async getTransactions(req, res, next){
    try {
      let page = req.query.page
      let limit = req.query.limit
      let keyword = req.query.keyword
      let booking_id = req.query.booking_id
      var requests = await AdminService.getTransactions(page,limit,keyword,booking_id)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getTransactionDetail(req, res, next){
    try {
      let transaction_id = req.query.transaction_id
      var requests = await AdminService.getTransactionDetail(transaction_id)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }

  /*======Paypal module========*/
  static async getPaypalTeachers(req, res, next){
    try {
      let page = req.query.page
      let limit = req.query.limit
      let keyword = req.query.keyword
      var requests = await AdminService.getPaypalTeachers(page,limit,keyword)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getPaypalTransactions(req, res, next){
    try {
      let id = req.query.id
      let page = req.query.page
      let limit = req.query.limit
      var requests = await AdminService.getPaypalTransactions(page,limit,id)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async transferAmount(req, res, next){
    try {
      let id = req.body.id
      var requests = await AdminService.transferAmount(id)
      return res.status(200).json(requests)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
 
  /*======Staff management========*/
  static async editUserDetail(req, res, next) {
    try {
      let logged_id = req.body.id
      let first_name = req.body.first_name
      let last_name = req.body.last_name
      let email = req.body.email
      let phone_number = req.body.phone_number
      let country_code = req.body.country_code
      let profile_image = req.body.profile_image
      let state = req.body.state
      let country = req.body.country
      let city = req.body.city
      let dob = req.body.dob
      const result = await AdminService.editUserDetail(logged_id, first_name, last_name, email, phone_number, country_code, profile_image, state, country, city,dob);
      if (result) {
        return res.status(200).json({ message: "staff profile updated" });
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  

  // Project Management

  static async getProjects(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      let user_id = req.query.user_id;
      const projects = await AdminService.getProjects(keyword, limit, page, user_id);
      return res.status(200).json(projects)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getProjectDetail(req, res, next) {
    try {
      let project_id = req.query.project_id
      const project = await AdminService.getProjectDetail(project_id);
      return res.status(200).json(project)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async editProject(req, res, next) {
    try {
      let id = req.body.id;
      let title = req.body.title;
      let description = req.body.description;
      let price = req.body.price;
      let location = req.body.location;
      let images = req.body.images;
      let status = req.body.status;
      await AdminService.editProject(id, title, description, price, location, images, status);
      return res.status(200).json({
        message: 'Project Updated'
      })

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  static async addProject(req, res, next) {
    try {
      let title = req.body.title;
      let description = req.body.description;
      let price = req.body.price;
      let location = req.body.location;
      let images = req.body.images;
      var result = await AdminService.addProject(title, description, price, location, images);
      return res.status(200).json(result)

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  // Staff Management
  static async addUser(req, res, next) {
    try {
      let first_name = req.body.first_name;
      let last_name = req.body.last_name;
      let email = req.body.email;
      let password = req.body.password;
      let profile_image = req.body.profile_image;
      let city = req.body.city;
      let state = req.body.state;
      let country = req.body.country;
      let country_code = req.body.country_code;
      let address = req.body.address;
      let phone_number = req.body.phone_number;
      let dob = req.body.dob;
      let type = req.body.type;
      let description = req.body.description;
      console.log(req.body)
      const result = await AdminService.addUser(first_name, last_name, email, password, profile_image, city, state, country, country_code, address, phone_number, type, dob, description);
      if (result) {
        return res.status(200).json(result);
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async staffList(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      console.log(limit + ' ' + page);
      let users = await AdminService.getStaffList(keyword, page, limit)
      return res.status(200).json(users)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getStaffDetail(req, res, next) {
    try {
      let id = req.query.id;
      let type = req.query.type;
      const user = await AdminService.getStaffDetail(id, type);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  

  

  static async verifyUser(req, res, next) {
    try {
      let id = req.body.id;
      let is_verified = req.body.is_verified;
      let logged_user = req.user.user_type;
      console.log("type", logged_user)
      const response = await AdminService.verifyUser(id, logged_user, is_verified);
      if (response) {
        return res.status(200).json("User verified status changed.")
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async blockStaff(req, res, next) {
    try {
      console.log(1)
      let id = req.body.id;
      let unblock = req.body.unblock
      const result = await AdminService.blockUser(id, unblock);
      if (result) {
        console.log(3)
        return res.status(200).json({ message: 'Block status updated' })
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deleteProject(req, res, next) {
    try {
      let project_id = req.query.id;
      await AdminService.deleteProject(project_id);
      return res.status(200).json({
        message: 'Project Deleted.'
      })
    }
    catch (err) {
      console.log(err)
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  // Content Management

  //add content
  static async addContent(req, res, next) {
    try {
      let title = req.body.title;
      let content = req.body.content;
      let image = req.body.image;
      console.log(req.body)
      const result = await AdminService.addContent(title, content, image);
      if (result) {
        return res.status(200).json(result);
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Get Contents
  static async getContents(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const contents = await AdminService.getContents(keyword, limit, page);
      return res.status(200).json(contents)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Get Content Details
  static async getContentDetail(req, res, next) {
    try {
      let id = req.query.content_id
      const content = await AdminService.getContentDetail(id);
      return res.status(200).json(content[0])
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Edit content
  static async editContent(req, res, next) {
    try {
      let id = req.body.id;
      let title = req.body.title;
      let title_jp = req.body.title_jp;
      let content = req.body.content;
      let content_jp = req.body.content_jp;
      let image = req.body.image;
      await AdminService.editContent(id, title,title_jp, content,content_jp, image);
      return res.status(200).json({
        message: 'content Updated.'
      })

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Delete content
  static async deleteContent(req, res, next) {
    try {
      let content_id = req.body.id;
      await AdminService.deleteContent(content_id);
      return res.status(200).json({
        message: 'content Deleted.'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Delete content
  

  //Send Mail
  static async sendNotification(req, res, next) {
    try {
      let user_id = req.user.id
      let email = req.body.emails;
      let subject = req.body.subject;
      let email_content = req.body.email_content;
      let type = req.body.type;
      let notification_type = req.body.notification_type;
      if (type == 0) {
        await AdminService.saveMessage(type, null, subject, email_content, user_id)
      } else {
        await AdminService.saveMessage(type, email, subject, email_content, user_id)
      }
      if (type == 0 && notification_type == 0) {

        await AdminService.sendMail(null, subject, email_content);

      } else if (type == 1 && notification_type == 0) {

        await AdminService.sendMail(email, subject, email_content);

      } else if (type == 0 && notification_type == 1) {

        await AdminService.sendPush(null, subject, push_content);

      } else if (type == 1 && notification_type == 1) {

        await AdminService.sendPush(email, subject, push_content);

      } else if (type == 1 && (notification_type == null || notification_type == 2)) {

        await AdminService.sendPush(email, subject, push_content);
        await AdminService.sendMail(email, subject, email_content);

      } else if (type == 0 && (notification_type == null || notification_type == 2)) {

        await AdminService.sendPush(null, subject, push_content);
        await AdminService.sendMail(null, subject, email_content);

      }

      return res.status(200).json({
        message: 'Mail Sent Successfully'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  /*======Contact management======*/
  static async getContacts(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      let status = req.query.status
      const contacts = await AdminService.getContacts(keyword, limit, page, status);
      return res.status(200).json(contacts)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteContact(req, res, next) {
    try {
      let contact_id = req.body.id;
      await AdminService.deleteContact(contact_id);
      return res.status(200).json({
        message: 'contact deleted.'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async resolveContact(req, res, next){
    try {
      let contact_id = req.body.contact_id;
      await AdminService.resolveContact(contact_id);
      return res.status(200).json({
        message: 'contact resolved.'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //DB Backup
  static async dbBackup(req, res, next) {
    try {
      const response = await AdminService.dbBackup();
      return res.status(200).json({ dbname: response })
      // if (response) {
      //   return res.status(200).json("Backup Successfully Completed.")
      // }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

}
module.exports = AdminController;