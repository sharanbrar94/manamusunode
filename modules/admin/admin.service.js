const { sign } = require('jsonwebtoken');
const nodemailer = require('nodemailer')
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const logger = require('../../config/logger');
const childProcess = require('child_process')
const fs = require('fs')
const moment = require('moment');
let Common = require("../../middleware/common")
const stripe = require('stripe')('sk_test_8N9H8KPT7YIZ3vRCOPbWkIlc');
class AdminService {
    static async login(email, password) {
        let user = await this.getAdminDetail('email', email);
        console.log(user.length)
        if (user.length != 0) {
            if (user[0] && user[0].is_blocked == 0 && user[0].is_deactivated == 0) {
                let passwordResult = compareSync(password, user[0].password);
                if (!passwordResult) {
                    throw ("Please enter correct password");
                }
            } else {
                throw ("You are blocked or deactivated by the admin.");
            }
        } else {
            throw ("Please enter registered email id.")
        }
        delete user[0].password;
        let auth_token = await this.generateToken(user);
        return ({ 'message': "Login successfully", 'token': auth_token, 'user': user[0] });
    }
    static async getAllUsers(){
        let sqlQuery = `select id,email,first_name,last_name from users where user_type=1 or user_type=2 `
        return await connection.query(sqlQuery)
    }

    static async updateProfile(logged_id, first_name, last_name, profile_image) {
        let sqlQuery = 'update users set '
        let params = []
        let sql = []
        if (first_name != undefined) {
            params.push(first_name)
            sql.push('first_name=?')
        }
        if (last_name != undefined) {
            params.push(last_name)
            sql.push('last_name=?')
        }
        if (profile_image != undefined) {
            params.push(profile_image)
            sql.push('profile_image=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(logged_id)
        console.log(logged_id)
        await connection.query(sqlQuery, params)
        return { message: "Profile updated successfully" }
    }

    static async changePassword(new_password, old_password, logged_id) {
        const user = await this.getAdminDetail('id', logged_id)
        let result = compareSync(old_password, user[0].password);
        if (!result) {
            throw ('Please enter correct old password.');
        } else {
            const salt = genSaltSync(10);
            let password = hashSync(new_password, salt);
            let sqlQuery = `update users set password=? where id=?`
            let params = [password, logged_id]
            const result = await connection.query(sqlQuery, params);
            if (result) {
                return ({ message: "Password changed" });
            }
        }
    }

    static async getProfile(id) {
        let sqlQuery = `select * from users where id=${id}`
        return await connection.query(sqlQuery)

    }
    static async getUserByEmail(email) {
        let sqlQuery = `select * from users where email=? `
        return await connection.query(sqlQuery, [email])

    }
    static async generateToken(user) {
        let token = await sign({ user: user[0] }, 'secretkey', { expiresIn: "2Days" });
        return token

    }

    //student management
    static async getSudents(keyword,limit,page,is_blocked) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select users.*,c.name as country_name_en,c.name as country_name_jp from users left join counties as c on users.country = c.id where users.user_type=1 and users.deleted_at is null `
        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or concat(users.first_name, ' ', users.last_name) like  '%${keyword}%'  or users.email like  '%${keyword}%') `
        }
        if (is_blocked) {
            sqlQuery += ` and users.is_blocked = ${is_blocked} `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
        const users = await connection.query(sqlQuery);

        let countSqlQuery = `select * from users where user_type=1 and deleted_at is null`
        if (keyword) {
            countSqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or concat(users.first_name, ' ', users.last_name) like  '%${keyword}%'  or users.email like  '%${keyword}%') `
        }
        if (is_blocked) {
            countSqlQuery += ` and users.is_blocked = ${is_blocked} `
        }
        const count = await connection.query(countSqlQuery);
        return {count:count.length,students:users}
    }
    //start add student by admin//
    static async addStudent(first_name,last_name,email,user_type,fcm_id,device_id,device_type,password){
        let user = await this.getUserProfileByEmail(email);
        if (user[0]) {
            throw ("Email already exist!");
        }
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        const email_verify_token = await Common.generateRandomString(15);
        var sqlQuery = 'insert into users(first_name, last_name, email,password,user_type,email_verify_token) values (?,?,?,?,?,?)';
        let params = [first_name, last_name, email, password, user_type,email_verify_token]
        let userInsert = await connection.query(sqlQuery, params);
        //add in stripe
        await this.createCustomer(userInsert.insertId,email)
        await this.insertOnUpdateDeviceInfo(device_id, fcm_id, device_type, userInsert.insertId)
        return ({ 'message': "Student added successfully"})
    }
    static async createCustomer(user_id,email_address){
        const customer = await stripe.customers.create({
            email : email_address
        });
        let customer_id = customer['id'];
        var stripeQuery = 'update users set customer_id=? where id=?';
        let stripeParams = [customer_id,user_id]
        return await connection.query(stripeQuery, stripeParams)
    }
    static async getUserProfileByEmail(email) {
        let sqlQuery = `select * from users where email=?`
        return await connection.query(sqlQuery, [email])
    }
    static async insertOnUpdateDeviceInfo(device_id, fcm_id, device_type, user_id) {
        let expired_at = null;
        let sqlQuery = `insert into user_logins(user_id,device_id,fcm_id,device_type,expired_at) values(?,?,?,?,?) ON DUPLICATE KEY UPDATE user_id=?,fcm_id=?,device_type=?,expired_at=?`
        let params = [user_id, device_id, fcm_id, device_type, expired_at, user_id, fcm_id, device_type, expired_at]
        return await connection.query(sqlQuery, params)
    }
    //end add student by admin//
    static async getUserDetail(id){
        let sqlQuery = `select users.*,c.name as country_name_en,c.name_jp as country_name_jp,n.name as nationality_name_en,n.name_jp as nationality_name_jp from users left join counties as c on users.country = c.id left join nationalities as n on users.nationality = n.id where users.id=?`
        const user = await connection.query(sqlQuery, [id]);
        return user;
    }
    static async blockUser(id, unblock) {
        let sqlQuery = 'update users set users.is_blocked=? where users.id=? '
        return await connection.query(sqlQuery, [unblock, id])
    }
    static async actDeactUser(id,deactivate){
        let sqlQuery = 'update users set users.is_deactivated=? where users.id=? '
        return await connection.query(sqlQuery, [deactivate, id])
    }
    static async deleteUser(id) {
        var current_date = new Date()
        let sqlQuery = 'update users set deleted_at=? where id=?'
        const response = await connection.query(sqlQuery, [current_date,id])
        return { message: "Deleted" }
    }

    /*========Teacher management======*/
    static async addTeacher(first_name,last_name,email,user_type,fcm_id,device_id,device_type,password){
        let user = await this.getUserProfileByEmail(email);
        if (user[0]) {
            throw ("Email already exist!");
        }
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        const email_verify_token = await Common.generateRandomString(15);
        var sqlQuery = 'insert into users(first_name, last_name, email,password,user_type,email_verify_token) values (?,?,?,?,?,?)';
        let params = [first_name, last_name, email, password, user_type,email_verify_token]
        let userInsert = await connection.query(sqlQuery, params);
        //add in stripe
        await this.insertOnUpdateDeviceInfo(device_id, fcm_id, device_type, userInsert.insertId)
        return ({ 'message': "Teacher added successfully"})
    }
    static async getTeachers(keyword,limit,page,is_blocked) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select users.*,c.name as country_name_en,c.name_jp as country_name_jp from users left join counties as c on users.country = c.id where users.user_type=2 and users.deleted_at is null `
        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or concat(users.first_name, ' ', users.last_name) like  '%${keyword}%'  or users.email like  '%${keyword}%') `
        }
        if (is_blocked) {
            sqlQuery += ` and users.is_blocked = ${is_blocked} `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
        const users = await connection.query(sqlQuery);

        let countSqlQuery = `select * from users where user_type=2 and deleted_at is null `
        if (keyword) {
            countSqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or concat(users.first_name, ' ', users.last_name) like  '%${keyword}%'  or users.email like  '%${keyword}%') `
        }
        if (is_blocked) {
            countSqlQuery += ` and users.is_blocked = ${is_blocked} `
        }
        const count = await connection.query(countSqlQuery);
        return {count:count.length,students:users}
    }

    /*======Post manegement======*/
    static async getPosts(keyword,limit,page,teacher_id){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select posts.*,s.name as subject_name,(select Avg(rating) from reviews where reviews.post_id=posts.id) as post_rating,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from posts left join users as u on posts.user_id = u.id left join subjects as s on posts.subject = s.id where posts.type = ? and posts.deleted_at is null `
        if(teacher_id){
            sqlQuery += ` and posts.user_id = ${teacher_id}`
        }
        if(keyword){
            sqlQuery += ` and posts.title like  '%${keyword}%'`
        }
        sqlQuery += ` order by posts.id DESC LIMIT ${limit} OFFSET ${offset}`
        let posts = await connection.query(sqlQuery, ['post'])
        let countSqlQuery = `select posts.*,s.name as subject_name,(select Avg(rating) from reviews where reviews.post_id=posts.id) as post_rating,u.first_name,u.last_name,u.profile_image from posts left join users as u on posts.user_id = u.id left join subjects as s on posts.subject = s.id where posts.type = ? and posts.deleted_at is null `
        if(teacher_id){
            countSqlQuery += ` and posts.user_id = ${teacher_id}`
        }
        if(keyword){
            countSqlQuery += ` and posts.title like  '%${keyword}%'`
        }
        let count = await connection.query(countSqlQuery, ['post'])
        return {count:count.length,posts:posts}
    }
    static async getPostDetail(post_id){
        let sqlQuery = `select posts.*,s.name as subject_name,(select Avg(rating) from user_ratings where user_ratings.user_id=posts.user_id) as teacher_rating,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from posts left join users as u on posts.user_id = u.id left join subjects as s on posts.subject = s.id  where posts.id=?`
        var post = await connection.query(sqlQuery, [post_id])
        return post[0]
    }
    static async getPostLanguage(post_id){
        let sqlQuery = `select l.name,l.id from post_languages left join languages as l on post_languages.language_id=l.id where post_languages.post_id=?`
        return await connection.query(sqlQuery, [post_id])
    }
    static async getPostTimeSlot(post_id){
        let sqlQuery = `select * from post_days where post_id=?`
        var post_days =  await connection.query(sqlQuery, [post_id])
        var arr = []
        await Promise.all(post_days.map(async (x) => {
            var post = {}
            let optionQuery = `select * from post_day_slots where post_day_id = ${x.id}`
            const options = await connection.query(optionQuery)
            
            post.days = x.day;
            post.time_slot = options
            arr.push(post)
        }));
        return arr
    }
    static async blockPost(post_id,block){
        const result = await connection.query(`update posts set status=? where id = ${post_id}`, [block]);
        return ({ message: "Status updated" })
    }

    /*======Request management=======*/
    static async getRequests(limit,page,keyword,student_id){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select requests.*,s.name as subject_name,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id left join subjects as s on requests.subject = s.id where requests.deleted_at is null`
        if(keyword){
            sqlQuery += ` and requests.title like  '%${keyword}%'`
        }
        if(student_id){
            sqlQuery += ` and requests.user_id=${student_id}`
        }
        sqlQuery += ` order by requests.id DESC LIMIT ${limit} OFFSET ${offset}`
        let requests = await connection.query(sqlQuery)
        let countSqlQuery = `select requests.*,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id where requests.deleted_at is null`
        if(keyword){
            countSqlQuery += ` and requests.title like  '%${keyword}%'`
        }
        if(student_id){
            countSqlQuery += ` and requests.user_id=${student_id}`
        }
        let count = await connection.query(countSqlQuery)
        return {count:count.length,requests:requests}
    }
    static async getRequestDetail(request_id){
        let sqlQuery = `select requests.*,s.name as subject,t.name as teaching_standard,u.first_name,u.last_name,u.profile_image from requests left join subjects as s on requests.subject=s.id left join teaching_standards as t on requests.teaching_standard = t.id left join users as u on requests.user_id = u.id  where requests.id=?`
        var post = await connection.query(sqlQuery, [request_id])
        return post[0]
    }
    static async getRequestLanguage(request_id){
        let sqlQuery = `select l.name,l.id from request_languages left join languages as l on request_languages.language_id=l.id where request_languages.request_id=?`
        return await connection.query(sqlQuery, [request_id])
    }
    static async getRequestTimeSlot(request_id){
        let sqlQuery = `select * from request_days where request_id=?`
        var request_days =  await connection.query(sqlQuery, [request_id])
        var arr = []
        await Promise.all(request_days.map(async (x) => {
            var request = {}
            let optionQuery = `select * from request_day_slots where request_day_id = ${x.id}`
            const options = await connection.query(optionQuery)
            
            request.date = x.date;
            request.time_slot = options
            arr.push(request)
        }));
        return arr
    }
    static async getResponses(request_id){
        let sqlQuery = `select teacher_responses.*,(select Avg(rating) from booking_reviews where booking_reviews.to_user_id = teacher_responses.user_id) as teacher_rating,u.description as teacher_description,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from teacher_responses left join users as u on teacher_responses.user_id=u.id where teacher_responses.request_id=?`
        let responses =  await connection.query(sqlQuery, [request_id])
        var arr = []
        await Promise.all(responses.map(async (x) => {
            var booking = {}
            let optionQuery = `select r.date,d.start_time,d.end_time,d.id from teacher_accepted_time_slots left join request_day_slots as d on teacher_accepted_time_slots.request_time_slot_id = d.id left join request_days as r on d.request_day_id = r.id where teacher_accepted_time_slots.teacher_id = ${x.user_id} and teacher_accepted_time_slots.request_id = ${request_id}`
            const options = await connection.query(optionQuery)
            
            responses = x;
            responses.accepted_time_slots = options
            arr.push(responses)
        }));
        return arr
    }
    // static async getResponses(request_id){
    //     let sqlQuery = `select teacher_responses.*,(select Avg(rating) from booking_reviews where booking_reviews.to_user_id = teacher_responses.user_id) as teacher_rating,u.description as teacher_description,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from teacher_responses left join users as u on teacher_responses.user_id=u.id where teacher_responses.request_id=?`
    //     return await connection.query(sqlQuery, [request_id])
    // }
    static async blockRequest(request_id,block){
        const result = await connection.query(`update requests set request_status=? where id = ${request_id}`, [block]);
        return ({ message: "Status updated" })
    }

    /*=======Filter management=======*/
    static async getNationality(){
        let sqlQuery = `select * from nationalities where deleted_at is null`
        let nationality = await connection.query(sqlQuery)
        nationality.forEach(x=>{
            x.parent = 'nationality'
        })
        return nationality
    }
    static async getLanguage(){
        let sqlQuery = `select * from languages where deleted_at is null`
        let language = await connection.query(sqlQuery)
        language.forEach(x=>{
            x.parent = 'language'
        })
        return language
    }
    static async getSubject(){
        let sqlQuery = `select * from subjects where deleted_at is null`
        let subject = await connection.query(sqlQuery)
        subject.forEach(x=>{
            x.parent = 'subject'
        })
        return subject
    }
    static async getTeachingStandard(){
        let sqlQuery = `select * from  teaching_standards where deleted_at is null`
        let teaching_standard = await connection.query(sqlQuery)
        teaching_standard.forEach(x=>{
            x.parent = 'teaching_standard'
        })
        return teaching_standard
    }
    static async addPopular(parent,name,id,status){
        if(status == 1){
            await connection.query('insert into popular_filters set parent=?,name=?,value=?', [parent,name,id]);
        }
        if(status == 0){
            await connection.query('delete from popular_filters where parent = ? and value = ?', [parent,id])
        }
        if(parent == 'nationality'){
            await connection.query(`update nationalities set popular_status=${status} where id = ${id}`)
        }
        if(parent == 'language'){
            await connection.query(`update languages set popular_status=${status} where id = ${id}`)
        }
        if(parent == 'subject'){
            await connection.query(`update subjects set popular_status=${status} where id = ${id}`)
        }
        if(parent == 'teaching_standard'){
            await connection.query(`update teaching_standards set popular_status=${status} where id = ${id}`)
        }
        return ({message:"status updated"})
    }
    

    //Language management
    static async getLanguages(keyword,limit,page){
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select * from languages where deleted_at is null`
        if (keyword) {
            sqlQuery += ` and name like  '%${keyword}%' `
        }
        sqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const languages = await connection.query(sqlQuery);

        let countSqlQuery = `select * from languages where deleted_at is null`
        if (keyword) {
            countSqlQuery += ` and name like  '%${keyword}%' `
        }
        const count = await connection.query(countSqlQuery);
        return {count:count.length,languages:languages}
    }
    static async getLanDetail(language_id){
        let sqlQuery = `select * from languages where id=?`
        const lan = await connection.query(sqlQuery, [language_id]);
        return lan[0];
    }
    static async addLanguage(name,name_jp){
        let sqlQuery = await connection.query(`select * from languages where deleted_at is null and name = '${name}'`);
        if(sqlQuery.length > 0){
            throw("Language already added")
        }
        else{
            let getJp = await connection.query(`select * from languages where deleted_at is null and name_jp = '${name_jp}'`);
            if(getJp.length > 0){
                throw("Language already added")
            }
            else{
                const result = await connection.query('insert into languages set name=?,name_jp=?', [name,name_jp]);
                return ({ id: result.insertId, message: "Language added" })
            }
            
        }
    }
    static async editLanguage(id,name,name_jp){
        const result = await connection.query(`update languages set name=?,name_jp=? where id = ${id}`, [name,name_jp]);
        return ({ message: "Language updated" })
    }
    static async deleteLanguage(id){
        let current_time = new Date()
        const result = await connection.query(`update languages set deleted_at=? where id = ${id}`, [current_time]);
        return ({ message: "Language deleted" })
    }

    /*=======Occupation management=========*/
    static async getOccupations(keyword,limit,page){
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select * from occupations where deleted_at is null`
        if (keyword) {
            sqlQuery += ` and name like  '%${keyword}%' `
        }
        sqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const occupations = await connection.query(sqlQuery);

        let countSqlQuery = `select * from occupations where deleted_at is null`
        if (keyword) {
            countSqlQuery += ` and name like  '%${keyword}%' `
        }
        const count = await connection.query(countSqlQuery);
        return {count:count.length,occupations:occupations}
    }
    static async getOccupationDetail(occupation_id){
        let sqlQuery = `select * from occupations where id=?`
        const occu = await connection.query(sqlQuery, [occupation_id]);
        return occu[0];
    }
    static async addOccupation(name,name_jp){
        const result = await connection.query('insert into occupations set name=?,name_jp=?', [name,name_jp]);
        return ({ id: result.insertId, message: "Occupation added" })
    }
    static async editOccupation(id,name,name_jp){
        const result = await connection.query(`update occupations set name=?,name_jp=? where id = ${id}`, [name,name_jp]);
        return ({ message: "Occupation updated" })
    }
    static async deleteOccupation(id){
        let current_time = new Date()
        const result = await connection.query(`update occupations set deleted_at=? where id = ${id}`, [current_time]);
        return ({ message: "Occupation deleted" })
    }

    /*=======residing country management=========*/
    static async getResidingCountries(keyword,limit,page){
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select * from counties where deleted_at is null`
        if (keyword) {
            sqlQuery += ` and name like  '%${keyword}%' `
        }
        sqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const counties = await connection.query(sqlQuery);

        let countSqlQuery = `select * from counties where deleted_at is null`
        if (keyword) {
            countSqlQuery += ` and name like  '%${keyword}%' `
        }
        const count = await connection.query(countSqlQuery);
        return {count:count.length,counties:counties}
    }
    static async getResidingCountry(occupation_id){
        let sqlQuery = `select * from counties where id=?`
        const occu = await connection.query(sqlQuery, [occupation_id]);
        return occu[0];
    }
    static async addResidingCountry(name,name_jp){
        let sqlQuery = `select * from counties where (name=? or name_jp=?) and deleted_at is null`
        const occu = await connection.query(sqlQuery, [name,name_jp]);
        if(occu.length > 0){
            throw("Country already added")
        }
        else{
            const result = await connection.query('insert into counties set name=?,name_jp=?', [name,name_jp]);
            return ({ id: result.insertId, message: "Counties added" })
        }
    }
    static async editResidingCountry(id,name,name_jp){
        let sqlQuery = `select * from counties where (name=? or name_jp=?) and id != ? and deleted_at is null`
        const occu = await connection.query(sqlQuery, [name,name_jp,id]);
        if(occu.length > 0){
            throw("Country already added")
        }
        else{
            const result = await connection.query(`update counties set name=?,name_jp=? where id = ${id}`, [name,name_jp]);
            return ({ message: "Counties updated" })
        }
    }
    static async deleteResidingCountry(id){
        if(id == 2){
            throw("Unable to delete Jpan country")
        }
        else{
            let current_time = new Date()
            const result = await connection.query(`update counties set deleted_at=? where id = ${id}`, [current_time]);
            return ({ message: "Counties deleted" })
        }
    }


    /*=========Nationalitites management=========*/
    static async getNationalities(keyword,limit,page){
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select * from nationalities where deleted_at is null`
        if (keyword) {
            sqlQuery += ` and name like  '%${keyword}%' `
        }
        sqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const nationalities = await connection.query(sqlQuery);

        let countSqlQuery = `select * from nationalities where deleted_at is null`
        if (keyword) {
            countSqlQuery += ` and name like  '%${keyword}%' `
        }
        const count = await connection.query(countSqlQuery);
        return {count:count.length,nationalities:nationalities}
    }
    static async getNationalityDetail(nationality_id){
        let sqlQuery = `select * from nationalities where id=?`
        const nat = await connection.query(sqlQuery, [nationality_id]);
        return nat[0];
    }
    static async addNationality(name,name_jp){
        const result = await connection.query('insert into nationalities set name=?,name_jp=?', [name,name_jp]);
        return ({ id: result.insertId, message: "Nationality added" })
    }
    static async editNationality(id,name,name_jp){
        const result = await connection.query(`update nationalities set name=?,name_jp=? where id = ${id}`, [name,name_jp]);
        return ({ message: "Nationality updated" })
    }
    static async deleteNationality(id){
        let current_time = new Date()
        const result = await connection.query(`update nationalities set deleted_at=? where id = ${id}`, [current_time]);
        return ({ message: "Nationality deleted" })
    }

    /*========Teaching standards management========*/
    static async getTeachingStandards(keyword,limit,page){
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select * from teaching_standards where deleted_at is null`
        if (keyword) {
            sqlQuery += ` and name like  '%${keyword}%' `
        }
        sqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const teaching_standards = await connection.query(sqlQuery);

        let countSqlQuery = `select * from teaching_standards where deleted_at is null`
        if (keyword) {
            countSqlQuery += ` and name like  '%${keyword}%' `
        }
        countSqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const count = await connection.query(countSqlQuery);
        return {count:count.length,teaching_standards:teaching_standards}
    }
    static async getTeachingStandardDetail(teaching_id){
        let sqlQuery = `select * from teaching_standards where id = ?`
        const teach = await connection.query(sqlQuery, [teaching_id]);
        return teach[0];
    }
    static async addTeachingStand(name,name_jp){
        const result = await connection.query('insert into teaching_standards set name=?,name_jp=?', [name,name_jp]);
        return ({ id: result.insertId, message: "Teaching standard added" })
    }
    static async editTeachingStand(id,name,name_jp){
        const result = await connection.query(`update teaching_standards set name=?,name_jp=? where id = ${id}`, [name,name_jp]);
        return ({ message: "Teaching standard updated" })
    }
    static async deleteTeachingStandard(id){
        let current_time = new Date()
        const result = await connection.query(`update teaching_standards set deleted_at=? where id = ${id}`, [current_time]);
        return ({ message: "Teaching standard deleted" })
    }

    /*========Subject management========*/
    static async getSubjects(keyword,limit,page){
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select * from subjects where deleted_at is null`
        if (keyword) {
            sqlQuery += ` and name like  '%${keyword}%' `
        }
        sqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const subjects = await connection.query(sqlQuery);

        let countSqlQuery = `select * from subjects where deleted_at is null`
        if (keyword) {
            countSqlQuery += ` and name like  '%${keyword}%' `
        }
       const count = await connection.query(countSqlQuery);
        return {count:count.length,subjects:subjects}
    }
    static async getSubjectDetail(subject_id){
        let sqlQuery = `select * from subjects where id = ?`
        const teach = await connection.query(sqlQuery, [subject_id]);
        return teach[0];
    }
    static async addSubject(name,name_jp){
        const result = await connection.query('insert into subjects set name=?,name_jp=?', [name,name_jp]);
        return ({ id: result.insertId, message: "Subject added" })
    }
    static async editSubject(id,name,name_jp){
        const result = await connection.query(`update subjects set name=?,name_jp=? where id = ${id}`, [name,name_jp]);
        return ({ message: "Subject updated" })
    }
    static async deleteSubject(id){
        let current_time = new Date()
        const result = await connection.query(`update subjects set deleted_at=? where id = ${id}`, [current_time]);
        return ({ message: "Subject deleted" })
    }

    /*========Education level management========*/
    static async getEducationLevels(keyword,limit,page){
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select * from level_educations where deleted_at is null`
        if (keyword) {
            sqlQuery += ` and name like  '%${keyword}%' `
        }
        sqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const educations = await connection.query(sqlQuery);

        let countSqlQuery = `select * from level_educations where deleted_at is null`
        if (keyword) {
            countSqlQuery += ` and name like  '%${keyword}%' `
        }
        countSqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const count = await connection.query(countSqlQuery);
        return {count:count.length,educations:educations}
    }
    static async getEducationLevelDetail(subject_id){
        let sqlQuery = `select * from level_educations where id = ?`
        const teach = await connection.query(sqlQuery, [subject_id]);
        return teach[0];
    }
    static async addEducationLevel(name,name_jp){
        const result = await connection.query('insert into level_educations set name=?,name_jp=?', [name,name_jp]);
        return ({ id: result.insertId, message: "Education level added" })
    }
    static async editEducationLevel(id,name,name_jp){
        const result = await connection.query(`update level_educations set name=?,name_jp=? where id = ${id}`, [name,name_jp]);
        return ({ message: "Education level updated" })
    }
    static async deleteEducationLevel(id){
        let current_time = new Date()
        const result = await connection.query(`update level_educations set deleted_at=? where id = ${id}`, [current_time]);
        return ({ message: "Education level deleted" })
    }

    /*========Complain subject management========*/
    static async getComplainSubjects(keyword,limit,page){
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } 
        else if (limit) {
            var limit = limit
        } 
        else {
            var limit = 20
        }
        let sqlQuery = `select * from complain_subjects where deleted_at is null`
        if (keyword) {
            sqlQuery += ` and name like  '%${keyword}%' `
        }
        sqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const subjects = await connection.query(sqlQuery);

        let countSqlQuery = `select * from complain_subjects where deleted_at is null`
        if (keyword) {
            countSqlQuery += ` and name like  '%${keyword}%' `
        }
        countSqlQuery += ` order by id desc LIMIT ${limit} OFFSET ${offset}`
        const count = await connection.query(countSqlQuery);
        return {count:count.length,subjects:subjects}
    }
    static async getComplainSubjectDetail(subject_id){
        let sqlQuery = `select * from complain_subjects where id = ?`
        const teach = await connection.query(sqlQuery, [subject_id]);
        return teach[0];
    }
    static async addComplainSubject(name,name_jp){
        const result = await connection.query('insert into complain_subjects set name=?,name_jp=?', [name,name_jp]);
        return ({ id: result.insertId, message: "Subject added" })
    }
    static async editComplainSubject(id,name,name_jp){
        const result = await connection.query(`update complain_subjects set name=?,name_jp=? where id = ${id}`, [name,name_jp]);
        return ({ message: "Subject updated" })
    }
    static async deleteComplainSubject(id){
        let current_time = new Date()
        const result = await connection.query(`update complain_subjects set deleted_at=? where id = ${id}`, [current_time]);
        return ({ message: "Subject deleted" })
    }

    /*=======Booking management=======*/
    static async getBookingTimeSlotComplains(page,limit,keyword){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let query = `select booking_request_slots.id as time_slot_id,booking_requests.id as booking_id,booking_requests.booking_unique_id,booking_request_slots.start_time,booking_request_slots.end_time,booking_request_slots.complain_description,c.name as complain_subject,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_request_slots  left join booking_requests on booking_request_slots.booking_request_id = booking_requests.id left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join complain_subjects as c on booking_request_slots.complain_subject_id = c.id left join users as uu on booking_requests.user_id = uu.id where booking_request_slots.complain_subject_id IS NOT NULL`
        if(keyword){
            query += ` and p.title like  '%${keyword}%' `
        }
        
        query += ` order by booking_requests.id DESC LIMIT ${limit} OFFSET ${offset}`
        let bookings = await connection.query(query)
        

        let countQuery = `select booking_request_slots.id as time_slot_id,booking_requests.id as booking_id,booking_requests.booking_unique_id,booking_request_slots.complain_description,c.name as complain_subject,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_request_slots left join booking_requests on booking_request_slots.booking_request_id = booking_requests.id left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join complain_subjects as c on booking_request_slots.complain_subject_id = c.id left join users as uu on booking_requests.user_id = uu.id where  booking_request_slots.complain_subject_id IS NOT NULL`
        if(keyword){
            countQuery += ` and p.title like  '%${keyword}%' `
        }
        let count = await connection.query(countQuery)
        return {count:count.length,complains:bookings}
    }
    static async getBookingComplains(page,limit,keyword){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let query = `select booking_requests.id,booking_requests.booking_unique_id,booking_requests.complain_description,c.name as complain_subject,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join complain_subjects as c on booking_requests.complain_subject_id = c.id left join users as uu on booking_requests.user_id = uu.id where booking_requests.deleted_at is null and booking_requests.complain_subject_id IS NOT NULL`
        if(keyword){
            query += ` and p.title like  '%${keyword}%' `
        }
        
        query += ` order by booking_requests.id DESC LIMIT ${limit} OFFSET ${offset}`
        let bookings = await connection.query(query)
        

        let countQuery = `select booking_requests.booking_unique_id,booking_requests.complain_description,c.name as complain_subject,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join complain_subjects as c on booking_requests.complain_subject_id = c.id left join users as uu on booking_requests.user_id = uu.id where booking_requests.deleted_at is null and booking_requests.complain_subject_id IS NOT NULL`
        if(keyword){
            countQuery += ` and p.title like  '%${keyword}%' `
        }
        let count = await connection.query(countQuery)
        return {count:count.length,complains:bookings}
    }
    static async getBookingList(page,limit,keyword,status){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let query = `select booking_requests.*,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join users as uu on booking_requests.user_id = uu.id where booking_requests.deleted_at is null`
        if(keyword){
            query += ` and p.title like  '%${keyword}%' `
        }
        if(status){
            if(status == 1){
                query += ` and booking_requests.status = 1`
            }
            if(status == 2){
                query += ` and booking_requests.status = 2`
            }
            else if(status == 3){
                query += ` and booking_requests.status = 3`
            }
            else if(status == 4){
                query += ` and booking_requests.status = 4`
            }
            else if(status == 5){
                query += ` and booking_requests.status = 5`
            }
        }
        query += ` order by booking_requests.id DESC LIMIT ${limit} OFFSET ${offset}`
        let bookings = await connection.query(query)
        var arr = []
        await Promise.all(bookings.map(async (x) => {
            var booking = {}
            let optionQuery = `select * from booking_request_slots where booking_request_id = ${x.id}`
            const options = await connection.query(optionQuery)
            
            booking = x;
            arr.push(booking)
        }));

        let countQuery = `select booking_requests.*,p.hourly_rate,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id where booking_requests.deleted_at is null`
        if(keyword){
            countQuery += ` and p.title like  '%${keyword}%' `
        }
        if(status){
            if(status == 1){
                countQuery += ` and booking_requests.status = 1`
            }
            if(status == 2){
                countQuery += ` and booking_requests.status = 2`
            }
            else if(status == 3){
                countQuery += ` and booking_requests.status = 3`
            }
            else if(status == 4){
                countQuery += ` and booking_requests.status = 4`
            }
            else if(status == 5){
                countQuery += ` and booking_requests.status = 5`
            }
        }
        let count = await connection.query(countQuery)
        return {count:count.length,booking:arr}
    }
    static async getBookingDetail(booking_id,post_id){
        let query = `select booking_requests.*,c.name as complain_subject,p.title as post_title,p.description,p.teaching_standard,p.subject,sub.name as subject_name,p.type as post_type,u.id as teacher_id,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join users as uu on booking_requests.user_id = uu.id left join complain_subjects as c on booking_requests.complain_subject_id = c.id left join subjects as sub on p.subject = sub.id`
        if(booking_id){
            query += ` where booking_requests.id = ${booking_id}`
        }
        let bookingDetail = await connection.query(query)
        // let query = `select booking_requests.*,p.title as post_title,p.teaching_standard,p.subject,p.type as post_type,u.id as teacher_id,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id `
        // if(booking_id){
        //     query += ` where booking_requests.id = ${booking_id}`
        // }
        // let bookingDetail = await connection.query(query)
        
        let accpetedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 2`
        const getAcceptedTimeSlots = await connection.query(accpetedTimeSlots)

        let paymentPendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 1`
        const getPaymentPendingTimeSlots = await connection.query(paymentPendingTimeSlots)

        let pendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 0`
        const getPendingTimeSlots = await connection.query(pendingTimeSlots)

        let cancelledTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (status = 2 or status = 4) and payment_status=2`
        const getCancelledTimeSlots = await connection.query(cancelledTimeSlots)

        let declinedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (status = 2 or status = 4) and payment_status = 1`
        const getDeclinedTimeslots = await connection.query(declinedTimeSlots)

        let completedTimeSlots = `select booking_request_slots.*,c.name as complaint_subject_name from booking_request_slots left join complain_subjects as c on booking_request_slots.complain_subject_id = c.id where booking_request_slots.booking_request_id = ${bookingDetail[0].id} and booking_request_slots.status = 3`
        const getCompletedTimeSlots = await connection.query(completedTimeSlots)

        let postLang = `select l.name,l.id from post_languages left join languages as l on post_languages.language_id=l.id where post_languages.post_id=?`
        const getPostLan = await connection.query(postLang, [post_id])

        let convId = ''
        let conId = `select * from message where booking_id=?`
        const conversation_id = await connection.query(conId, [booking_id]) 
        if(conversation_id.length != 0){
            convId = conversation_id[0].conversation_id
        }

        let bookingReviews = await Common.bookingReviews(booking_id)

        return {booking_detail:bookingDetail[0],accepted_time_slots:getAcceptedTimeSlots,payment_pending_timeslots:getPaymentPendingTimeSlots,pending_time_slots:getPendingTimeSlots,cancelled_time_slots:getCancelledTimeSlots,declined_time_slots:getDeclinedTimeslots,completed_time_slots:getCompletedTimeSlots,post_languages:getPostLan,conversation_id:convId,reviews:bookingReviews}
    }
    // static async getBookingDetail(booking_id){
    //     let query = `select booking_requests.*,c.name as complain_subject,p.title as post_title,p.description,p.teaching_standard,p.subject,p.type as post_type,u.id as teacher_id,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join users as uu on booking_requests.user_id = uu.id left join complain_subjects as c on booking_requests.complain_subject_id = c.id`
    //     if(booking_id){
    //         query += ` where booking_requests.id = ${booking_id}`
    //     }
    //     let bookingDetail = await connection.query(query)
        
        
    //     let accpetedTimeSlots = `select booking_request_slots.*,c.name as complain_subject from booking_request_slots left join complain_subjects as c on booking_request_slots.complain_subject_id = c.id where booking_request_slots.booking_request_id = ${bookingDetail[0].id} and booking_request_slots.status = 1`
    //     const getAcceptedTimeSlots = await connection.query(accpetedTimeSlots)

    //     let pendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 0`
    //     const getPendingTimeSlots = await connection.query(pendingTimeSlots)

    //     let cancelledTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (status = 2 or status = 4)`
    //     const getCancelledTimeSlots = await connection.query(cancelledTimeSlots)

    //     return {booking_detail:bookingDetail[0],accepted_time_slots:getAcceptedTimeSlots,pending_time_slots:getPendingTimeSlots,cancelled_time_slots:getCancelledTimeSlots}
    // }
    
    /*=======transaction module======*/
    static async getTransactions(page,limit,keyword,booking_id){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let query = `select payments.*,b.id as booking_id,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.profile_image as teacher_profile_image,bu.first_name as student_first_name,bu.last_name as student_last_name,bu.profile_image as student_profile_image from payments left join booking_requests as b on payments.booking_id = b.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as bu on b.user_id = bu.id where payments.deleted_at is null `
        if(keyword){
            query += ` and bu.first_name like  '%${keyword}%' `
        }
        if(booking_id){
            query += ` and payments.booking_id = ${booking_id}`
        }
        query += ` order by payments.id DESC LIMIT ${limit} OFFSET ${offset}`
        let payments = await connection.query(query)

        let countQuery = `select payments.*,b.id as booking_id,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.profile_image as teacher_profile_image,bu.first_name as student_first_name,bu.last_name as student_last_name,bu.profile_image as student_profile_image from payments left join booking_requests as b on payments.booking_id = b.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as bu on b.user_id = bu.id where payments.deleted_at is null `
        if(keyword){
            countQuery += ` and bu.first_name like  '%${keyword}%' `
        }
        if(booking_id){
            countQuery += ` and payments.booking_id = ${booking_id}`
        }
        let count = await connection.query(countQuery)
        return {count:count.length,transactions:payments}
    }
    static async getTransactionDetail(transaction_id){
        let query = `select booking_request_slots.*,b.currency,p.title as post_title,b.created_at as booking_date,py.invoice_id,py.created_at as payment_date,tu.country as teacher_country,su.country as student_country from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id left join payments as py on booking_request_slots.payment_id = py.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as su on b.user_id = su.id where booking_request_slots.payment_id = ${transaction_id} `
        let payments = await connection.query(query)
        return payments
    }

    /*===========Paypal module==========*/
    static async getPaypalTeachers(page,limit,keyword){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select booking_requests.*,u.paypal_username,u.id as teacher_id,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join users as uu on booking_requests.user_id = uu.id where booking_requests.deleted_at is null and u.paypal_username != ''`
        if(keyword){
            sqlQuery += ` and (u.first_name like  '%${keyword}%' or u.last_name like  '%${keyword}%' or concat(u.first_name, ' ', u.last_name) like  '%${keyword}%') `
        }
        sqlQuery += ` order by booking_requests.id DESC LIMIT ${limit} OFFSET ${offset}`
        const users = await connection.query(sqlQuery);
        
        let countQuery = `select booking_requests.*,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join users as uu on booking_requests.user_id = uu.id where booking_requests.deleted_at is null and u.paypal_username != ''`
        if(keyword){
            countQuery += ` and (u.first_name like  '%${keyword}%' or u.last_name like  '%${keyword}%' or concat(u.first_name, ' ', u.last_name) like  '%${keyword}%') `
        }
        let count = await connection.query(countQuery)
        return {count:count.length,users:users}
    }
    static async getPaypalTransactions(page,limit,booking_id){
        let query = `select booking_requests.*,c.name as complain_subject,p.title as post_title,p.description,p.teaching_standard,p.subject,sub.name as subject_name,p.type as post_type,u.paypal_username,u.id as teacher_id,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image,uu.first_name as student_first_name,uu.last_name as student_last_name,uu.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join users as uu on booking_requests.user_id = uu.id left join complain_subjects as c on booking_requests.complain_subject_id = c.id left join subjects as sub on p.subject = sub.id`
        if(booking_id){
            query += ` where booking_requests.id = ${booking_id}`
        }
        let bookingDetail = await connection.query(query)
        // let query = `select booking_requests.*,p.title as post_title,p.teaching_standard,p.subject,p.type as post_type,u.id as teacher_id,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id `
        // if(booking_id){
        //     query += ` where booking_requests.id = ${booking_id}`
        // }
        // let bookingDetail = await connection.query(query)
        
        let ongoingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 2`
        const getOngoingTimeSlots = await connection.query(ongoingTimeSlots)

        let payoutsTimeslots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 3`
        const getPayoutTimeslots = await connection.query(payoutsTimeslots)

        
        // let postLang = `select l.name,l.id from post_languages left join languages as l on post_languages.language_id=l.id where post_languages.post_id=?`
        // const getPostLan = await connection.query(postLang, [post_id])

        let convId = ''
        let conId = `select * from message where booking_id=?`
        const conversation_id = await connection.query(conId, [booking_id]) 
        if(conversation_id.length != 0){
            convId = conversation_id[0].conversation_id
        }

        let bookingReviews = await Common.bookingReviews(booking_id)

        return {booking_detail:bookingDetail[0],payoutsTimeslots:getPayoutTimeslots,ongoingTimeSlots:getOngoingTimeSlots,conversation_id:convId,reviews:bookingReviews}
    }
    static async transferAmount(id){
        let sqlQuery = `select * from booking_request_slots where id = ${id}`
        const user = await connection.query(sqlQuery);
        if(user.length > 0){
            await connection.query('update booking_request_slots set transfer_status = 1 where id=? ', [id]);
            return {message:"Balance transferred"}
        }
        
    }
    

    static async getAdminDetail(param, value) {
        let sqlQuery = `select * from users where ${param}=? and user_type = 3`
        const user = await connection.query(sqlQuery, [value]);
        return user;
    }

    static async editUserDetail(logged_id, first_name, last_name, email, phone_number, country_code, profile_image, state, country, city,dob) {
        let sqlQuery = 'update users set '
        let params = []
        let sql = []
        if (first_name != undefined) {
            params.push(first_name)
            sql.push('first_name=?')
        }
        if (last_name != undefined) {
            params.push(last_name)
            sql.push('last_name=?')
        }
        if (email != undefined) {
            params.push(email)
            sql.push('email=?')
        }

        if (phone_number != undefined) {
            params.push(phone_number)
            sql.push('phone_number=?')
        }
        if (country_code != undefined) {
            params.push(country_code)
            sql.push('country_code=?')
        }

        if (city != undefined) {
            params.push(city)
            sql.push('city=?')
        }
        if (state != undefined) {
            params.push(state)
            sql.push('state=?')
        }
        if (country != undefined) {
            params.push(country)
            sql.push('country=?')
        }
        if (profile_image != undefined) {
            params.push(profile_image)
            sql.push('profile_image=?')
        }
        if (dob != undefined) {
            params.push(dob)
            sql.push('dob=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(logged_id)
        return await connection.query(sqlQuery, params)
    }

    

    static async getProjects(keyword, limit, page, user_id) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select projects.*,users.first_name,users.profile_image from projects left join users on users.id=projects.user_id where 1 `
        if (user_id) {
            sqlQuery += ` and deleted_at is null and projects.user_id=${user_id}`
        }
        if (keyword) {
            sqlQuery += ` and (projects.title like  '%${keyword}%' or projects.location like  '%${keyword}%' or projects.status like  '%${keyword}%' or projects.description like  '%${keyword}%') `
        }
        sqlQuery += ` order by projects.id desc LIMIT ${limit} OFFSET ${offset}`
        let project = await connection.query(sqlQuery);
        let main_array = [];
        project.map(item => {
            main_array.push(item);
        })
        let get_count = `select count(*) as count from projects `
        if (keyword) {
            get_count += ` where (projects.title like  '%${keyword}%' or projects.location like  '%${keyword}%' or projects.status like  '%${keyword}%' or projects.description like  '%${keyword}%') `
        }
        let query = await connection.query(get_count)
        console.log("count ", query[0].count);
        return { count: query[0].count, project }
    }

    static async getProjectDetail(project_id) {
        const project_array = await connection.query('select projects.*,users.first_name,users.profile_image from projects left join users on users.id=projects.user_id where projects.id=?', [project_id]);
        if (project_array.length > 0) {
            const project = project_array[0];
            const images_array = await connection.query('select image from project_images where project_id=?', [project_id]);
            let images = [];
            images_array.filter(image => {
                images.push(image.image)
            })
            project.images = images;
            return project;
        }
        else {
            return project_array
        }
    }

    static async editProject(id, title, description, price, location, images, status) {
        let sqlQuery = 'update projects set '
        let params = []
        let sql = []
        if (title != undefined) {
            params.push(title)
            sql.push('title=?')
        }
        if (description != undefined) {
            params.push(description)
            sql.push('description=?')
        }
        if (price != undefined) {
            params.push(price)
            sql.push('price=?')
        }

        if (location != undefined) {
            params.push(location)
            sql.push('location=?')
        }
        if (status != undefined) {
            params.push(status)
            sql.push('status=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        const result = await connection.query(sqlQuery, params)
        if (result && images) {
            await connection.query('delete from project_images where  project_id=?', [id]);

            const values = [];
            images.forEach(image => {
                values.push([id, image])
            })
            console.log('2......')

            await connection.query('insert into project_images (project_id,image) values ?', [values]);
            console.log('3......')

            return result

        }
    }

    static async getUsers(keyword, page, limit, type, is_blocked) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select * from users where users.user_type = ?`
        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or concat(users.first_name, ' ', users.last_name) like  '%${keyword}%'  or users.email like  '%${keyword}%'  or users.phone_number like  '%${keyword}%'  or users.address like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            sqlQuery += ` and users.is_blocked = ? `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
        let user = await connection.query(sqlQuery, [type, is_blocked])
        // console.log(user.sql)
        let main_array = [];
        user.map(item => {
            delete item.password;
            main_array.push(item);
        })

        let get_count = `select count(users.id) as count from users where users.user_type = ${type} `
        if (keyword) {
            get_count += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'  or users.email like  '%${keyword}%'  or users.phone_number like  '%${keyword}%'  or users.address like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            get_count += ` and users.is_blocked = ? `
        }
        let query = await connection.query(get_count, [is_blocked])
        console.log("count ", query[0].count);
        return { count: query[0].count, user }
    }

    static async getStaffList(keyword, page, limit) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select * from users where user_type = 4 and deleted_at is null`

        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or concat(users.first_name, ' ', users.last_name) like  '%${keyword}%' or users.email like  '%${keyword}%' or users.user_type like  '%${keyword}%') `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
        let user = await connection.query(sqlQuery);
        let main_array = [];
        user.map(item => {
            delete item.password;
            main_array.push(item);
        })

        let get_count = `select count(*) as count from users where user_type = 4 and deleted_at is null`
        if (keyword) {
            get_count += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or concat(users.first_name, ' ', users.last_name) like  '%${keyword}%' or users.email like  '%${keyword}%' or users.user_type like  '%${keyword}%') `
        }
        let query = await connection.query(get_count)
        // console.log("count ",query);
        return { count: query[0].count, user }

    }

    static async getStaffDetail(id, type) {
        const user = await connection.query('select * from users where id=? and user_type=?', [id, type]);
        if (user.length == 0) {
            throw ("User does not exist")
        } else {
            delete user[0].password;
        }
        return user[0];
    }

    

    static async deactivateUser(id, logged_user, is_deactivate, deactivate_reason) {
        if (logged_user == 2) {
            if (is_deactivate == 1) {
                let response = await connection.query('update users set is_deactivated = 1,deactivate_reason=? where id=? ', [deactivate_reason, id]);
                return response
            } else {
                let response = await connection.query('update users set is_deactivated = 0, deactivate_reason = null where id=? ', [id]);
                console.log(response)
                return response

            }

        } else {
            throw ("You are not authorized to deactivate the staff.!");
        }
    }

    static async verifyUser(id, logged_user, is_verified) {
        if (logged_user == 2) {
            let response = await connection.query('update users set is_user_verified = ? where id=? ', [is_verified, id]);
            return response
        } else {
            throw ("You are not authorized to deactivate the staff.!");
        }
    }

    static async addUser(first_name, last_name, email, password, profile_image, city, state, country, country_code, address, phone_number, type, dob, description) {
        if (await this.checkIsEmailExist(email)) {
            throw ("Email already exist!");
        }
        const salt = genSaltSync(10);
        let hashPassword = hashSync(password, salt);
        const result = await connection.query('insert into users set first_name=?,last_name=?,email=?,password=?, profile_image=?, city=?, state=?, country=?, country_code=?, location=?, phone_number=?,user_type=?, dob=?, description=?', [first_name, last_name, email, hashPassword, profile_image, city, state, country, country_code, address, phone_number, type, dob, description]);
        console.log(result)
        if (result) {
            return ({ id: result.insertId, message: "Staff member added" })
        }
    }

    static async addContent(title, content, image) {
        const result = await connection.query('insert into static_pages set title=?,content=?, image=?', [title, content, image]);
        console.log(result)
        if (result.insertId > 0) {
            return ({ id: result.insertId, message: "Content added" })
        }
    }

    static async addProject(title, description, price, location, images) {
        const result = await connection.query('insert into projects set title=?,description=?,price=?,location=?', [title, description, price, location]);
        console.log(result)
        if (images) {
            for (var i in images) {
                await connection.query('insert into project_images set project_id=?,image=?', [result.insertId, images[i]]);
            }
        }
        if (result) {
            return ({ id: result.insertId, message: "Project added" })
        }
    }

    static async checkIsEmailExist(email) {
        let user = await this.getUserByEmail(email);
        if (user[0]) {
            return 1
        } else {
            return 0
        }
    }

    static async deleteProject(project_id) {
        let current_time = new Date()
        console.log(current_time)
        const result = await connection.query(`update projects set deleted_at=? where id = ${project_id}`, [current_time]);
        return ({ message: "Project Deleted" })
    }

    static async getContents(keyword, limit, page) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select * from static_pages`
        if (keyword) {
            sqlQuery += ` where (static_pages.title like  '%${keyword}%' or static_pages.content like  '%${keyword}%') `
        }
        sqlQuery += ` order by static_pages.id asc LIMIT ${limit} OFFSET ${offset}`
        let content = await connection.query(sqlQuery);
        let main_array = [];
        content.map(item => {
            main_array.push(item);
        })
        let get_count = `select count(*) as count from static_pages`
        if (keyword) {
            get_count += ` where (static_pages.title like  '%${keyword}%' or static_pages.content like  '%${keyword}%') `
        }
        let query = await connection.query(get_count)
        return { count: query[0].count, content }
    }

    static async getContentDetail(id) {
        return await connection.query('select * from static_pages where id=?', [id]);
    }

    static async editContent(id, title,title_jp, content,content_jp, image) {
        let sqlQuery = 'update static_pages set '
        let params = []
        let sql = []
        if (title != undefined) {
            params.push(title)
            sql.push('title=?')
        }
        if (title_jp != undefined) {
            params.push(title_jp)
            sql.push('title_jp=?')
        }
        if (content != undefined) {
            params.push(content)
            sql.push('content=?')
        }
        if (content_jp != undefined) {
            params.push(content_jp)
            sql.push('content_jp=?')
        }
        if (image != undefined) {
            params.push(image)
            sql.push('image=?')
        }

        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        const result = await connection.query(sqlQuery, params)
        return result
    }

    static async deleteContent(content_id) {
        const result = await connection.query('delete from static_pages where id = ?', [content_id]);
        if (result.affectedRows > 0) {
            return ({ message: "Content deleted" })
        } else {
            throw ("Content does not exist")
        }
    }

    

    static async sendMail(email, subject, content) {
        if (!email) {
            const data = await connection.query('select email from users where user_type=1 and is_email_notified = 1');
            var emails = []
            for (var i in data) {
                emails.push(data[i].email)
            }
            console.log(emails)
            // return await Common.sendMail(emails, subject, content)
            return await Common.adminNotification(emails,content)
        } else {
            return await Common.adminNotification(email,content)
        }

    }

    static async sendPush(email, subject, content) {
        if (!email) {
            const data = await connection.query('select fcm_id from user_logins inner join users on users.id=user_logins.user_id where is_push_notified = 1 and expired_at is null');
            var fcm_ids = []
            for (var i in data) {
                fcm_ids.push(data[i].fcm_id)
            }
            console.log(fcm_ids)
            return await Common.fcmPush('0', fcm_ids, subject, content)
        } else {
            const data = await connection.query('select fcm_id from user_logins inner join users on users.id=user_logins.user_id where users.email IN (?) and users.is_push_notified = 1 and expired_at is null', email);
            var fcm_ids = []
            for (var i in data) {
                fcm_ids.push(data[i].fcm_id)
            }
            console.log(fcm_ids)
            return await Common.fcmPush('0', fcm_ids, subject, content)
        }
    }

    static async getUsersByEmail(type, email) {
        let query = `select id from users`
        let users
        if (type == 0) {
            users = await connection.query(query);
        } else {
            query += ` where email in ('${email.toString()}')`
            console.log(query);
            users = await connection.query(query);
        }
        // console.log(users);
        return users
    }

    static async saveMessage(type, email, subject, content, user_id) {
        let user_ids = []
        const users = await this.getUsersByEmail(type, email)
        if(users.length >= 1){
            const data = await connection.query(`insert into notifications(type,title,message,from_user_id) values(?,?,?,?)`, [0, subject, content, user_id]);
        if (users.length) {
            users.forEach(x => {
                user_ids.push([data.insertId, x.id])
            })
        }
        return await connection.query(`insert into notification_tos(notification_id,to_user_id) values ?`, [user_ids])
        }
        
    }

    /*======Contact us management========*/
    static async getContacts(keyword, limit, page, status) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select * from contact_us where deleted_at is null`
        if (keyword) {
            sqlQuery += ` and ( contact_us.phone_number like  '%${keyword}%' or contact_us.email like  '%${keyword}%' or contact_us.name like  '%${keyword}%' or contact_us.message like  '%${keyword}%' ) `
        }
        if (status) {
            sqlQuery += ` and status=${status}`
        }
        sqlQuery += ` order by contact_us.id desc LIMIT ${limit} OFFSET ${offset}`

        let contact = await connection.query(sqlQuery);
        let main_array = [];
        contact.map(item => {
            main_array.push(item);
        })
        let get_count = `select count(*) as count from contact_us where deleted_at is null`
        if (keyword) {
            get_count += ` and ( contact_us.email like  '%${keyword}%' or contact_us.name like  '%${keyword}%' or contact_us.message like  '%${keyword}%' )`
        }
        if (status) {
            get_count += ` and status=${status}`
        }
        let query = await connection.query(get_count)
        return { count: query[0].count, contact }
    }
    static async deleteContact(contact_id) {
        const result = await connection.query('delete from contact_us where id = ?', [contact_id]);
        if (result.affectedRows > 0) {
            return ({ message: "Contact deleted" })
        } else {
            throw ("Contact does not exist")
        }
    }
    static async resolveContact(contact_id){
        let sqlQuery = `update contact_us set status=2 where id=${contact_id}`
        return await connection.query(sqlQuery);
    }

    static async dbBackup() {
        var username = process.env.DB_USER;
        var password = process.env.DB_PASSWORD;
        var databaseName = process.env.DB_DATABASE;
        console.log('123')
        //  var tablename= "users";
        //creating key for backup file
        // var key =
        //   databaseName + '-' + moment().format('YYYY-MM-DD-HH-mm-ss') + '.sql';
        var key = databaseName + '.sql'
        var dumpCommand = `mysqldump -u${username} -p${password} ${databaseName} > db/${key}`;

        // dumpCommand mysqldump -uroot -proot demo
        childProcess.execSync(dumpCommand, (error, stdout, stderr) => {


        });

        // fs.unlink("db/" + key,function (err, ) {
        //     if (err) {
        //         return err;
        //     }
        // // });
        // return res
        // return file

        var buffer = ("db/" + key);
        console.log('buffer', buffer);
        return key

    }



}
module.exports = AdminService;