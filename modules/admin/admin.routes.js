var express = require('express');
const { checkSchema } = require('express-validator');
let adminSchema = require("./admin.validation.json")
var router = express.Router();
const valid = require('../../config/validator')
const AdminController = require("./admin.controller");
let auth = require("../../middleware/auth")


router.post('/login', checkSchema(adminSchema.login), valid, AdminController.login);
router.get('/profile', auth.checkToken, AdminController.getProfile);
router.put('/profile', auth.checkOptionalToken, AdminController.updateProfile);
router.put('/change-password', auth.checkToken, checkSchema(adminSchema.changePassword), valid, AdminController.changePassword);

//Dashboard management
router.get('/dashboard', auth.checkToken, AdminController.dashboard)
router.get('/all-users', auth.checkToken, AdminController.getAllUsers)

//User management
// router.post('/user', auth.checkToken, checkSchema(adminSchema.addUser), valid, AdminController.addUser);
router.get('/students', auth.checkOptionalToken, AdminController.getSudents);
router.get('/student', auth.checkOptionalToken, AdminController.getUserDetail);
router.post('/student', auth.checkOptionalToken, AdminController.addStudent);
router.put('/student/block', auth.checkToken, AdminController.blockUser);
router.post('/student/deactivate', auth.checkToken, AdminController.actDeactUser);
router.post('/student/delete', auth.checkToken, AdminController.deleteUser);

//Teacher management
router.post('/teacher', auth.checkOptionalToken, AdminController.addTeacher);
router.get('/teachers', auth.checkOptionalToken, AdminController.getTeachers);
router.get('/teacher', auth.checkOptionalToken, AdminController.getUserDetail);
router.put('/teacher/block', auth.checkToken, AdminController.blockUser);
router.post('/teacher/deactivate', auth.checkToken, AdminController.actDeactUser);
router.post('/teacher/delete', auth.checkToken, AdminController.deleteUser);
router.post('/teacher/verify', auth.checkToken, AdminController.verifyTeacher);

//post management
router.get('/posts', auth.checkOptionalToken, AdminController.getPosts);
router.get('/post-detail', auth.checkOptionalToken, AdminController.getPostDetail);
router.post('/block-post', auth.checkOptionalToken, AdminController.blockPost);

//request management
router.get('/requests', auth.checkOptionalToken, AdminController.getRequests);
router.get('/request-detail', auth.checkOptionalToken, AdminController.getRequestDetail);
router.post('/block-request', auth.checkOptionalToken, AdminController.blockRequest);

//popular filter management
router.get('/popular-filters', auth.checkOptionalToken, AdminController.getAllStaticPagesData);
router.post('/popular-filters', auth.checkOptionalToken, AdminController.addPopular);

//Language management
router.get('/languages', auth.checkOptionalToken, AdminController.getLanguages);
router.get('/language', auth.checkOptionalToken, AdminController.getLanDetail);
router.post('/language', auth.checkToken, AdminController.addLanguage);
router.put('/language', auth.checkToken, AdminController.editLanguage);
router.post('/delete-language', auth.checkToken, AdminController.deleteLanguage);

//Occupation management
router.get('/occupations', auth.checkOptionalToken, AdminController.getOccupations);
router.get('/occupation', auth.checkOptionalToken, AdminController.getOccupationDetail);
router.post('/occupations', auth.checkToken, AdminController.addOccupation);
router.put('/occupations', auth.checkToken, AdminController.editOccupation);
router.post('/delete-occupation', auth.checkToken, AdminController.deleteOccupation);

//Nationality management
router.get('/nationalities', auth.checkOptionalToken, AdminController.getNationalities);
router.get('/nationality', auth.checkOptionalToken, AdminController.getNationalityDetail);
router.post('/nationality', auth.checkToken, AdminController.addNationality);
router.put('/nationality', auth.checkToken, AdminController.editNationality);
router.post('/delete-nationality', auth.checkToken, AdminController.deleteNationality);

//Teaching standards management
router.get('/teaching-standards', auth.checkOptionalToken, AdminController.getTeachingStandards);
router.get('/teaching-standard', auth.checkOptionalToken, AdminController.getTeachingStandardDetail);
router.post('/teaching-standard', auth.checkToken, AdminController.addTeachingStand);
router.put('/teaching-standard', auth.checkToken, AdminController.editTeachingStand);
router.post('/delete-teaching-standard', auth.checkToken, AdminController.deleteTeachingStandard);

//Residing country management
router.get('/residing-countries', auth.checkOptionalToken, AdminController.getResidingCountries);
router.get('/residing-country', auth.checkOptionalToken, AdminController.getResidingCountry);
router.post('/residing-country', auth.checkToken, AdminController.addResidingCountry);
router.put('/residing-country', auth.checkToken, AdminController.editResidingCountry);
router.post('/delete-residing-country', auth.checkToken, AdminController.deleteResidingCountry);

//Subject management
router.get('/subjects', auth.checkOptionalToken, AdminController.getSubjects);
router.get('/subject', auth.checkOptionalToken, AdminController.getSubjectDetail);
router.post('/subject', auth.checkToken, AdminController.addSubject);
router.put('/subject', auth.checkToken, AdminController.editSubject);
router.post('/delete-subject', auth.checkToken, AdminController.deleteSubject);

//education level management
router.get('/education-levels', auth.checkOptionalToken, AdminController.getEducationLevels);
router.get('/education-level', auth.checkOptionalToken, AdminController.getEducationLevelDetail);
router.post('/education-level', auth.checkToken, AdminController.addEducationLevel);
router.put('/education-level', auth.checkToken, AdminController.editEducationLevel);
router.post('/delete-education-level', auth.checkToken, AdminController.deleteEducationLevel);

//complain subjects
router.get('/complain-subjects', auth.checkOptionalToken, AdminController.getComplainSubjects);
router.get('/complain-subject', auth.checkOptionalToken, AdminController.getComplainSubjectDetail);
router.post('/complain-subject', auth.checkToken, AdminController.addComplainSubject);
router.put('/complain-subject', auth.checkToken, AdminController.editComplainSubject);
router.post('/delete-complain-subject', auth.checkToken, AdminController.deleteComplainSubject);

router.get('/booking-complains', auth.checkToken, AdminController.getBookingComplains);
router.get('/booking-timeslot-complains', auth.checkToken, AdminController.getBookingTimeSlotComplains);

//Booking management
router.get('/bookings', auth.checkToken, AdminController.getBookings);
router.get('/booking-detail', auth.checkToken, AdminController.getBookingDetail);

//Transaction module
router.get('/transactions', auth.checkToken, AdminController.getTransactions);
router.get('/transaction-detail', auth.checkToken, AdminController.getTransactionDetail);

//Paypal payouts
router.get('/paypal-teachers', auth.checkToken, AdminController.getPaypalTeachers);
router.get('/paypal-teachers-transactions', auth.checkToken, AdminController.getPaypalTransactions);
router.post('/transfer-amount', auth.checkToken, AdminController.transferAmount);

//staff

// router.put('/user', auth.checkToken, valid, AdminController.editUserDetail);

// router.delete('/user', auth.checkToken, AdminController.deleteUser);
// router.post('/user/deactivate', auth.checkToken, AdminController.deactivateUser)
// router.post('/user/verify', auth.checkToken, AdminController.verifyUser)

// //project management
// router.post('/project', auth.checkToken, valid, AdminController.addProject);
// router.get('/projects', auth.checkToken, AdminController.getProjects);
// router.get('/project', auth.checkToken, AdminController.getProjectDetail);
// router.put('/project', auth.checkToken, valid, AdminController.editProject);
// router.delete('/project', auth.checkToken, AdminController.deleteProject);

//staff management
router.post('/user', auth.checkToken, checkSchema(adminSchema.addUser), valid, AdminController.addUser);
router.put('/user', auth.checkToken,AdminController.editUserDetail);
router.put('/user/block', auth.checkToken, AdminController.blockUser);
router.delete('/user', auth.checkToken, AdminController.deleteUser);
router.post('/user/deactivate', auth.checkToken, AdminController.actDeactUser)
router.get('/staffs', auth.checkToken, AdminController.staffList);
router.get('/staff', auth.checkToken, AdminController.getStaffDetail)

//content management
router.post('/content', auth.checkToken, AdminController.addContent);
router.get('/contents', auth.checkToken, AdminController.getContents);
router.get('/content', auth.checkToken, AdminController.getContentDetail);
router.put('/content', auth.checkToken, valid, AdminController.editContent);
router.delete('/content', auth.checkToken, AdminController.deleteContent);

//send mail
router.post('/notification', auth.checkToken, AdminController.sendNotification);

//contact-us
router.get('/contact-us', auth.checkToken, AdminController.getContacts);
router.delete('/contact-us', auth.checkToken, AdminController.deleteContact);
router.post('/resolve-contact', auth.checkToken, AdminController.resolveContact);

//backup-db
router.get('/backup-db', auth.checkToken, AdminController.dbBackup)


module.exports = router;