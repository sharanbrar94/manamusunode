const axios = require("axios");
var http = require("http");
var https = require("https");
var fs = require("fs");
// var rootCas = require('ssl-root-cas/latest').create();
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
const ChatController = require("../../chat/chat.controller");
// default for all https requests
// (whether using https directly, request, or another module)
// require('https').globalAgent.options.ca = rootCas;
var users = [];

module.exports = function (io) {
  io.on("connection", (socket) => {
    console.log("Socket running...");
    console.log("socket", socket.id);
    socket.on("init", async (user) => {
      console.log("Socket init...");
      const data_temp = user;
      var user_id = user.user_from;
      users[user_id] = socket.id;
    });

    socket.on("message_sent", async (data) => {
      console.log("Socket message sent...",data);            
      const data_temp = data;
      var chat = await ChatController.saveMessage(data)
      io.emit("message_received", chat);        
    });
    socket.on("make_payment", async (data) => {
      console.log("Socket message sent...");            
      const data_temp = data;
      // if(data.booking_type == 1){
      //   data.message = 'To confirm your booked classes, please deposit the outstanding class fees. Class details are available on the payment screen.'
      // }
      // if(data.booking_type == 3){
      //   data.message = 'Payment done'
      // }
      if(data.booking_type == 3){
        var chat = await ChatController.saveMessage(data)
        io.emit("message_received", chat);
        var chatId = chat.id
        console.log("chatId",chatId)
        // var student = await ChatController.getMessage(chatId)
        // io.emit("message_received", student);
      }
      else{
        var chat = await ChatController.saveMessage(data)
        io.emit("message_received", chat);      
      }
      // var chat = await ChatController.saveMessage(data)
      // io.emit("message_received", chat);        
    });

    socket.on("unread_message_count", async (data) => {
      console.log("Socket message sent...");            
      const data_temp = data;
      var chat = await ChatController.unreadMessage(data)
      io.emit("message_received", chat);        
    });

    socket.on("delete_message", async (data) => {
      await axios.delete(
        `${process.env.URL}/api/delete/message`,data_temp,  {
          headers: { Authorization: `${data.token}` }
        }
      );
    });

    

    socket.on("disconnect", async (data) => {
      console.log("Socket disconnect...");
      const data_temp = data;
      console.log(data_temp);
      await axios.post(
        `${process.env.URL}/api/user/online`,
        data_temp,
        {
          headers: { Authorization: `${data_temp.token}` }
        }
      );
    });
  });
};
