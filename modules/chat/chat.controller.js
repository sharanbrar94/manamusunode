const ChatService = require("../chat/chat.service");
const logger = require("../../config/logger");
const { off } = require("../../config/logger");
const _ = require("lodash");

class chatController {
  static async getUserConversation(req, res) {
    try {
      const { id } = req.user;
      const { keyword, page, limit,status } = req.query;
      console.log("status",status)
      const result = await ChatService.getUserConversation(id, id, page, limit, keyword,status);

      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res
        .status(400)
        .json({ error: "bad_request", error_description: err.message });
    }
  }
  static async getCategoryCount(req, res){
    try {
      const { id,user_type } = req.user;
       const result = await ChatService.getCategoryCount(id,user_type);

      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res
        .status(400)
        .json({ error: "bad_request", error_description: err.message });
    }
  }

  static async getUserListing(req, res) {
    try {
      const { keyword } = req.query;
      const result = await ChatService.getUserListing(keyword);

      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res
        .status(400)
        .json({ error: "bad_request", error_description: err.message });
    }
  }
  static async getMessage(data,chatId){
    try {
      let from_user = data.user_from
      let to_user = data.user_to
      const firstNumber = await Math.min(from_user, to_user);
      const secondNumber = await Math.max(from_user, to_user);
      const conversation_id = `${firstNumber}_${secondNumber}`;
      data.conversation_id = conversation_id;
      let result = await ChatService.getMessage(data,chatId);
      console.log("data",result)
      return result
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return err
    }
  }
  static async saveMessage(data) {   
    try {
      let from_user = data.user_from
      let to_user = data.user_to
      const firstNumber = await Math.min(from_user, to_user);
      const secondNumber = await Math.max(from_user, to_user);
      const conversation_id = `${firstNumber}_${secondNumber}`;
      data.conversation_id = conversation_id;
      let result = await ChatService.saveMessage(data);
      console.log("data",result)
      return result

    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return err
    }
    
  }
  static async unreadMessage(data){
    try {
      let user_id = data.user_id
      let result = await ChatService.unreadMessage(user_id);
      return result[0]
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return err
    }
  }
  static async sendMessage(req, res) {
    try {
      const { user_to, user_from,message,booking_id } = req.body
      const firstNumber = await Math.min(user_from, user_to)
      const secondNumber = await Math.max(user_from, user_to)
      const conversation_id = `${firstNumber}_${secondNumber}`
      req.body.conversation_id = conversation_id
      await ChatService.saveMessage(req.body);
      return res.status(200).json({ message: "Message saved!" });
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong.",
      });
    }
  }

  static async deleteMessage(req, res) {
    try {
      
      const deleted_for_me=req.body.deleted_for_me
      const local_identifier=req.body.local_identifier
      const user  = req.user.id;

      let result = await ChatService.deleteMessage(deleted_for_me, user, local_identifier);

      if (result.affectedRows > 0) {
        return res.status(200).json({ message: "Message has been deleted!" });
      } else {
        return res.status(401).json({ message: "local_identifier is not correct" });
      }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong",
      });
    }
  }

  static async deleteConversation(req, res) {
    try {
      const { id: user_to } = req.body;

      const { id: user } = req.user;

      let result = await ChatService.deleteConversation(user, user_to);
      return res.status(200).json(result);
      // } else {
      // if (result > 0) {
      //   return res.status(200).json({ message: "Message has been deleted!" });
      // } else {
      //   return res.status(401).json({ message: "You can't delete this message" });
      // }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong",
      });
    }
  }

  static async userStatus(req, res) {
    try {
      const { status } = req.body;
      const { id } = req.user;
      await ChatService.userStatus(status, id);
      return res.status(200).json({ status: status, message: "status updated" });
    } catch (err) {
      console.log("error: ", err);
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong",
      });
    }
  }

  static async getUserProfile(req, res) {
    try {
      const { id: user_id } = req.query;
      let user = await ChatService.getUserProfile(user_id)
      delete user[0].password;
      return res.status(200).json(user[0])
    } catch (err) {
      console.log("error: ", err);
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong",
      });
    }
  }

  static async getMessages(req, res) {
    try {
      const { conversation_id,booking_id, limit, page_id } = req.query;
      // console.log("Param:", req.params);

      let user_id = req.user.id;

      // const findRes = await ChatService.checkUserBlockStatus(
      //   conversation_id, user_id

      // );

       const result = {};
      // result.blocked_status = findRes[0].blocked_status;

      var response = await ChatService.getMessages(
        conversation_id,booking_id,user_id,limit,page_id
      );

      result.count = response.length;
      result.message = response;
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong",
      });
    }
  }

  static async messageSeen(req, res) {
    try {
      const { id: user_to } = req.user;
      const { user_from } = req.body;
      let result = await ChatService.messageSeen(1, user_from, user_to);
      console.log("Result: ", result);
      if (result.affectedRows > 0) {
        return res.status(200).json({ message: "Message seen!" });
      } else {
        return res.status(400).json({ message: "Error in executing your request." });
      }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong: ",
      });
    }
  }

  static async searchUser(req, res) {
    try {
      const { name } = req.body;
      const { id } = req.user;
      const result = await ChatService.searchUser(name, id, id, 1);

      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong" + err.message,
      });
    }
  }

  static async blockUser(req, res) {
    try {
      const { id: blocked_user } = req.body;
      const { id: blocked_by } = req.user;

      await ChatService.blockUser(blocked_user, blocked_by);

      return res.status(200).json({ message: "Status changed." });
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong " + err.message,
      });
    }
  }


}

module.exports = chatController;
