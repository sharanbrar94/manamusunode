
const { response } = require("express");
let Common = require("../../middleware/common")
var moment = require('moment-timezone');


class ChatService {
  static async userStatus(status, user_id) {
    const query = `UPDATE users SET status = ?, last_seen_at = now() WHERE id = ?`;
    return await connection.query(query, [status, user_id]);
  }
  static async getMessage(chatId){
    const getChat = `select * from message where linked_to_chat=?`;
    var getChatDetail = await connection.query(getChat, [chatId]);
    return getChatDetail[0]
  }
  static async saveMessage(user_data) {
    const query = `INSERT  INTO message (user_to, user_from, message,conversation_id,booking_id) values (?,?,?,?,?)`;
    var chat = await connection.query(query, [user_data.user_to, user_data.user_from, user_data.message, user_data.conversation_id,user_data.booking_id]);
    if(user_data.booking_type){
      const bookingQuery = `select * from booking_requests where id=?`;
      const getBooking = await connection.query(bookingQuery, [user_data.booking_id]);
      var currency = getBooking[0].currency
      var hourly_rate = getBooking[0].hourly_rate

      if(user_data.booking_type == 1){
        const systemMsg = await connection.query(`select * from system_messages where message_type=1`)
        var message = systemMsg[0].message
        var message_jp = systemMsg[0].message_jp
      }
      if(user_data.booking_type == 3){
        const systemMsg = await connection.query(`select * from system_messages where message_type=3`)
        var message = systemMsg[0].message
        var message_jp = systemMsg[0].message_jp

        const systemMsgStu = await connection.query(`select * from system_messages where message_type=4`)
        var message_stu = systemMsgStu[0].message
        var message_jp_stu = systemMsgStu[0].message_jp
        
        const queryStu = `INSERT  INTO message (user_to, user_from,conversation_id,booking_id,booking_type,message,message_jp,linked_to_chat) values (?,?,?,?,?,?,?,?)`;
        await connection.query(queryStu, [user_data.user_to, user_data.user_from, user_data.conversation_id,user_data.booking_id,4,message_stu,message_jp_stu,chat.insertId]);
      }
      if(user_data.booking_type == 5){
        var message = "Hi, there! I would like to apply for classes with you, at an hourly rate of "+currency+hourly_rate+"/hr"
        var message_jp = "こんにちは。応募させて頂きましたので、確認よろしくお願いします。（時給："+currency+hourly_rate+"/hr)"
      }
      if(user_data.booking_type == 6){
        const systemMsg = await connection.query(`select * from system_messages where message_type=6`)
        var message = systemMsg[0].message
        var message_jp = systemMsg[0].message_jp
      }
      if(user_data.booking_type == 7){
        const systemMsg = await connection.query(`select * from system_messages where message_type=7`)
        var message = systemMsg[0].message
        var message_jp = systemMsg[0].message_jp
      }
      if(user_data.booking_type == 8){
        const timeSloQuery = `select * from booking_request_slots where id=?`;
        const getTimeSlot = await connection.query(timeSloQuery, [user_data.time_slot_id]);
        var start_time = getTimeSlot[0].start_time
        var end_time = getTimeSlot[0].end_time
        var start_log_time = getTimeSlot[0].start_log_time
        var end_log_time = getTimeSlot[0].end_log_time
        var total_log_hour = getTimeSlot[0].total_log_hour

        let addMonth = getTimeSlot[0].date.getMonth() + 1;
        let getStartDate = getTimeSlot[0].date.getFullYear() + '-' + addMonth + '-' + getTimeSlot[0].date.getDate()
       
        var convertStartTime = getStartDate+' '+start_time+':00'
        var convertEndTime = getStartDate+' '+end_time+':00'

        var getBookingStartTime = await this.convertLocalToTimezone(convertStartTime, null, user_data.timezone); 
        var getBookingEndTime = await this.convertLocalToTimezone(convertEndTime, null, user_data.timezone); 

        var getStartTime = await this.convertLocalToTimezone(start_log_time, null, user_data.timezone);
        
        var message = "Today's "+getBookingStartTime+" - "+getBookingEndTime+" ("+user_data.timezone+") class has started at "+getStartTime+" ("+user_data.timezone+"), You can join/rejoin the call via the 'Join Online Classroom' button after refreshing the page."
        var message_jp = getBookingStartTime+" - "+getBookingEndTime+" ("+user_data.timezone+") の授業が "+getStartTime+" ("+user_data.timezone+") から開始されました。ページ更新後に表示されるチャットの右の”授業開始”ボタンをクリックし、授業にお進みください。"
        
      }
      if(user_data.booking_type == 9){
        const timeSloQuery = `select * from booking_request_slots where id=?`;
        const getTimeSlot = await connection.query(timeSloQuery, [user_data.time_slot_id]);
        var start_time = getTimeSlot[0].start_time
        var end_time = getTimeSlot[0].end_time
        var start_log_time = getTimeSlot[0].start_log_time
        var end_log_time = getTimeSlot[0].end_log_time
        var total_log_hour = getTimeSlot[0].total_log_hour

        var getEndTime = await this.convertLocalToTimezone(end_log_time, null, user_data.timezone);

        let addMonth = getTimeSlot[0].date.getMonth() + 1;
        let getStartDate = getTimeSlot[0].date.getFullYear() + '-' + addMonth + '-' + getTimeSlot[0].date.getDate()
       
        var convertStartTime = getStartDate+' '+start_time+':00'
        var convertEndTime = getStartDate+' '+end_time+':00'

        var getBookingStartTime = await this.convertLocalToTimezone(convertStartTime, null, user_data.timezone); 
        var getBookingEndTime = await this.convertLocalToTimezone(convertEndTime, null, user_data.timezone); 

        let getDuration = await this.calculateDuration(total_log_hour)

        var message = "Today's "+getBookingStartTime+" - "+getBookingEndTime+" ("+user_data.timezone+") class has ended at "+getEndTime+" ("+user_data.timezone+"). Duration of the class was "+getDuration+"."

        var message_jp = getBookingStartTime+"-"+getBookingEndTime+" ("+user_data.timezone+") の授業が "+getEndTime+" ("+user_data.timezone+") に終了しました。総授業時間は "+getDuration+" です。"
        
      }
      if(user_data.booking_type == 10){
        const systemMsg = await connection.query(`select * from system_messages where message_type=10`)
        var message = systemMsg[0].message
        var message_jp = systemMsg[0].message_jp
      }
      if(user_data.booking_type == 11){
        const systemMsg = await connection.query(`select * from system_messages where message_type=11`)
        var message = systemMsg[0].message
        var message_jp = systemMsg[0].message_jp
      }
      if(user_data.booking_type == 12){
        
        const systemMsg = await connection.query(`select * from system_messages where message_type=12`)
        var message = systemMsg[0].message
        var message_jp = systemMsg[0].message_jp
      }
      if(user_data.booking_type == 13){
        const userQuery = `select * from users where id=?`;
        const getUsers = await connection.query(userQuery, [getBooking[0].user_id]);

        var message = "IMPORTANT: "+getUsers[0].first_name+" has made a change to the hourly rate. Please be sure to check the new hourly rate before responding to any applications."

        var message_jp = "（重要）生徒が時給を変更しました。ご確認の上、授業を確定してください。"
        
        
      }
      const updateQuery = `UPDATE message SET booking_type = ?,message=?,message_jp=? WHERE id = ?`;
      await connection.query(updateQuery, [user_data.booking_type,message,message_jp, chat.insertId]);
    }
    const getChat = `select * from message where id=?`;
    var getChatDetail = await connection.query(getChat, [chat.insertId]);
    return getChatDetail[0]
  }
  static async convertLocalToTimezone(localDt, localDtFormat, timezone) {
    return moment(localDt, localDtFormat).tz(timezone).format('hh:mm A');
  }
  
  static async calculateDuration(mins){
    let getMin = mins*parseInt(60)
    var h = Math.floor(getMin / 60);          
    var m = getMin % 60;
    
    h = h < 10 ? '0' + h : h;
    m = m < 10 ? '0' + m : m;

    const duration_time = h+':'+m
    return duration_time

    
  }
  // Local date "2020-05-20 10:12:44 PM" to "America/Los_Angeles" timezone date:
  

  static async unreadMessage(user_id){
    const query = `select count* from block_user where (blocked_user=? and blocked_by=?) OR (blocked_by=? and blocked_user=?)`;
    return await connection.query(query, [user_to, user_from, user_to, user_from]);
  }

  static async checkBlockedUser(user_to, user_from) {
    const query = `select * from block_user where (blocked_user=? and blocked_by=?) OR (blocked_by=? and blocked_user=?)`;
    return await connection.query(query, [user_to, user_from, user_to, user_from]);
  }

  static async getUserProfile(id) {
    let sqlQuery = `select * from users where id=${id}`
    return await connection.query(sqlQuery)
  }

  static async deleteMessage(deleted_for_me, user,local_identifier) {
    let query;

    if (deleted_for_me == 1) {
      query = `UPDATE message SET deleted_by = ? WHERE  (user_from = ? OR user_to=?) AND local_identifier = ?  `;
      return await connection.query(query, [user, user, user,local_identifier]);
    } else {
      query = `DELETE from message WHERE user_from = ? AND local_identifier = ? `;
      return await connection.query(query, [user,local_identifier]);
    }
  }

  static async deleteConversation(user, user_to) {
    let query = `select * from message  where (user_from = ? and user_to =?) OR (user_from = ? and user_to =?) `;
    var response = await connection.query(query, [user, user_to, user_to, user]);
    console.log(response)
    var deleted_msgs = []
    var msgs = []
    response.filter((ele) => {
      if (ele.deleted_by != user) {
        deleted_msgs.push(ele.id)
      } else if (ele.deleted_by == null || ele.deleted_by == user) {
        msgs.push(ele.id)
      }
    })

    if (deleted_msgs.length > 0) {
      let delete_query = "delete from message where id IN (?)"
      var del_response = await connection.query(delete_query, [deleted_msgs]);
      console.log("delete response", del_response)
    }
    if (msgs.length > 0) {

      let update_query = "update message set deleted_by = ? where id IN (?)"
      var up_response = await connection.query(update_query, [user, msgs]);
      console.log("delete response", up_response)
    }
    return response
    // if (deleted_for_me == 1) {
    //   query = `UPDATE message SET deleted_for_me = ? WHERE  (user_from = ? OR user_to=?) AND id = ?  `;
    //   return await connection.query(query, [deleted_for_me, user, user, id]);
    // } else {
    //   query = `DELETE from message WHERE user_from = ? AND id = ?`;
    //   return await connection.query(query, [user, id]);
    // }
  }

  static async messageSeen(is_read, user_from, user_to) {
    const query = `UPDATE message SET is_read = ? WHERE  user_from = ? AND user_to = ?`;
    return await connection.query(query, [
      parseInt(is_read),
      user_from,
      user_to,
    ]);
  }
  static async getCategoryCount(id,user_type){
    console.log("id:user_id",id)
    let pending_count = 0
    let pending_payment_count = 0
    let declined_count = 0
    let ongoing_count = 0
    let completed_count = 0

    if(user_type == 1){
      var get_pending_payment_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read_student=0 and message.user_to=${id} and  booking_requests.status = 2 GROUP BY message.booking_id`)
    }
    else{
      var get_pending_payment_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read=0 and message.user_to=${id} and  booking_requests.status = 2 GROUP BY message.booking_id`)
    }
    await Promise.all(get_pending_payment_count.map(async (x) => {
        pending_payment_count = pending_payment_count+x.count
    }));
    console.log("get_pending_payment_count",get_pending_payment_count)

    if(user_type == 1){
      var get_pending_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read_student=0 and message.user_to=${id} and  booking_requests.status = 1 GROUP BY message.booking_id`)
    }
    else{
      var get_pending_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read=0 and message.user_to=${id} and  booking_requests.status = 1 GROUP BY message.booking_id`)
    } 
    console.log("sql:2",get_pending_count)
    await Promise.all(get_pending_count.map(async (x) => {
      pending_count = pending_count+x.count
    }));
    console.log("get_pending_count",get_pending_count)

    if(user_type == 1){
      var get_declined_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read_student=0 and message.user_to=${id} and  booking_requests.status = 3 GROUP BY message.booking_id`)
    }
    else{
      var get_declined_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read=0 and message.user_to=${id} and  booking_requests.status = 3 GROUP BY message.booking_id`)
    }
    await Promise.all(get_declined_count.map(async (x) => {
      declined_count = declined_count+x.count
    }));
    console.log("declined_count",get_declined_count)

    if(user_type == 1){
      var get_ongoing_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read_student=0 and message.user_to=${id} and  booking_requests.status = 4 GROUP BY message.booking_id`)
    }
    else{
      var get_ongoing_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read=0 and message.user_to=${id} and  booking_requests.status = 4 GROUP BY message.booking_id`)
    }
    await Promise.all(get_ongoing_count.map(async (x) => {
      ongoing_count = ongoing_count+x.count
    }));
    
    if(user_type == 1){
      var get_completed_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read_student=0 and message.user_to=${id} and  booking_requests.status = 5 GROUP BY message.booking_id`)
    }
    else{
      var get_completed_count = await connection.query(` select booking_id, count(message.id) as count from message left join booking_requests on message.booking_id = booking_requests.id where message.is_read=0 and message.user_to=${id} and  booking_requests.status = 5 GROUP BY message.booking_id`)
    }
    await Promise.all(get_completed_count.map(async (x) => {
      completed_count = completed_count+x.count
    }));
    
    return {pending_count:pending_count,pending_payment_count:pending_payment_count,declined_count:declined_count,ongoing_count:ongoing_count,completed_count:completed_count}
  }

  static async getUserConversation(user_from, user_to, page, limit, keyword,status) {

    var offset = 0
    if (page && limit) {
      var offset = ((page < 1 ? 1 : page) - 1) * limit;
    } else if (limit) {
      var limit = limit
    } else {
      var limit = 20
    }
    var query = `select m.* ,
    ( CASE WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, m.created_at) != 0 THEN  IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, m.created_at)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, m.created_at)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, m.created_at)) ,' year ago'))  WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, m.created_at) != 0 THEN  IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, m.created_at)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, m.created_at)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, m.created_at)) ,' month ago'))  WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, m.created_at)) != 0 THEN  IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, m.created_at))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, m.created_at))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, m.created_at))) ,' hours ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, m.created_at))) ,' hour ago')),  IF(DATEDIFF(UTC_TIMESTAMP, m.created_at) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, m.created_at) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, m.created_at) ,' day ago'))) WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, m.created_at)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, m.created_at)) ,' min ago') ELSE CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, m.created_at)) ,'s ago') END ) as time_since,
     u.first_name,u.last_name,u.profile_image,u.email,u.description,u.status,u.last_seen_at,IF(u.last_name is null,u.first_name,CONCAT(u.first_name , ' ', u.last_name )) as participant, IF(m.user_from like ${user_from},1, 0) as from_me from message as m left join users as u on u.id = IF(m.user_from like ${user_from}, m.user_to,m.user_from) left join booking_requests on m.booking_id = booking_requests.id where m.id in (  SELECT MAX(id) FROM message  WHERE user_from like ${user_from} OR user_to like ${user_from}  GROUP BY booking_id )   AND (deleted_by is NULL OR deleted_by NOT like ${user_from})`

    if (keyword && keyword != "") {
      query += ` and IF(u.last_name is null,u.first_name,CONCAT(u.first_name , ' ', u.last_name )) like '%${keyword}%'  `
    }
    if (status && status != "") {
      query += ` and booking_requests.status = ${status}  `
    }

    query += `  ORDER BY m.id DESC LIMIT ${limit} OFFSET ${offset}`;
    var list = await connection.query(query, [user_from, user_to]);

    let get_count = await connection.query(` select booking_id, if(count(message.id)=0,0, count(message.id)) as count from message where is_read=0 and user_to=${user_from} group by booking_id`)

    list.forEach(x => {
      x.unread_count = 0
      get_count.forEach(y => {
        if (parseInt(x.booking_id) == parseInt(y.booking_id)) {
          console.log("count ", get_count[0].count);
          x.unread_count = get_count[0].count == undefined ? 0 : get_count[0].count
        }
      })
    })
    let main_array = [];
    list.map(item => {
      delete item.password;
      main_array.push(item);
    })
    return main_array
  }

  static async getUserListing(keyword) {

    var query = `SELECT * from users where user_type = 1 `
    if (keyword) {
      query += `and IF(last_name is null, first_name,CONCAT(first_name,"",last_name)) like '%${keyword}%'  `
    }
    var user = await connection.query(query);
    let main_array = [];
    user.map(item => {
      delete item.password;
      main_array.push(item);
    })
    console.log(main_array)
    return main_array
  }

  static async getMessages(conversation_id,booking_id, user_id, limit, page) {
    let query = `select booking_requests.*,p.user_id as teacher_id,p.title as post_title,p.teaching_standard,p.subject,p.type as post_type,u.first_name as student_first_name,u.last_name as student_last_name,u.profile_image as student_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on booking_requests.user_id = u.id  where booking_requests.id = ${booking_id}`
    let bookingDetail = await connection.query(query)
    if(bookingDetail[0].user_id == user_id || bookingDetail[0].teacher_id == user_id){
      var offset = 0
      console.log('user_id ', user_id);
      if (page && limit) {
        var offset = ((page < 1 ? 1 : page) - 1) * limit;
      } else if (limit) {
        var limit = limit
      } else {
        var limit = 20
      }
      if(bookingDetail[0].user_id == user_id){
        var  update_query = "UPDATE message SET is_read_student = 1 WHERE  conversation_id=? AND booking_id=?"
      }
      if(bookingDetail[0].teacher_id == user_id){
        var  update_query = "UPDATE message SET is_read = 1 WHERE  conversation_id=? AND booking_id=?"
      }
      
      await connection.query(update_query, [conversation_id,booking_id]);

      const query = `SELECT * FROM (
        SELECT id,booking_type, message,message_jp, user_from, user_to ,is_read,created_at,IF(user_from like ${user_id},1, 0) as from_me FROM message WHERE conversation_id='${conversation_id}' AND booking_id='${booking_id}' AND (deleted_by is NULL OR deleted_by NOT LIKE ${user_id}) ORDER BY id DESC 
      ) message  ORDER BY message.id DESC LIMIT ${limit} OFFSET ${offset}`;
      console.log(query);
      const result = await connection.query(query);
      return result.reverse()
    }
    else{
      throw("Not accessable")
    }

    

  }

  static async searchUser(name, id, block_by, status) {
    const query = `SELECT  IF(last_name is null, first_name,CONCAT(first_name,"",last_name)) as name, email 
    FROM users 
    WHERE  IF(last_name is null, first_name,CONCAT(first_name,"",last_name)) like ? AND id != ? AND id  NOT IN(SELECT blocked_user from block_user where  blocked_by = ? and status = ?)
    `;

    return await connection.query(query, [`%${name}%`, id, block_by, status]);
  }

  static async userDeviceInfo(id) {
    const query = `SELECT device_type, fcm_id from user_logins WHERE user_id = ? AND expired_at IS NULL`;
    return await connection.query(query, [id]);
  }

  static async blockUser(blocked_user, blocked_by) {
    const query = "select * from block_user where blocked_user=? and blocked_by=?"
    const response = await connection.query(query, [blocked_user, blocked_by])
    console.log(response)
    if (response.length > 0) {
      const query = `Delete from block_user where id = ? `;
      return await connection.query(query, response[0].id)
    } else {
      const query = `INSERT INTO block_user(blocked_user, blocked_by) values (?,?)`;
      return await connection.query(query, [blocked_user, blocked_by])
    }
    // const query = `INSERT INTO block_user(blocked_user, blocked_by) SELECT * from ( SELECT ?, ?) as temp WHERE NOT EXISTS( SELECT id from block_user WHERE blocked_user = ? AND blocked_by = ? AND status = 1)`;

  }

  static async unblockUser(status, blocked_user, blocked_by) {
    const query = `UPDATE block_user SET status = ? WHERE block_user = ? AND block_by = ?`;
    return await connection.query(query, [status, block_user, blocked_by]);
  }

  static async checkUserBlockStatus(converstaion_id, logged_in) {
    const user = await connection.query(`select * from message where conversation_id='${converstaion_id}'`)
    let blocked_user = user[0].user_from == logged_in ? user[0].user_to : user[0].user_from
    const query = `SELECT COUNT(id) as blocked_status from block_user WHERE blocked_user = ? AND blocked_by = ? `;
    var response = await connection.query(query, [blocked_user, logged_in]);
    console.log(response)
    return response
  }
}

module.exports = ChatService;
