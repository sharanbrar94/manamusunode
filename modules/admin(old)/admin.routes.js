var express = require('express');
const { checkSchema } = require('express-validator');
let adminSchema = require("./admin.validation.json")
var router = express.Router();
const valid = require('../../config/validator')
const AdminController = require("./admin.controller");
let auth = require("../../middleware/auth")


router.post('/login', checkSchema(adminSchema.login), valid, AdminController.login);
router.get('/profile', auth.checkToken, AdminController.getProfile);
router.put('/profile', auth.checkOptionalToken, AdminController.updateProfile);
router.put('/change-password', auth.checkToken, checkSchema(adminSchema.changePassword), valid, AdminController.changePassword);

//Admin Dashboard
router.get('/dashboard', auth.checkToken, AdminController.dashboard)

//User management
router.post('/user', auth.checkToken, checkSchema(adminSchema.addUser), valid, AdminController.addUser);
router.get('/users', auth.checkOptionalToken, AdminController.getUsers);
router.get('/user', auth.checkOptionalToken, AdminController.getUserDetail);
router.put('/user', auth.checkToken, valid, AdminController.editUserDetail);
router.put('/user/block', auth.checkToken, AdminController.blockUser);
router.delete('/user', auth.checkToken, AdminController.deleteUser);
router.post('/user/deactivate', auth.checkToken, AdminController.deactivateUser)
router.post('/user/verify', auth.checkToken, AdminController.verifyUser)

//project management
router.post('/project', auth.checkToken, valid, AdminController.addProject);
router.get('/projects', auth.checkToken, AdminController.getProjects);
router.get('/project', auth.checkToken, AdminController.getProjectDetail);
router.put('/project', auth.checkToken, valid, AdminController.editProject);
router.delete('/project', auth.checkToken, AdminController.deleteProject);

//staff management
router.get('/staffs', auth.checkToken, AdminController.staffList);
router.get('/staff', auth.checkToken, AdminController.getStaffDetail)

//content management
router.post('/content', auth.checkToken, AdminController.addContent);
router.get('/contents', auth.checkToken, AdminController.getContents);
router.get('/content', auth.checkToken, AdminController.getContentDetail);
router.put('/content', auth.checkToken, valid, AdminController.editContent);
router.delete('/content', auth.checkToken, AdminController.deleteContent);

//send mail
router.post('/notification', auth.checkToken, AdminController.sendNotification);

//contact-us
router.get('/contact-us', auth.checkToken, AdminController.getContacts);
router.delete('/contact-us', auth.checkToken, AdminController.deleteContact);

//backup-db
router.get('/backup-db', auth.checkToken, AdminController.dbBackup)


module.exports = router;