const { sign } = require('jsonwebtoken');
const nodemailer = require('nodemailer')
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const logger = require('../../config/logger');
const childProcess = require('child_process')
const fs = require('fs')
const moment = require('moment');
let Common = require("../../middleware/common")
class AdminService {
    static async login(email, password) {
        let user = await this.getAdminDetail('email', email);
        console.log(user.length)
        if (user.length != 0) {
            if (user[0] && user[0].is_blocked == 0 && user[0].is_deactivated == 0) {
                let passwordResult = compareSync(password, user[0].password);
                if (!passwordResult) {
                    throw ("Please enter correct password");
                }
            } else {
                throw ("You are blocked or deactivated by the admin.");
            }
        } else {
            throw ("Please enter registered email id.")
        }
        delete user[0].password;
        let auth_token = await this.generateToken(user);
        return ({ 'message': "Login successfully", 'token': auth_token, 'user': user[0] });
    }

    static async updateProfile(logged_id, first_name, last_name, profile_image) {
        let sqlQuery = 'update users set '
        let params = []
        let sql = []
        if (first_name != undefined) {
            params.push(first_name)
            sql.push('first_name=?')
        }
        if (last_name != undefined) {
            params.push(last_name)
            sql.push('last_name=?')
        }
        if (profile_image != undefined) {
            params.push(profile_image)
            sql.push('profile_image=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(logged_id)
        console.log(logged_id)
        await connection.query(sqlQuery, params)
        return { message: "Profile updated successfully" }
    }

    static async changePassword(new_password, old_password, logged_id) {
        const user = await this.getAdminDetail('id', logged_id)
        let result = compareSync(old_password, user[0].password);
        if (!result) {
            throw ('Please enter correct old password.');
        } else {
            const salt = genSaltSync(10);
            let password = hashSync(new_password, salt);
            let sqlQuery = `update users set password=? where id=?`
            let params = [password, logged_id]
            const result = await connection.query(sqlQuery, params);
            if (result) {
                return ({ message: "Password changed" });
            }
        }
    }

    static async getProfile(id) {
        let sqlQuery = `select * from users where id=${id}`
        return await connection.query(sqlQuery)

    }
    static async getUserByEmail(email) {
        let sqlQuery = `select * from users where email=? `
        return await connection.query(sqlQuery, [email])

    }
    static async generateToken(user) {
        let token = await sign({ user: user[0] }, 'secretkey', { expiresIn: "2Days" });
        return token

    }

    static async getUserDetail(param, value) {
        let sqlQuery = `select * from users where ${param}=?`
        const user = await connection.query(sqlQuery, [value]);
        delete user[0].password;
        return user[0]
    }

    static async getAdminDetail(param, value) {
        let sqlQuery = `select * from users where ${param}=? and user_type != 1`
        const user = await connection.query(sqlQuery, [value]);
        return user;
    }

    static async editUserDetail(id, first_name, last_name, email, phone_number, country_code, profile_image, state, country, city, address, lat, lng, zip, dob, description, type) {
        let sqlQuery = 'update users set '
        let params = []
        let sql = []
        if (first_name != undefined) {
            params.push(first_name)
            sql.push('first_name=?')
        }
        if (last_name != undefined) {
            params.push(last_name)
            sql.push('last_name=?')
        }
        if (email != undefined) {
            params.push(email)
            sql.push('email=?')
        }

        if (phone_number != undefined) {
            params.push(phone_number)
            sql.push('phone_number=?')
        }
        if (country_code != undefined) {
            params.push(country_code)
            sql.push('country_code=?')
        }

        if (city != undefined) {
            params.push(city)
            sql.push('city=?')
        }
        if (state != undefined) {
            params.push(state)
            sql.push('state=?')
        }
        if (country != undefined) {
            params.push(country)
            sql.push('country=?')
        }
        if (address != undefined) {
            params.push(address)
            sql.push('address=?')
        }
        if (lat != undefined) {
            params.push(lat)
            sql.push('lat=?')
        }
        if (lng != undefined) {
            params.push(lng)
            sql.push('lng=?')
        }
        if (zip != undefined) {
            params.push(zip)
            sql.push('zip=?')
        }
        if (profile_image != undefined) {
            params.push(profile_image)
            sql.push('profile_image=?')
        }
        if (dob != undefined) {
            params.push(dob)
            sql.push('dob=?')
        }
        if (description != undefined) {
            params.push(description)
            sql.push('description=?')
        }
        if (type != undefined) {
            params.push(type)
            sql.push('user_type=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        return await connection.query(sqlQuery, params)
    }

    static async blockUser(id, unblock) {
        console.log(2)
        let sqlQuery = 'update users set users.is_blocked=? where users.id=? '
        return await connection.query(sqlQuery, [unblock, id])
    }

    static async getProjects(keyword, limit, page) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select projects.*,users.first_name,users.profile_image from projects left join users on users.id=projects.user_id`
        if (keyword) {
            sqlQuery += ` where (projects.title like  '%${keyword}%' or projects.location like  '%${keyword}%' or projects.status like  '%${keyword}%' or projects.description like  '%${keyword}%') `
        }
        sqlQuery += ` order by projects.id desc LIMIT ${limit} OFFSET ${offset}`
        let project = await connection.query(sqlQuery);
        let main_array = [];
        project.map(item => {
            main_array.push(item);
        })
        let get_count = `select count(*) as count from projects `
        if (keyword) {
            get_count += ` where (projects.title like  '%${keyword}%' or projects.location like  '%${keyword}%' or projects.status like  '%${keyword}%' or projects.description like  '%${keyword}%') `
        }
        let query = await connection.query(get_count)
        console.log("count ", query[0].count);
        return { count: query[0].count, project }
    }

    static async getProjectDetail(project_id) {
        const project_array = await connection.query('select projects.*,users.first_name,users.profile_image from projects left join users on users.id=projects.user_id where projects.id=?', [project_id]);
        if (project_array.length > 0) {
            const project = project_array[0];
            const images_array = await connection.query('select image from project_images where project_id=?', [project_id]);
            let images = [];
            images_array.filter(image => {
                images.push(image.image)
            })
            project.images = images;
            return project;
        }
        else {
            return project_array
        }
    }

    static async editProject(id, title, description, price, location, images, status) {
        let sqlQuery = 'update projects set '
        let params = []
        let sql = []
        if (title != undefined) {
            params.push(title)
            sql.push('title=?')
        }
        if (description != undefined) {
            params.push(description)
            sql.push('description=?')
        }
        if (price != undefined) {
            params.push(price)
            sql.push('price=?')
        }

        if (location != undefined) {
            params.push(location)
            sql.push('location=?')
        }
        if (status != undefined) {
            params.push(status)
            sql.push('status=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        const result = await connection.query(sqlQuery, params)
        if (result && images) {
            await connection.query('delete from project_images where  project_id=?', [id]);

            const values = [];
            images.forEach(image => {
                values.push([id, image])
            })
            console.log('2......')

            await connection.query('insert into project_images (project_id,image) values ?', [values]);
            console.log('3......')

            return result

        }
    }

    static async getUsers(keyword, page, limit, type, is_blocked) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select * from users where users.user_type = ?`
        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'  or users.email like  '%${keyword}%'  or users.phone_number like  '%${keyword}%'  or users.address like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            sqlQuery += ` and users.is_blocked = ? `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
        let user = await connection.query(sqlQuery, [type, is_blocked])
        // console.log(user.sql)
        let main_array = [];
        user.map(item => {
            delete item.password;
            main_array.push(item);
        })

        let get_count = `select count(users.id) as count from users where users.user_type = ${type} `
        if (keyword) {
            get_count += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'  or users.email like  '%${keyword}%'  or users.phone_number like  '%${keyword}%'  or users.address like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            get_count += ` and users.is_blocked = ? `
        }
        let query = await connection.query(get_count, [is_blocked])
        console.log("count ", query[0].count);
        return { count: query[0].count, user }
    }

    static async getStaffList(keyword, page, limit) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select * from users where user_type!=1`

        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or users.email like  '%${keyword}%' or users.city like  '%${keyword}%' or users.state like  '%${keyword}%' or users.country like  '%${keyword}%' or users.address like  '%${keyword}%' or users.user_type like  '%${keyword}%') `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
        let user = await connection.query(sqlQuery);
        let main_array = [];
        user.map(item => {
            delete item.password;
            main_array.push(item);
        })

        let get_count = `select count(*) as count from users where user_type!=1`
        if (keyword) {
            get_count += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or users.email like  '%${keyword}%' or users.city like  '%${keyword}%' or users.state like  '%${keyword}%' or users.country like  '%${keyword}%' or users.address like  '%${keyword}%' or users.user_type like  '%${keyword}%') `
        }
        let query = await connection.query(get_count)
        // console.log("count ",query);
        return { count: query[0].count, user }

    }

    static async getStaffDetail(id, type) {
        const user = await connection.query('select * from users where id=? and user_type=?', [id, type]);
        if (user.length == 0) {
            throw ("User does not exist")
        } else {
            delete user[0].password;
        }
        return user[0];
    }

    static async deleteUser(id, logged_user) {
        if (logged_user == 2) {
            let response = await connection.query('delete from users where id=? ', [id]);
            console.log(response)
            if (response.affectedRows > 0) {
                return { message: "User Deleted" }
            } else {
                throw ("User does not exist")
            }
        } else {
            throw ("You are not authorized to delete the staff.!");
        }
    }

    static async deactivateUser(id, logged_user, is_deactivate, deactivate_reason) {
        if (logged_user == 2) {
            if (is_deactivate == 1) {
                let response = await connection.query('update users set is_deactivated = 1,deactivate_reason=? where id=? ', [deactivate_reason, id]);
                return response
            } else {
                let response = await connection.query('update users set is_deactivated = 0, deactivate_reason = null where id=? ', [id]);
                console.log(response)
                return response

            }

        } else {
            throw ("You are not authorized to deactivate the staff.!");
        }
    }

    static async verifyUser(id, logged_user, is_verified) {
        if (logged_user == 2) {
            let response = await connection.query('update users set is_user_verified = ? where id=? ', [is_verified, id]);
            return response
        } else {
            throw ("You are not authorized to deactivate the staff.!");
        }
    }

    static async addUser(first_name, last_name, email, password, profile_image, city, state, country, country_code, address, phone_number, type, dob, description) {
        if (await this.checkIsEmailExist(email)) {
            throw ("Email already exist!");
        }
        const salt = genSaltSync(10);
        let hashPassword = hashSync(password, salt);
        const result = await connection.query('insert into users set first_name=?,last_name=?,email=?,password=?, profile_image=?, city=?, state=?, country=?, country_code=?, address=?, phone_number=?,user_type=?, dob=?, description=?', [first_name, last_name, email, hashPassword, profile_image, city, state, country, country_code, address, phone_number, type, dob, description]);
        console.log(result)
        if (result) {
            return ({ id: result.insertId, message: "Staff member added" })
        }
    }

    static async addContent(title, content, image) {
        const result = await connection.query('insert into static_pages set title=?,content=?, image=?', [title, content, image]);
        console.log(result)
        if (result.insertId > 0) {
            return ({ id: result.insertId, message: "Content added" })
        }
    }

    static async addProject(title, description, price, location, images) {
        const result = await connection.query('insert into projects set title=?,description=?,price=?,location=?', [title, description, price, location]);
        console.log(result)
        if (images) {
            for (var i in images) {
                await connection.query('insert into project_images set project_id=?,image=?', [result.insertId, images[i]]);
            }
        }
        if (result) {
            return ({ id: result.insertId, message: "Project added" })
        }
    }

    static async checkIsEmailExist(email) {
        let user = await this.getUserByEmail(email);
        if (user[0]) {
            return 1
        } else {
            return 0
        }
    }

    static async deleteProject(project_id) {
        const result = await connection.query('delete from posts where id = ?', [project_id]);
        if (result.affectedRows > 0) {
            return ({ message: "Project Deleted" })
        } else {
            throw ("Project does not exist")
        }
    }

    static async getContents(keyword, limit, page) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select * from static_pages`
        if (keyword) {
            sqlQuery += ` where (static_pages.title like  '%${keyword}%' or static_pages.content like  '%${keyword}%') `
        }
        sqlQuery += ` order by static_pages.id asc LIMIT ${limit} OFFSET ${offset}`
        let content = await connection.query(sqlQuery);
        let main_array = [];
        content.map(item => {
            main_array.push(item);
        })
        let get_count = `select count(*) as count from static_pages`
        if (keyword) {
            get_count += ` where (static_pages.title like  '%${keyword}%' or static_pages.content like  '%${keyword}%') `
        }
        let query = await connection.query(get_count)
        return { count: query[0].count, content }
    }

    static async getContentDetail(id) {
        return await connection.query('select * from static_pages where id=?', [id]);
    }

    static async editContent(id, title, content, image) {
        let sqlQuery = 'update static_pages set '
        let params = []
        let sql = []
        if (title != undefined) {
            params.push(title)
            sql.push('title=?')
        }
        if (content != undefined) {
            params.push(content)
            sql.push('content=?')
        }
        if (image != undefined) {
            params.push(image)
            sql.push('image=?')
        }

        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        const result = await connection.query(sqlQuery, params)
        return result
    }

    static async deleteContent(content_id) {
        const result = await connection.query('delete from static_pages where id = ?', [content_id]);
        if (result.affectedRows > 0) {
            return ({ message: "Content deleted" })
        } else {
            throw ("Content does not exist")
        }
    }

    static async deleteContact(contact_id) {
        const result = await connection.query('delete from contact_us where id = ?', [contact_id]);
        if (result.affectedRows > 0) {
            return ({ message: "Contact deleted" })
        } else {
            throw ("Contact does not exist")
        }
    }

    static async sendMail(email, subject, content) {
        if (!email) {
            const data = await connection.query('select email from users where user_type=1 and is_email_notified = 1');
            var emails = []
            for (var i in data) {
                emails.push(data[i].email)
            }
            console.log(emails)
            // return await Common.sendMail(emails, subject, content)
            return await Common.sendMail(emails, null, subject, content, null)
        } else {
            return await Common.sendMail(email, null, subject, content, null)
        }

    }

    static async sendPush(email, subject, content) {
        if (!email) {
            const data = await connection.query('select fcm_id from user_logins inner join users on users.id=user_logins.user_id where is_push_notified = 1 and expired_at is null');
            var fcm_ids = []
            for (var i in data) {
                fcm_ids.push(data[i].fcm_id)
            }
            console.log(fcm_ids)
            return await Common.fcmPush('0', fcm_ids, subject, content)
        } else {
            const data = await connection.query('select fcm_id from user_logins inner join users on users.id=user_logins.user_id where users.email IN (?) and users.is_push_notified = 1 and expired_at is null', email);
            var fcm_ids = []
            for (var i in data) {
                fcm_ids.push(data[i].fcm_id)
            }
            console.log(fcm_ids)
            return await Common.fcmPush('0', fcm_ids, subject, content)
        }
    }

    static async getUsersByEmail(type, email) {
        let query = `select id from users`
        let users
        if (type == 0) {
            users = await connection.query(query);
        } else {
            query += ` where email in ('${email.toString()}')`
            console.log(query);
            users = await connection.query(query);
        }
        // console.log(users);
        return users
    }

    static async saveMessage(type, email, subject, content, user_id) {
        let user_ids = []
        const users = await this.getUsersByEmail(type, email)
        const data = await connection.query(`insert into notifications(type,title,message,from_user_id) values(?,?,?,?)`, [0, subject, content, user_id]);
        if (users.length) {
            users.forEach(x => {
                user_ids.push([data.insertId, x.id])
            })
        }
        return await connection.query(`insert into notification_tos(notification_id,to_user_id) values ?`, [user_ids])
    }

    static async getContacts(keyword, limit, page) {
        var offset = 0
        if (page > 0 && limit) {
            var offset = (page - 1) * limit;
        } else if (limit) {
            var limit = limit
        } else {
            var limit = 20
        }
        let sqlQuery = `select id,user_id,message,phone_number,email,name as first_name,country_code from contact_us`
        if (keyword) {
            sqlQuery += ` where ( contact_us.name like  '%${keyword}%' or contact_us.message like  '%${keyword}%' ) `
        }
        //or contact_us.last_name like  '%${keyword}%'
        sqlQuery += ` order by contact_us.id desc LIMIT ${limit} OFFSET ${offset}`
        let contact = await connection.query(sqlQuery);
        let main_array = [];
        contact.map(item => {
            main_array.push(item);
        })
        let get_count = `select count(*) as count from contact_us`
        if (keyword) {
            get_count += ` where ( contact_us.name like  '%${keyword}%' or contact_us.message like  '%${keyword}%' )`
        }
        let query = await connection.query(get_count)
        return { count: query[0].count, contact }
    }

    static async dbBackup() {
        var username = process.env.DB_USER;
        var password = process.env.DB_PASSWORD;
        var databaseName = process.env.DB_DATABASE;
        console.log('123')
        //  var tablename= "users";
        //creating key for backup file
        // var key =
        //   databaseName + '-' + moment().format('YYYY-MM-DD-HH-mm-ss') + '.sql';
        var key = databaseName + '.sql'
        var dumpCommand = `mysqldump -u${username} -p${password} ${databaseName} > db/${key}`;

        // dumpCommand mysqldump -uroot -proot demo
        childProcess.execSync(dumpCommand, (error, stdout, stderr) => {


        });

        // fs.unlink("db/" + key,function (err, ) {
        //     if (err) {
        //         return err;
        //     }
        // // });
        // return res
        // return file

        var buffer = ("db/" + key);
        console.log('buffer', buffer);
        return key

    }



}
module.exports = AdminService;