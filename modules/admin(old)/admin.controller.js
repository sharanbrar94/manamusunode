require('dotenv').config();
const logger = require('../../config/logger'); // importiing winston logger module
const jwt = require('jsonwebtoken')
const AdminService = require("./admin.service");
const path = require("path")
class AdminController {
  static async login(req, res, next) {
    try {
      let email = req.body.email;
      let password = req.body.password
      let user = await AdminService.login(email, password);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async dashboard(req, res, next) {

    try {
      let users = await AdminService.getUsers(null, null, null, 1, null)
      console.log(users.count);
      const projects = await AdminService.getProjects(null, null, null);
      console.log(projects.count)
      const contacts = await AdminService.getContacts(null, null, null);
      console.log(contacts.count);
      return res.status(200).json({ user_count: users.count, project_count: projects.count, contacts_count: contacts.count })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }

  }

  static async updateProfile(req, res, next) {
    try {
      let first_name = req.body.first_name;
      let last_name = req.body.last_name;
      let profile_image = req.body.profile_image;
      let logged_id = req.user.id
      const result = await AdminService.updateProfile(logged_id, first_name, last_name, profile_image)
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async changePassword(req, res, next) {
    try {
      let logged_id = req.body.id;
      let old_password = req.body.old_password;
      let new_password = req.body.new_password;
      console.log(logged_id)
      const result = await AdminService.changePassword(new_password, old_password, logged_id);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getProfile(req, res, next) {
    try {
      let user_id = req.user.id;
      let user = await AdminService.getProfile(user_id)
      return res.status(200).json(user[0])
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  // User Managemant 
  static async getUsers(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      let type = req.query.type;
      let is_blocked = req.query.is_blocked;
      console.log(limit + ' ' + page);
      let users = await AdminService.getUsers(keyword, page, limit, type, is_blocked)

      return res.status(200).json(users)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getUserDetail(req, res, next) {
    try {
      let id = req.query.id
      const user = await AdminService.getUserDetail('id', id);
      console.log(user);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }



  static async editUserDetail(req, res, next) {
    try {
      let logged_id = req.body.id
      let first_name = req.body.first_name
      let last_name = req.body.last_name
      let email = req.body.email
      let phone_number = req.body.phone_number
      let country_code = req.body.country_code
      let profile_image = req.body.profile_image
      let state = req.body.state
      let country = req.body.country
      let city = req.body.city
      let address = req.body.address
      let lat = req.body.lat
      let lng = req.body.lng
      let zip = req.body.zip
      let dob = req.body.dob
      let description = req.body.description
      let type = req.body.type
      const result = await AdminService.editUserDetail(logged_id, first_name, last_name, email, phone_number, country_code, profile_image, state, country, city, address, lat, lng, zip, dob, description, type);
      if (result) {
        return res.status(200).json({ message: "user profile updated" });
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async blockUser(req, res, next) {
    try {
      console.log(1)
      let id = req.body.id;
      let unblock = req.body.unblock;
      const result = await AdminService.blockUser(id, unblock);
      if (result) {
        console.log(3)
        return res.status(200).json({ message: 'block status updated' })
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  // Project Management

  static async getProjects(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const projects = await AdminService.getProjects(keyword, limit, page);
      return res.status(200).json(projects)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getProjectDetail(req, res, next) {
    try {
      let project_id = req.query.project_id
      const project = await AdminService.getProjectDetail(project_id);
      return res.status(200).json(project)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async editProject(req, res, next) {
    try {
      let id = req.body.id;
      let title = req.body.title;
      let description = req.body.description;
      let price = req.body.price;
      let location = req.body.location;
      let images = req.body.images;
      let status = req.body.status;
      await AdminService.editProject(id, title, description, price, location, images, status);
      return res.status(200).json({
        message: 'Project Updated'
      })

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  static async addProject(req, res, next) {
    try {
      let title = req.body.title;
      let description = req.body.description;
      let price = req.body.price;
      let location = req.body.location;
      let images = req.body.images;
      var result = await AdminService.addProject(title, description, price, location, images);
      return res.status(200).json(result)

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  // Staff Management
  static async addUser(req, res, next) {
    try {
      let first_name = req.body.first_name;
      let last_name = req.body.last_name;
      let email = req.body.email;
      let password = req.body.password;
      let profile_image = req.body.profile_image;
      let city = req.body.city;
      let state = req.body.state;
      let country = req.body.country;
      let country_code = req.body.country_code;
      let address = req.body.address;
      let phone_number = req.body.phone_number;
      let dob = req.body.dob;
      let type = req.body.type;
      let description = req.body.description;
      console.log(req.body)
      const result = await AdminService.addUser(first_name, last_name, email, password, profile_image, city, state, country, country_code, address, phone_number, type, dob, description);
      if (result) {
        return res.status(200).json(result);
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async staffList(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      console.log(limit + ' ' + page);
      let users = await AdminService.getStaffList(keyword, page, limit)
      return res.status(200).json(users)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getStaffDetail(req, res, next) {
    try {
      let id = req.query.id;
      let type = req.query.type;
      const user = await AdminService.getStaffDetail(id, type);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deleteUser(req, res, next) {
    try {
      let id = req.body.id;
      let logged_user = req.user.user_type;
      console.log("type", logged_user)
      const response = await AdminService.deleteUser(id, logged_user);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deactivateUser(req, res, next) {
    try {
      let id = req.body.id;
      let is_deactivate = req.body.is_deactivate;
      let deactivate_reason = req.body.deactivate_reason;
      let logged_user = req.user.user_type;
      console.log("type", logged_user)
      const response = await AdminService.deactivateUser(id, logged_user, is_deactivate, deactivate_reason);
      if (response) {
        return res.status(200).json("User deactivate status changed.")
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async verifyUser(req, res, next) {
    try {
      let id = req.body.id;
      let is_verified = req.body.is_verified;
      let logged_user = req.user.user_type;
      console.log("type", logged_user)
      const response = await AdminService.verifyUser(id, logged_user, is_verified);
      if (response) {
        return res.status(200).json("User verified status changed.")
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async blockStaff(req, res, next) {
    try {
      console.log(1)
      let id = req.body.id;
      let unblock = req.body.unblock
      const result = await AdminService.blockUser(id, unblock);
      if (result) {
        console.log(3)
        return res.status(200).json({ message: 'Block status updated' })
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deleteProject(req, res, next) {
    try {
      let project_id = req.body.id;
      await AdminService.deleteProject(project_id);
      return res.status(200).json({
        message: 'Project Deleted.'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  // Content Management

  //add content
  static async addContent(req, res, next) {
    try {
      let title = req.body.title;
      let content = req.body.content;
      let image = req.body.image;
      console.log(req.body)
      const result = await AdminService.addContent(title, content, image);
      if (result) {
        return res.status(200).json(result);
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Get Contents
  static async getContents(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const contents = await AdminService.getContents(keyword, limit, page);
      return res.status(200).json(contents)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Get Content Details
  static async getContentDetail(req, res, next) {
    try {
      let id = req.query.content_id
      const content = await AdminService.getContentDetail(id);
      return res.status(200).json(content[0])
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Edit content
  static async editContent(req, res, next) {
    try {
      let id = req.body.id;
      let title = req.body.title;
      let content = req.body.content;
      let image = req.body.image;
      await AdminService.editContent(id, title, content, image);
      return res.status(200).json({
        message: 'content Updated.'
      })

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Delete content
  static async deleteContent(req, res, next) {
    try {
      let content_id = req.body.id;
      await AdminService.deleteContent(content_id);
      return res.status(200).json({
        message: 'content Deleted.'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Delete content
  static async deleteContact(req, res, next) {
    try {
      let contact_id = req.body.id;
      await AdminService.deleteContact(contact_id);
      return res.status(200).json({
        message: 'contact deleted.'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Send Mail
  static async sendNotification(req, res, next) {
    try {
      let user_id=req.user.id
      let email = req.body.emails;
      let subject = req.body.subject;
      let push_content = req.body.push_content;
      let email_content = req.body.email_content;
      let type = req.body.type;
      let notification_type = req.body.notification_type;
      if(type==0){
        await AdminService.saveMessage(type,null,subject, push_content,user_id)
      }else{
        await AdminService.saveMessage(type,email,subject, push_content,user_id)
      }
      if (type == 0 && notification_type == 0) {

        await AdminService.sendMail(null, subject, email_content);

      } else if (type == 1 && notification_type == 0) {

        await AdminService.sendMail(email, subject, email_content);

      } else if (type == 0 && notification_type == 1) {

        await AdminService.sendPush(null, subject, push_content);

      } else if (type == 1 && notification_type == 1) {

        await AdminService.sendPush(email, subject, push_content);

      } else if (type == 1 && (notification_type == null || notification_type == 2)) {

        await AdminService.sendPush(email, subject, push_content);
        await AdminService.sendMail(email, subject, email_content);

      } else if (type == 0 && (notification_type == null || notification_type == 2)) {

        await AdminService.sendPush(null, subject, push_content);
        await AdminService.sendMail(null, subject, email_content);

      }

      return res.status(200).json({
        message: 'Mail Sent Successfully'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Get Contacts
  static async getContacts(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const contacts = await AdminService.getContacts(keyword, limit, page);
      return res.status(200).json(contacts)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //DB Backup
  static async dbBackup(req, res, next) {
    try {
      const response = await AdminService.dbBackup();
      return res.status(200).json({ dbname: response })
      // if (response) {
      //   return res.status(200).json("Backup Successfully Completed.")
      // }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

}
module.exports = AdminController;