require('dotenv').config();
const multer = require('multer');
const sharp = require('sharp');
//const path = require('path');
var AWS = require('aws-sdk');
// const {
//     BlobServiceClient,
//     StorageSharedKeyCredential,
//     newPipeline
// } = require('@azure/storage-blob');

// const sharedKeyCredential = new StorageSharedKeyCredential(
//     process.env.AZURE_STORAGE_ACCOUNT_NAME,
//     process.env.AZURE_STORAGE_ACCOUNT_ACCESS_KEY);

// const pipeline = newPipeline(sharedKeyCredential);

// const blobServiceClient = new BlobServiceClient(
//     `https://${process.env.AZURE_STORAGE_ACCOUNT_NAME}.blob.core.windows.net`,
//     pipeline
// );

const containerName = 'files'

const { Storage } = require('@google-cloud/storage');
const logger = require('../../config/logger')
const UploadService = require('./upload.service')
const path = require('path');
const fs = require('fs');
const { dirname } = require('path');
const BUCKET_NAME = process.env.BUCKET_NAME;

const storage = new Storage();

//const account = "";
// const defaultAzureCredential = new DefaultAzureCredential();

// const blobServiceClient = new BlobServiceClient(
//     `https://${account}.blob.core.windows.net`,
//     defaultAzureCredential
// );

AWS.config.update({
    accessKeyId: "AKIAS5K4ICWL4L2GKW56",
    secretAccessKey: "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",
    region: "us-east-2"
});
var s3 = new AWS.S3({
    endpoint: "https://bucket-traxi.s3.us-east-2.amazonaws.com/"
});

class UploadController {

    //Upload Local
    static async uploadImageToLocal(req, res, next) {
        var file = req.file
       
           try {
                let result = await UploadService.uploadImageToLocal(res, file)
                return res.status(200).json(
                    result
                );
            } catch (err) {
                console.log(err);
                return res.status(400).json({ 'error': "bad_request", 'error_description': err });
            }
       

    }
    static async getFromLocal(req, res, next) {
        let folder = req.query.folder;
        let filename = req.query.filename
        try {
            return await UploadService.getFromLocal(res, filename, folder);
        } catch (err) {
            console.log(err);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });

        }
    }


    static async getFromAws(req, res, next) {
        try {
            let filename = req.query.filename;
            let folder = req.query.type;
            let data = await UploadService.getFromAws(filename, folder)
            res.writeHead(200, { 'Content-Type': 'image/jpeg' });
            res.write(data.Body, 'binary');
            res.end(null, 'binary');
        }
        catch (err) {
            console.log(err);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });

        }
    }

    static async uploadImageToAws(req, res, next) {
        try {
            var file = req.file
            console.log("File: ", file)

            const result = await UploadService.uploadImageToAws(file);
            if (result) {
                return res.status(200).json(result);
            }
        }
        catch (err) {
            console.log(err)
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async uploadImageToAzure(req, res, next) {
        console.log("Azure")
        let file = req.file
        try {
            return await UploadService.uploadImageToAzure(file, res)
        } catch (err) {
            console.log(err);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });

        }
    }

    static async getFromAzure(req, res, next) {
        try {
            let filename = req.query.filename;
            let folder = req.query.type;
            const data = await UploadService.getFromAzure(folder, filename);
            res.writeHead(200, { 'Content-Type': 'image/jpeg' });
            res.write(data.Body, 'binary');
            res.end(null, 'binary');
        }
        catch (err) {
            return res.status(400).json({ 'error': "bad_request", 'error_description': err })
        }
    }

    static async uploadImageToGc(req, res, next) {
        try {
            var file = req.file;
            const data = await UploadService.uploadImageToGc(file);
            return data
        }
        catch (err) {
            console.log(err);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });

        }
    }



    //API for image get
    static async getFromGc(req, res, next) {
        let file = req.file;
        try {
            const data = await UploadService.getFromGc(file);
            res.writeHead(200, { 'Content-Type': 'image/jpeg' });
            res.write(data.Body, 'binary');
            res.end(null, 'binary');
        }
        catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            })
        }
    }

}
module.exports = UploadController;
