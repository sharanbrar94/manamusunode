var fcm = require("fcm-notification");
var FCM = new fcm("./public/firebase.json");

// exports.push = (type, body, token) => {
//   let message = {};
//   switch (type) {
//     case 1:
//       message = {
//         android: {
//           ttl: 3600 * 1000,
//           priority: "normal",
//           notification: {
//             title: "New Message received",
//             body,
//             icon: "slack.png",
//             color: "#f45342",
//           },
//         },
//         token,
//       };
//       break;
//     case 2:
//       message = {
//         apns: {
//           ttl: 3600 * 1000, // 1 hour in milliseconds
//           priority: "normal",
//           notification: {
//             title: "New Message received",
//             body,
//             icon: "slack.png",
//             color: "#f45342",
//           },
//         },
//         token,
//       };
//       break;
//     case 3:
//       message = {
//         webpush: {
//           notification: {
//             title: "New Message received",
//             body,
//             icon: "./public/slack.png",
//           },
//         },
//         token,
//       };
//       break;
//   }
var message = {
  notification: {
    title: '$GOOG up 1.43% on the day',
    body: '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
  },
  android: {
    ttl: 3600 * 1000,
    notification: {
      icon: 'stock_ticker_update',
      color: '#f45342',
    },
  },
  apns: {
    payload: {
      aps: {
        badge: 42,
      },
    },
  },
  topic: 'TopicName'
};
FCM.send(message, function (err, response) {
  if (err) {
    console.log("error found", err);
  } else {
    console.log("response here", response);
  }
});
//};

exports.broadcast = (body) => {
  var message = {
    notification: {
      body,
    },
    android: {
      ttl: 3600 * 1000,
      notification: {
        body,
        icon: "stock_ticker_update",
        color: "#f45342",
      },
    },
    apns: {
      payload: {
        aps: {
          badge: 42,
          body,
        },
      },
    },
    topic: "/to/all",
  };

  fcm.send(message, function (err, response) {
    if (err) {
      console.log("Something has gone wrong! ", err);
    } else {
      console.log("Successfully sent with response: ", response);
    }
  });
};
