var express = require('express');
const { checkSchema } = require('express-validator');
let userSchema = require("./login.validation.json")
var router = express.Router();
const valid = require('../../config/validator')
const LoginController = require("./login.controller");

let auth = require("../../middleware/auth")
router.post('/signup', LoginController.signup);
router.post('/login', LoginController.login);
router.get('/login-as-user', LoginController.loginAsUser);
router.get('/static-data', LoginController.staticData);
router.get('/static-pages', LoginController.staticPages);

router.post('/activateDeactivate',auth.checkOptionalToken, LoginController.activateDeactivate);
router.post('/contact-us', LoginController.contactUs);
router.get('/nationality', LoginController.nationality);
router.get('/languages', LoginController.languages);
router.get('/occupations', LoginController.occupations);
//profile module
router.get('/profile', auth.checkOptionalToken, LoginController.getUserProfile);
router.put('/profile', auth.checkToken, LoginController.updateUserProfile);
//academic module
router.post('/academic', auth.checkToken, LoginController.addAcademic);
router.put('/academic', auth.checkToken, LoginController.editAcademic);
router.get('/academic', auth.checkToken, LoginController.getAcademicDetail);
router.delete('/academic', auth.checkToken, LoginController.deleteAcademic);

router.get('/certificates', auth.checkOptionalToken,LoginController.getCertificateDetail);
router.post('/certificates',auth.checkOptionalToken,LoginController.addCertificate);
router.put('/certificates', auth.checkOptionalToken,LoginController.editCertificate);
router.delete('/certificates', auth.checkOptionalToken,LoginController.deleteCertificates);

router.post('/forgot/password', auth.checkOptionalToken, checkSchema(userSchema.forgotPassword), valid, LoginController.forgotPassword);
router.post('/check-email-otp', auth.checkOptionalToken, LoginController.checkEmailOtp);
router.post('/reset/password', auth.checkToken, LoginController.resetPassword);
//change password
router.post('/user/change-password', auth.checkToken, LoginController.changePassword);
router.post('/verify/email',auth.checkOptionalToken, checkSchema(userSchema.verifyEmail), valid, LoginController.verifyEmail);
router.post('/resend/verification-email', auth.checkToken, LoginController.resendEmail);
router.post('/resend/verification-Phone', auth.checkToken, LoginController.resendPhone);
router.post('/verify/phone', auth.checkToken, LoginController.verifyPhone);
router.post('/reset/notifications', auth.checkToken, LoginController.resetNotification);
router.delete('/logout', auth.checkToken,  LoginController.logout);
router.post('/deactivate/account', auth.checkToken,checkSchema(userSchema.logout), valid, checkSchema(userSchema.deactivateAccount), valid, LoginController.deactivateAccount)

router.post('/connect-account-webhook', LoginController.connectAccountWebhook);

module.exports = router;
