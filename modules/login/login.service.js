const { sign } = require('jsonwebtoken');
const nodemailer = require('nodemailer')
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const logger = require('../../config/logger');
let Common = require("../../middleware/common");
const { cloneDeep, last } = require('lodash');
const stripe = require('stripe')('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');
//const stripe = require('stripe')('sk_test_51JAVmXG8oPs9xsvBJhSw3b1t8rIj5ytAmWnXYVPTBHRPa0XAgAUogARsw4Z0R1rhGWWIavikDiMMPgtRfxY9ZESP008bXrCUSx');

class LoginService {
    static async signup(first_name, last_name,gender, email, confirm_email, password, confirm_password, user_type,fcm_id, device_id, device_type,lang) {
        first_name = first_name.trim()
        last_name = last_name.trim()
        let getLangMsg = await Common.localization(lang)

        let user = await this.getUserProfileByEmail(email);
        if (user[0]) {
            throw (getLangMsg.email_exist);
        }
        if (email) {
            if (confirm_email != email) {
                throw (getLangMsg.email_not_match)
            }
        }
        if (password) {
            if (confirm_password != password) {
                throw (getLangMsg.password_not_match)
            }
        }
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        const email_verify_token = await Common.generateRandomString(15);
        //const phone_verification_code = 1234;
        var sqlQuery = 'insert into users(gender,first_name, last_name, email,password,user_type,email_verify_token) values (?,?,?,?,?,?,?)';
        let params = [gender,first_name, last_name, email, password, user_type,email_verify_token]
        let userInsert = await connection.query(sqlQuery, params);
        //add in stripe
        await this.createCustomer(userInsert.insertId,email)
        //get profile detail
        user = await this.getUserProfileById(userInsert.insertId)
        
        //generate token
        let token = await this.generateToken(user);
        await this.insertOnUpdateDeviceInfo(device_id, fcm_id, device_type, userInsert.insertId)
        
        //send email
        let getUrl = process.env.WEBSITE_URL
        let url = getUrl+'/verification/'+email_verify_token
        if(user_type == 1){
            await Common.userStudentVerification(email, getUrl,url,first_name);
            
        }
        if(user_type == 2){
            await Common.userTeacherVerification(email, getUrl,url,first_name);
            
        }
        
        
        delete user[0].password
        delete user[0].email_verify_token
        if (user.length) {
            let unread_count = await this.getUnreadCount(user[0].id)
            user[0].unread_count = unread_count[0].unread_count
        }
        
        return ({ 'message': getLangMsg.welcome +" "+ first_name + getLangMsg.login_successfull, token: token, user: user[0] })

    }
    static async login(inputs,lang) {
        let getLangMsg = await Common.localization(lang)

        let user_id
        let user = await this.getUser(inputs.email, "email");
        if (user[0]) {
            if(user[0].is_deactivated == 1){
                var userQueryy = 'update users set is_deactivated=? where id=?';
                let userQueryyParams = [0,user[0].id]
                await connection.query(userQueryy, userQueryyParams)
            }
            user_id = user[0].id
            if (inputs.password) {
                let passwordResult = compareSync(inputs.password, user[0].password);
                if (!passwordResult) {
                    throw (getLangMsg.password_wrong)
                }
            }
            // if (user[0].is_user_verified == 0) {
            //     throw ("Your account not verified. Kindly check your email.")
            // }
        }
        else{
            throw (getLangMsg.email_not_exist)
        }
        if(user[0].customer_id == ''){
            await this.createCustomer(user[0].id,user[0].email)
        }
        user = await this.getUserProfileById(user_id);
        let getTotalHour = user[0].total_hours/parseInt(60)
        getTotalHour = parseInt(getTotalHour)
        user[0].total_hours = getTotalHour
        let get_bank_detail = await this.getBankDetail(user_id);
        await this.generateToken(user);
        let auth_token = await this.generateToken(user);
        if (user.length) {
            let unread_count = await this.getUnreadCount(user_id)
            user[0].unread_count = unread_count[0].unread_count
        }
        return ({ 'message': getLangMsg.welcome +" "+ user[0].first_name + getLangMsg.login_successfull, 'token': auth_token, 'user': user[0],'bank_detail':get_bank_detail });
    }
    //Add user in stripe
    static async createCustomer(user_id,email_address){
        const customer = await stripe.customers.create({
            email : email_address
        });
        let customer_id = customer['id'];
        var stripeQuery = 'update users set customer_id=? where id=?';
        let stripeParams = [customer_id,user_id]
        return await connection.query(stripeQuery, stripeParams)
    }

    static async getUserProfile(id) {
        let sqlQuery = `select *,IFNULL((select Avg(rating) from booking_reviews where booking_reviews.to_user_id=users.id),0) as user_rating from users where id=${id}`
        let user = await connection.query(sqlQuery)
        let getTotalHour = user[0].total_hours/parseInt(60)
        getTotalHour = parseInt(getTotalHour)
        user[0].total_hours = getTotalHour
        return user[0]
    }
    static async getUserLanguage(id){
        let sqlQuery = `select user_languages.*,n.name,n.name_jp from user_languages left join languages as n on user_languages.lang_id = n.id where user_languages.user_id=${id}`
        let nat = await connection.query(sqlQuery)
        return nat
    }
    static async getBankDetail(id) {
        let sqlQuery = `select * from bank_details where user_id=${id}`
        let bankDetail = await connection.query(sqlQuery)
        return bankDetail[0]
    }
    static async getPersonalDetail(id){
        let sqlQuery = `select * from stripe_personal_details where user_id=${id} order by id desc`
        let personalDetail = await connection.query(sqlQuery)
        return personalDetail[0]
    }

    static async getUnreadCount(user_id) {
        const query = `SELECT count(is_read) as unread_count from notification_tos WHERE to_user_id = ${user_id} and is_read=0 `;
        return await connection.query(query)
    }

    static async generateToken(user) {
        let token = await sign({ user: user[0] }, 'secretkey', { expiresIn: "2 Days" });
        return token

    }

    static async getUserProfileByEmail(email) {
        let sqlQuery = `select * from users where email=?`
        return await connection.query(sqlQuery, [email])

    }
    static async getUserProfileById(id) {
        let sqlQuery = `select * from users where id=?`
        return await connection.query(sqlQuery, [id])

    }

    static async getUserProfileByPhone(phone_no, country_code) {
        let sqlQuery = `select * from users where phone_number=? and country_code=?`
        return await connection.query(sqlQuery, [phone_no, country_code])

    }

    static async getUserByToken(token) {
        let sqlQuery = `select * from users where email_verify_token=?`
        return await connection.query(sqlQuery, [token])

    }

    static async getUserByCode(code, id) {

        let sqlQuery = `select * from users where phone_verify_code=? and id=?`
        return await connection.query(sqlQuery, [code, id])

    }

    static async getUserProfileByToken(token) {
        let sqlQuery = `select * from users where reset_pass_token=?`
        return await connection.query(sqlQuery, [token])

    }

    static async resetpass(email, password) {
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        let sqlQuery = 'update users set password=? where email=?'
        let params = [password, email]
        let sql = 'update users set reset_pass_token=null where email=?'
        let param = [email]
        await connection.query(sqlQuery, params);
        return await connection.query(sql, param);

    }

    static async updateEmailVerifyToken(email) {
        let sqlQuery = 'update users set email_verify_token=null, is_email_verified = 1,is_user_verified=1 where email=?'
        let params = [email]
        return await connection.query(sqlQuery, params);

    }

    static async updateEmailResendToken(token, email) {
        let sqlQuery = 'update users set email_verify_token=?, is_email_verified = 0 where email=?'
        let params = [token, email]
        return await connection.query(sqlQuery, params);

    }

    static async updatePhoneResendCode(token, phone, country_code) {
        let sqlQuery = 'update users set phone_verify_code=?, is_phone_verified = 0 where phone_number=? and country_code =?'
        let params = [token, phone, country_code]
        return await connection.query(sqlQuery, params);
    }

    static async updatePhoneVerifyCode(Phone_no, country_code) {
        let sqlQuery = 'update users set phone_verify_code=null, is_phone_verified = 1 where phone_number=? and country_code=?'
        let params = [Phone_no, country_code]
        return await connection.query(sqlQuery, params);

    }

    static async insertOnUpdateDeviceInfo(device_id, fcm_id, device_type, user_id) {
        let expired_at = null;
        let sqlQuery = `insert into user_logins(user_id,device_id,fcm_id,device_type,expired_at) values(?,?,?,?,?) ON DUPLICATE KEY UPDATE user_id=?,fcm_id=?,device_type=?,expired_at=?`
        let params = [user_id, device_id, fcm_id, device_type, expired_at, user_id, fcm_id, device_type, expired_at]
        return await connection.query(sqlQuery, params)

    }

    static async changePassword(new_password, old_password, user_id,lang) {
        let getLangMsg = await Common.localization(lang)
        let getUser = await connection.query(`select * from users where id=${user_id}`)
        let user_password = getUser[0].password
        let result = compareSync(old_password, user_password);
        if (!result) {
           throw (getLangMsg.correct_old_password);
        } else {
            const salt = genSaltSync(10);
            let password = hashSync(new_password, salt);
            let sqlQuery = `update users set password=? where id=?`
            let params = [password, user_id]
            await connection.query(sqlQuery, params)
            return { message: getLangMsg.password_changed }
        }
    }

    static async logout(user_id, device_id) {
        console.log(user_id);
        let date = new Date();

        let result = await connection.query(`select * from user_logins where device_id='${device_id}' and user_id=${user_id}`)
        console.log(result);
        if (!result.length) {
            throw ('Device id is wrong.')
        } else {
            let sqlQuery = 'update user_logins set expired_at=? where device_id=?'
            let params = [date, device_id]
            return await connection.query(sqlQuery, params)
        }

    }
    //profile module
    static async updateUserProfile(logged_id, first_name, last_name,dob,email,gender,nationality,occupation,language, country,profile_image, description,paypal_username,vectera_id,lang) {
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = 'update users set '
        let params = []
        let sql = []
        let data = 0
        if(nationality != undefined){
            let sqlQuery = 'update users set nationality=? where id=?'
            let params = [nationality,logged_id]
            await connection.query(sqlQuery, params)
        }
        
        if (first_name != undefined) {
            let sqlQuery = 'update users set first_name=? where id=?'
            let params = [first_name,logged_id]
            await connection.query(sqlQuery, params)
        }
        if (last_name != undefined) {
            let sqlQuery = 'update users set last_name=? where id=?'
            let params = [last_name,logged_id]
            await connection.query(sqlQuery, params)
        }
        if (dob != undefined) {
            let sqlQuery = 'update users set dob=? where id=?'
            let params = [dob,logged_id]
            await connection.query(sqlQuery, params)
        }
        if (email != undefined) {
            let sqlQuery = 'update users set email=? where id=?'
            let params = [email,logged_id]
            await connection.query(sqlQuery, params)
        }
        if (vectera_id != undefined) {
            let sqlQuery = 'update users set vectera_id=? where id=?'
            let params = [vectera_id,logged_id]
            await connection.query(sqlQuery, params)
        }
        if (gender != undefined) {
            let sqlQuery = 'update users set gender=? where id=?'
            let params = [gender,logged_id]
            await connection.query(sqlQuery, params)
        }
        
        if (occupation != undefined) {
            let sqlQuery = 'update users set occupation=? where id=?'
            let params = [occupation,logged_id]
            await connection.query(sqlQuery, params)
        }
        if (language) {
            await connection.query('delete from user_languages where user_id=?', [logged_id])
            for (var i in language) {
                console.log(language[i])
                
                await connection.query('insert into user_languages (user_id,lang_id) values (?,?)', [logged_id, language[i]])
            }
        }
        if (country != undefined) {
            if(country == 2){
                await connection.query('update users set bank_status=?,paypal_username=? where id=? ', [0,'',logged_id])
            }
            let sqlQuery = 'update users set country=? where id=?'
            let params = [country,logged_id]
            await connection.query(sqlQuery, params)
        }
        if (profile_image != undefined) {
            let sqlQuery = 'update users set profile_image=? where id=?'
            let params = [profile_image,logged_id]
            await connection.query(sqlQuery, params)
        }
        if (description != undefined) {
            let sqlQuery = 'update users set description=? where id=?'
            let params = [description,logged_id]
            await connection.query(sqlQuery, params)
        }
        if (paypal_username != undefined) {
            let sqlQuery = 'update users set paypal_username=? where id=?'
            let params = [paypal_username,logged_id]
            await connection.query(sqlQuery, params)

            let userSqlQuery = 'update users set bank_status=? where id=?'
            let userSqlParams = [1, logged_id]
            await connection.query(userSqlQuery, userSqlParams)

        }
        let getUserDetail = await this.getUserProfileById(logged_id);
        return { message: getLangMsg.profile_updated,user:getUserDetail[0] }
    }
    static async getEducationLevel(lang){
        var getCol = await Common.dbColumnLang("name",lang)
        let sqlQuery = `select id,${getCol} as name from level_educations where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async getCountries(lang){
        var getCol = await Common.dbColumnLang("name",lang)
        let sqlQuery = `select id,${getCol} as name from counties where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async getEducationYear(){
        let sqlQuery = `select * from year_educations where deleted_at is null order by year asc`
        return await connection.query(sqlQuery)
    }
    static async getSubjects(lang){
        var getCol = await Common.dbColumnLang("name",lang)
        let sqlQuery = `select id,${getCol} as name from subjects where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async getTeachingStandards(lang){
        var getCol = await Common.dbColumnLang("name",lang)
        let sqlQuery = `select id,${getCol} as name from  teaching_standards where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async getPopularFilter(){
        let sqlQuery = `select * from popular_filters`
        return await connection.query(sqlQuery)
    }
    static async getComplainSubject(lang){
        var getCol = await Common.dbColumnLang("name",lang)
        let sqlQuery = `select id,${getCol} as name from complain_subjects where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async getPrivacyPolicy(){
        let sqlQuery = `select * from static_pages where id = 4`
        let privacy = await connection.query(sqlQuery)
        return privacy[0]
    }
    static async staticPages(id,lang){
        var getTitle = await Common.dbColumnLang("title",lang)
        var getContent = await Common.dbColumnLang("content",lang)
        let sqlQuery = `select id,image,${getTitle} as title,${getContent} as content from static_pages where id = ${id}`
        let param = await connection.query(sqlQuery)
        return param[0]
    }
    /*======activate deactivate account========*/
    static async activateDeactivate(status,user_id,lang){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = 'update users set is_deactivated=? where id=?'
        let params = [status,user_id]
        await connection.query(sqlQuery, params)
        let user = await this.getUserProfileById(user_id);
        return { message: getLangMsg.status_update,user:user[0] }
    }
    /*=====contact us=====*/
    static async contactUs(name,email,country_code,phone_number,message,lang){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = `insert into contact_us(name,email,country_code,phone_number,message) values(?,?,?,?,?)`
        let params = [name,email,country_code,phone_number,message]
        await connection.query(sqlQuery, params)
        return { message: getLangMsg.send_succesfully }
    }
    /*========academic module======*/
    static async addAcademic(user_id,school,level,country,year,image,lang){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = `insert into academics(user_id,school,level,country,graduation_year,image) values(?,?,?,?,?,?)`
        let params = [user_id, school, level, country, year, image]
        await connection.query(sqlQuery, params)
        return { message: getLangMsg.academic_added }
    }
    static async addUserCertificate(user_id,certificate_name,result,year,description,image,lang){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = `insert into certificates(user_id,name,result,year,description,image) values(?,?,?,?,?,?)`
        let params = [user_id, certificate_name, result, year, description, image]
        await connection.query(sqlQuery, params)
        return { message: getLangMsg.certificate_added }
    }
    static async editAcademic(user_id,academic_id,school,level,country,year,image,lang){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = 'update academics set user_id=?,school=?,level=?,country=?,graduation_year=?,image=? where id=?'
        let params = [user_id,school,level,country,year,image,academic_id]
        await connection.query(sqlQuery, params)
        return { message: getLangMsg.academic_updated }
    }
    static async getAcademicDetail(academic_id){
        let sqlQuery = `select * from academics where id=?`
        return await connection.query(sqlQuery, [academic_id])
    }
    static async getAcademics(user_id){
        let sqlQuery = `select * from academics where user_id=?`
        return await connection.query(sqlQuery, [user_id])
    }
    /*======certificate module========*/
    
    static async addCertificate(user_id,certificate_name,result,year,description,image,lang){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = `insert into certificates(user_id,name,result,year,description,image) values(?,?,?,?,?,?)`
        let params = [user_id, certificate_name, result, year, description, image]
        await connection.query(sqlQuery, params)
        return { message: getLangMsg.certificate_added }
    }
    static async editCertificate(user_id,certificate_id,certificate_name,result,year,description,image,lang){
        let getLangMsg = await Common.localization(lang)
        
        let sqlQuery = 'update certificates set user_id=?,name=?,result=?,year=?,description=?,image=? where id=?'
        let params = [user_id,certificate_name,result,year,description,image,certificate_id]
        await connection.query(sqlQuery, params)
        return { message: getLangMsg.certificate_updated }
    }
    static async deleteCertificates(user_id,certificate_id,lang){
        await connection.query('delete from certificates where user_id = ? and id = ?', [user_id,certificate_id])
        return { message: "Deleted" }
    }
    static async deleteAcademic(user_id,id,lang){
        await connection.query('delete from academics where user_id = ? and id = ?', [user_id,id])
        return { message: "Deleted" }
    }
    static async getCertificateDetail(certificate_id){
        let sqlQuery = `select * from certificates where id=?`
        return await connection.query(sqlQuery, [certificate_id])
    }
    
    static async getCertificates(user_id){
        let sqlQuery = `select * from certificates where user_id=?`
        return await connection.query(sqlQuery, [user_id])
    }
    static async getTeacherPosts(user_id){
        let sqlQuery = `select posts.*,s.name as subject_name,(select Avg(rating) from reviews where reviews.post_id=posts.id) as post_rating,u.first_name,u.last_name,u.profile_image from posts left join users as u on posts.user_id = u.id left join subjects as s on posts.subject = s.id where posts.user_id=? and posts.type = ? and posts.deleted_at is null `
        sqlQuery += ` order by posts.id DESC LIMIT 10`
        let posts = await connection.query(sqlQuery, [user_id,'post'])
        return posts
    }
    static async getTeacherReviews(user_id){
        let sqlQuery = `select booking_reviews.*,b.booking_unique_id,p.title,p.id as post_id,fu.first_name as student_first_name,fu.last_name as student_last_name,fu.profile_image as student_profile_image,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.profile_image as teacher_profile_image from booking_reviews left join users as fu on booking_reviews.from_user_id=fu.id left join users as tu on booking_reviews.to_user_id=tu.id left join booking_requests as b on booking_reviews.booking_id = b.id left join posts as p on b.post_id = p.id where booking_reviews.to_user_id=? order by booking_reviews.id DESC LIMIT 4`
        const reviews = await connection.query(sqlQuery, [user_id])
        return reviews
        // let sqlQuery = `select booking_reviews.*,u.first_name as student_first_name,u.last_name as student_last_name,u.profile_image as student_profile_image from booking_reviews left join users as u on booking_reviews.from_user_id = u.id `
        // sqlQuery += ` order by booking_reviews.id DESC LIMIT 4`
        // let reviews = await connection.query(sqlQuery, [user_id,'post'])
        // return reviews
    }
    static async getStudentRequests(user_id){
        let sqlQuery = `select requests.*,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id where requests.user_id=? and requests.deleted_at is null`
        sqlQuery += ` order by requests.id DESC LIMIT 10`
        let requests = await connection.query(sqlQuery, [user_id])
        return requests
    }

    static async updateResetPasswordToken(email, otp) {
        let sqlQuery = 'update users set email_otp=? where email=?'
        let params = [otp, email]
        return await connection.query(sqlQuery, params)

    }

    static async getUser(param, param_name) {
        let sqlQuery = `select * from users where ${param_name}=? and deleted_at is null and is_blocked = 0 and user_type != 3`
        return await connection.query(sqlQuery, [param])

    }

    static async socialLogin(fb_id, google_id, apple_id, email, first_name, last_name) {

        var sqlQuery = 'insert into users(fb_id, google_id, apple_id,email,first_name,last_name) values (?,?,?,?,?,?)';
        let params = [fb_id, google_id, apple_id, email, first_name, last_name]
        return await connection.query(sqlQuery, params)

    }

    static async forgotPassword(email,lang) {
        let getLangMsg = await Common.localization(lang)
        let user = await this.getUserProfileByEmail(email);
        if (user.length > 0) {
            let otp = Math.floor(1000 + Math.random() * 9000);
            let pass = await this.updateResetPasswordToken(email, otp)
            //  console.log(pass);
            let getUrl = process.env.WEBSITE_URL
            await Common.forgotPassword(email,getUrl,otp,user[0].first_name);
            return { message: getLangMsg.email_sent}
        } 
        else {
            throw (getLangMsg.email_not_exist);
        }

    }
    static async checkEmailOtp(otp,lang){
        let getLangMsg = await Common.localization(lang)
        let sqlQuery = `select * from users where email_otp=?`
        let user =  await connection.query(sqlQuery, [otp])
        if(user.length > 0){
            let token = await this.generateToken(user);
            return { token: token , user_id:user[0].id}
        }
        else{
            throw (getLangMsg.wrong_otp);
        }
    }

    static async resetPassword(user_id, password,lang) {
        let getLangMsg = await Common.localization(lang)
        let sqlQuery = `select * from users where id=?`
        let user =  await connection.query(sqlQuery, [user_id])
        if (user.length > 0) {
            await this.resetpass(user[0].email, password)
            return ({ message: getLangMsg.password_changed })
        } else {
            throw (getLangMsg.invalid_user);
        }

    }

    static async resetNotification(id, email, push, sms) {
        if (email != undefined) {
            await this.updateEmailNotification(id, email)
        }
        if (push != undefined) {
            await this.updatePushNotification(id, push)
        }
        if (sms != undefined) {
            await this.updateSmsNotification(id, sms)
        }

        return { message: "Notification status updated." }

    }

    static async updateEmailNotification(id, email) {
        let sqlQuery = `update users set is_email_notified=${email} where id=${id}`
        return await connection.query(sqlQuery)
    }

    static async updatePushNotification(id, push) {
        let sqlQuery = `update users set is_push_notified=${push} where id=${id}`

        return await connection.query(sqlQuery)
    }

    static async updateSmsNotification(id, sms) {

        let sqlQuery = `update users set is_sms_notified=${sms} where id=${id}`
        return await connection.query(sqlQuery)

    }

    static async verifyEmail(token) {
        let user = await this.getUserByToken(token);
        if (user.length > 0) {
            let email = user[0].email;
            await this.updateEmailVerifyToken(email);
            let getUser = await this.getUserProfileByEmail(email);
            let auth_token = await this.generateToken(getUser);
            //send email
            let getUrl = process.env.WEBSITE_URL
            if(user[0].user_type == 1){
                await Common.verificationStudent(email,getUrl,user[0].first_name)
            }
            if(user[0].user_type == 2){
                await Common.verificationTeacher(email,getUrl,user[0].first_name)
            }
            return { message: 'Email verified successfully',token:auth_token,user:getUser[0] }
        } else {
            throw ("Invalid token");
        }
    }

    static async verifyPhone(code, id) {
        let user = await this.getUserByCode(code, id);
        if (user.length > 0) {
            let phone_no = user[0].phone_number;
            let country_code = user[0].country_code;
            await this.updatePhoneVerifyCode(phone_no, country_code);
            return { message: 'Phone verified successfully' }
        } else {
            throw ("Invalid code");
        }
    }

    static async resendEmail(email) {
        let user = await this.getUserProfileByEmail(email);
        if (user.length > 0) {
            var email_verify_token = await Common.generateRandomString(15);
            await this.updateEmailResendToken(email_verify_token, email)
            // let subject = 'Email Verification'
            // let url = `https://app-transfer.com:3008/verify-email/${email_verify_token}`
            // let text = "To verify you email, please click on the link below."
            // let link_text = "Verify your email here"
            // await Common.sendMail(email, url, subject, text, link_text);

            let getUrl = process.env.WEBSITE_URL
            let url = getUrl+'/verification/'+email_verify_token
            if(user[0].user_type == 1){
                await Common.userStudentVerification(email, getUrl,url,user[0].first_name);
            }
            if(user[0].user_type == 2){
                await Common.userTeacherVerification(email, getUrl,url,user[0].first_name);
            }
            return { message: 'Email sent successfully' }
        } else {
            throw ("User does not exist.");
        }
    }

    static async resendPhone(phone, country_code) {
        let user = await this.getUserProfileByPhone(phone, country_code);
        if (user.length > 0) {
            // let phone_verification_code = await Common.generateRandomNumber(6);
            let phone_verification_code = "111111"
            await this.updatePhoneResendCode(phone_verification_code, phone, country_code)
            // if(user.is_sms_notified == 1){
            // let message = "Your verification code is " + phone_verification_code
            // let send_msg = await Common.sendMessage(phone_number, message)
            return { message: 'Message sent successfully to your phone number.' }
            // }
        } else {
            throw ("User does not exist.");
        }
    }

    static async deactivateAccount(deactivate_reason, password, user_pass, user_id) {
        if (password) {
            let passwordResult = compareSync(password, user_pass);
            if (passwordResult == false) {
                throw ("Please enter correct password");
            } else {
                let sqlQuery = 'update users set deactivate_reason=?,is_deactivated=1 where id=?'
                let params = [deactivate_reason, user_id]
                await connection.query(sqlQuery, params)
                return ({ id: user_id, message: "Account deactivated successfully!" })
            }
        }
    }

    
    static async loginAsUser(id,token){
        let sqlQuery = `select * from users where id = ${id}`
        let user = await connection.query(sqlQuery)
        let user_id = user[0].id;
        user = await this.getUserProfileById(user_id);
        await this.generateToken(user);
        let auth_token = await this.generateToken(user);
        if (user.length) {
            let unread_count = await this.getUnreadCount(user_id)
            user[0].unread_count = unread_count[0].unread_count
        }
        return ({ 'message': "User login successfully", 'token': auth_token, 'user': user[0] });
    }
    static async getNationality(lang){
        var getCol = await Common.dbColumnLang("name",lang)
        let sqlQuery = `select id,${getCol} as name from counties where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async getLanguages(lang){
        var getCol = await Common.dbColumnLang("name",lang)
        let sqlQuery = `select id,${getCol} as name from languages where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async getOccupations(lang){
        var getCol = await Common.dbColumnLang("name",lang)
        let sqlQuery = `select id,${getCol} as name from occupations where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async updateUserDetail(email, password, id) {
        let sqlQuery = 'update users set '
        let params = []
        let sql = []
        if (email != undefined) {
            params.push(email)
            sql.push('email=?')
        }
        if (password != undefined) {
            params.push(password)
            sql.push('password=?')
        }

        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        return await connection.query(sqlQuery, params)

    }

    static async connectAccountWebhook(data){
        //console.log("testtt",data)
        console.log("jeevan",data.object.individual.verification)
        let query = `select * from users where connect_account_id=?`
        var getUserDetail = await connection.query(query, [data.object.id])
        let user_id = getUserDetail[0].id
        let email = getUserDetail[0].email
        let first_name = getUserDetail[0].first_name
        let getUrl = process.env.WEBSITE_URL
        if(data.object.individual.verification.status == 'unverified'){
            var restrict_issue = data.object.individual.requirements.errors[0].reason
            var status = 0
            await Common.connectAccountRestrict(email, getUrl,first_name,restrict_issue);
        }
        else if(data.object.individual.verification.status == 'pending'){
            var status = 2
        }
        else{
            var status = 1
        }
        var updateBank = 'update bank_details set status=? where user_id=?';
        let updateBankParams = [status,user_id]
        await connection.query(updateBank, updateBankParams)

        return data
    }
}
module.exports = LoginService;













































