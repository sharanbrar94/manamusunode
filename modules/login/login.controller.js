require('dotenv').config();
const logger = require('../../config/logger'); // importiing winston logger module
const jwt = require('jsonwebtoken')
const LoginService = require("./login.service");

class LoginController {
  static async signup(req, res, next) {

    try {
      let inputs = req.body;
      const lang = req.header("lang");
      const { first_name, last_name, gender,email, confirm_email, password, confirm_password, user_type,fcm_id, device_id, device_type} = inputs
      const result = await LoginService.signup(first_name, last_name,gender, email, confirm_email, password, confirm_password, user_type,fcm_id, device_id, device_type,lang)
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      console.log(err)
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getUserProfile(req, res, next) {
    try {
      if(req.query.user_id){
        var user_id = req.query.user_id;
      }
      else{
        var user_id = req.user.id;
      }
      let user = {}
      user.userDetail = await LoginService.getUserProfile(user_id)
      user.language = await LoginService.getUserLanguage(user_id)
      user.academics = await LoginService.getAcademics(user_id)
      user.certificates = await LoginService.getCertificates(user_id)
      user.bank_detail = await LoginService.getBankDetail(user_id)
      user.personal_detail = await LoginService.getPersonalDetail(user_id)
      
      if(user.userDetail.user_type == 2){
        user.posts = await LoginService.getTeacherPosts(user_id)
        user.reviews = await LoginService.getTeacherReviews(user_id)
      }
      if(user.userDetail.user_type == 1){
        user.requests = await LoginService.getStudentRequests(user_id)
      }
      return res.status(200).json(user)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async login(req, res, next) {
    try {
      const lang = req.header("lang");
      let inputs = req.body;
      let user = await LoginService.login(inputs,lang);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async loginAsUser(req, res, next){
    try {
      let id = req.query.id;
      let token = req.query.token;
      let user = await LoginService.loginAsUser(id,token);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async staticData(req, res, next){
    try {
      
      let lang = req.query.lang;
      //let lang = "en";
      let staticData = {}
      staticData['educationLevel'] = await LoginService.getEducationLevel(lang);
      staticData['graduationYear'] = await LoginService.getEducationYear();
      staticData['countries'] = await LoginService.getCountries(lang);
      staticData['nationalities'] = await LoginService.getCountries(lang);
      staticData['languages'] = await LoginService.getLanguages(lang);
      staticData['occupations'] = await LoginService.getOccupations(lang);
      staticData['subjects'] = await LoginService.getSubjects(lang);
      staticData['teaching_standards'] = await LoginService.getTeachingStandards(lang);
      staticData['popular_filter'] = await LoginService.getPopularFilter();
      staticData['complain_subject'] = await LoginService.getComplainSubject(lang);
      return res.status(200).json(staticData)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async staticPages(req, res, next){
    try {
      let id = req.query.id;
      let lang = req.query.lang;
      let staticData = await LoginService.staticPages(id,lang);
      return res.status(200).json(staticData)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }

  }
  static async activateDeactivate(req, res, next){
    try {
      let status = req.body.status;
      let user_id = req.user.id;
      let user = await LoginService.activateDeactivate(status,user_id);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async contactUs(req, res, next){
    try {
      const lang = req.header("lang");
      let name = req.body.name;
      let email = req.body.email;
      let country_code = req.body.country_code;
      let phone_number = req.body.phone_number;
      let message = req.body.message;
      let user = await LoginService.contactUs(name,email,country_code,phone_number,message,lang);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async nationality(req, res, next){
    try {
      let response = await LoginService.getNationality();
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async languages(req, res, next){
    try {
      let response = await LoginService.getLanguages();
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async occupations(req, res, next){
    try {
      let response = await LoginService.getOccupations();
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async forgotPassword(req, res, next) {
    try {
      let email = req.body.email;
      const lang = req.header("lang");
      const result = await LoginService.forgotPassword(email,lang);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async checkEmailOtp(req, res, next){
    try {
      let otp = req.body.otp;
      const lang = req.header("lang");
      const result = await LoginService.checkEmailOtp(otp,lang);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async resetPassword(req, res, next) {
    try {
      const lang = req.header("lang");
      let user_id = req.user.id;
      let password = req.body.password;
      let result = await LoginService.resetPassword(user_id, password,lang)
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  /*=====Change password=======*/
  static async changePassword(req, res, next) {
    try {
      const lang = req.header("lang");
      let user_id = req.user.id;
      let old_password = req.body.old_password;
      let new_password = req.body.new_password;
      let result = await LoginService.changePassword(new_password, old_password,  user_id,lang)
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async resetNotification(req, res, next) {
    try {
      let id = req.user.id;
      let email = req.body.is_email_notified;
      let push = req.body.is_push_notified;
      let sms = req.body.is_sms_notified;
      let result = await LoginService.resetNotification(id, email, push, sms)
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async verifyEmail(req, res, next) {
    try {
      let token = req.body.token;
      let result = await LoginService.verifyEmail(token);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async verifyPhone(req, res, next) {
    try {
      let code = req.body.code;
      let id = req.user.id;
      let result = await LoginService.verifyPhone(code, id);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async resendEmail(req, res, next) {

    try {
      let user_id = req.user.id;
      let user = await LoginService.getUserProfile(user_id)
      let email = user.email;
      let result = await LoginService.resendEmail(email);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async resendPhone(req, res, next) {
    try {
      let user_id = req.user.id;
      let user = await LoginService.getUserProfile(user_id)
      let phone = user[0].phone_number;
      let country_code = user[0].country_code;
      let result = await LoginService.resendPhone(phone, country_code);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  

  static async logout(req, res, next) {
    try {
      let device_id = req.body.device_id;
      await LoginService.logout(req.user.id, device_id);
      return res.status(200).json({ message: "You are logged out!" })
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async updateUserProfile(req, res, next) {
    try {
      const lang = req.header("lang");
      let logged_id = req.user.id
      let first_name = req.body.first_name
      let last_name = req.body.last_name
      let dob = req.body.dob
      let email = req.body.email
      let gender = req.body.gender
      let nationality = req.body.nationality
      let occupation = req.body.occupation
      let language = req.body.language
      let country = req.body.country
      let profile_image = req.body.profile_image
      let description = req.body.description
      let paypal_username = req.body.paypal_username
      let vectera_id = req.body.vectera_id
      const result = await LoginService.updateUserProfile(logged_id, first_name, last_name,dob,email,gender,nationality,occupation,language,country, profile_image, description,paypal_username,vectera_id,lang);
      if (result) {
        return res.status(200).json(result);
      }
    }
    catch (err) {
      if (err.code == "ER_TRUNCATED_WRONG_VALUE") {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
        return res.status(400).json({ 'error': "bad_request", 'error_description': err.sqlMessage });
      } else {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
        return res.status(400).json({ 'error': "bad_request", 'error_description': err });
      }
    }
  }
  /*=======Academic module========*/
  static async addAcademic(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id;
      let school = req.body.school;
      let level = req.body.level;
      let country = req.body.country
      let year = req.body.year
      let image = req.body.image
      let result = await LoginService.addAcademic(user_id,school,level,country,year,image,lang);
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  
  static async editAcademic(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id;
      let academic_id = req.body.academic_id;
      let school = req.body.school;
      let level = req.body.level;
      let country = req.body.country
      let year = req.body.year
      let image = req.body.image
      let result = await LoginService.editAcademic(user_id,academic_id,school,level,country,year,image,lang);
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getAcademicDetail(req, res, next){
    try {
      let academic_id = req.query.academic_id;
      let result = await LoginService.getAcademicDetail(academic_id);
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteAcademic(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id;
      let id = req.body.id;
      let result = await LoginService.deleteAcademic(user_id,id,lang);
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  /*===========Certificate module========*/
  static async addCertificate(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id;
      let certificate_name = req.body.certificate_name;
      let result = req.body.result;
      let year = req.body.year
      let description = req.body.description
      let image = req.body.image
      let response = await LoginService.addCertificate(user_id,certificate_name,result,year,description,image,lang);
      return res.status(200).json(response);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editCertificate(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id;
      let certificate_id = req.body.certificate_id;
      let certificate_name = req.body.certificate_name;
      let result = req.body.result;
      let year = req.body.year
      let description = req.body.description
      let image = req.body.image
      let response = await LoginService.editCertificate(user_id,certificate_id,certificate_name,result,year,description,image,lang);
      return res.status(200).json(response);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteCertificates(req, res, next){
    try {
      const lang = req.header("lang");
      let user_id = req.user.id;
      let id = req.body.id;
      let response = await LoginService.deleteCertificates(user_id,id,lang);
      return res.status(200).json(response);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getCertificateDetail(req, res, next  ){
    try {
      let certificate_id = req.query.certificate_id;
      let result = await LoginService.getCertificateDetail(certificate_id);
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  

  static async deactivateAccount(req, res, next) {
    try {
      const lang = req.header("lang");
      let deactivate_reason = req.body.deactivate_reason;
      let password = req.body.password;
      let user_id = req.user.id
      let user_pass = req.user.password
      let result = await LoginService.deactivateAccount(deactivate_reason, password, user_pass, user_id,lang);
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async connectAccountWebhook(req, res, next){
    try {
      let data = req.body.data
      console.log("test")
      let result  = await LoginService.connectAccountWebhook(data);
      return res.status(200).json(result)
    } 
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
}

module.exports = LoginController;
































































































