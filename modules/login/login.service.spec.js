const request = require('supertest');
//let router = require("./login.routes")
let app=require('../../app')
//const request = supertest(router)

// describe("POST/login", () => {

//     test("login only correct users", async () => {
//       const response = await request(router).post(`/login`)
//       console.log(response);
//       expect(response.body.length).toBe(1);
//       expect(response.statusCode).toBe(200);
//       expect(response.body).toHaveProperty('post')
//    //   expect(response.body.message).toBe("Unauthorized");
//     });
//   });


describe("GET/users", () => {

    test("tests the base route and returns true for status", () => {
        const response = request(app).get('/users');
        // expect(response.body.length).toBe(6);
        // expect(response.body[0]).toHaveProperty("id");
        // expect(response.body[0]).toHaveProperty("name");
        // expect(response).toBeDefined();
        expect(response.statusCode).toEqual(200);
        expect(response.body.message).toBe('test case working')
    });
})


// describe("Test the root path", () => {
//     test("It should response the GET method", done => {
//       request(app)
//         .get("/users")
//         .then(response => {
//           expect(response.statusCode).toBe(200);
//           done();
//         });
//     });
//   });