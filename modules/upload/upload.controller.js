require('dotenv').config();
const multer = require('multer');
const sharp = require('sharp');
//const path = require('path');


const logger = require('../../config/logger')
const UploadService = require('./upload.service')


class UploadController {

    static async getFromAws(req, res ) {
        try {
           
            let filename = req.query.filename;
            let folder = req.query.type;
            let data = await UploadService.getFromAws(filename, folder)
            console.log('data', data);
            res.writeHead(200, { 'Content-Type': 'image/jpeg' });
            res.write(data.Body, 'binary');
            res.end(null, 'binary');
        }
        catch (err) {
            console.log(err);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });

        }
    }

    static async uploadImageToAws(req, res ) {
       
        try {
            var file = req.file
            console.log("kkllk",file)
            const result = await UploadService.uploadImageToAws(file);
            console.log("rwewq",result)
            return res.status(200).json(result);
          
        }
        catch (err) {
           console.log(err)
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Upload Local
    static async uploadImageToLocal(req, res, next) {
        var file = req.file
       
           try {
                let result = await UploadService.uploadImageToLocal(res, file)
                return res.status(200).json(
                    result
                );
            } catch (err) {
                console.log(err);
                return res.status(400).json({ 'error': "bad_request", 'error_description': err });
            }
       

    }

    static async getFromLocal(req, res, next) {
        let folder = req.query.folder;
        let filename = req.query.filename
        try {
            return await UploadService.getFromLocal(res, filename, folder);
        } catch (err) {
            console.log(err);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });

        }
    }

}
module.exports = UploadController;
