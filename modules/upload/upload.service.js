const sharp = require('sharp');
require('dotenv').config();
const path = require('path');
const PIXELS_SMALL = 100;
const PIXELS_MEDIUM = 250;
const fs = require('fs');
const FOLDER_ORIG = "orig";
const FOLDER_SMALL = "small";
const FOLDER_MEDIUM = "medium";
const util = require('util');
const getStream = require('into-stream');

class UploadService {

  //Create random filename
  static async createRandomFilename(file) {
    let name = file.originalname.split(' ').join('-');
    return Date.now() + '-' + name;
  }

  // Sharp image
  static async sharpFile(file, pixels = null) {
    if (pixels) {
      return await sharp(file.buffer).resize(pixels).rotate().toBuffer();
    } else {
      return await sharp(file.buffer).rotate().toBuffer();
      //return await sharp(file.buffer).toBuffer();
    }
  }


   //configuration
   static async awsConfig() {
    const AWS = require('aws-sdk');
    AWS.config.update({
      accessKeyId: process.env.DO_ACCESS_KEY,
      secretAccessKey: process.env.DO_SECRET_ACCESS_KEY,
      region: process.env.DO_REGION
    });

    const s3 = new AWS.S3();

    return s3;
  }

  static async s3PutObject(data, filename_with_path,) {
    // Digital Ocean and AWS Cloud S3 upload
    let s3 = await this.awsConfig();
    await s3.putObject({
      Body: data,
      Bucket: process.env.BUCKET_NAME,
      Key: filename_with_path,
      ACL: 'public-read'
    }).promise();
  }



  //Upload image on AWS
  static async uploadImageToAws(file) {
    if (!file) 
      throw 'Image should not be empty.' 
    
    let filename = await this.createRandomFilename(file);
    const data = await this.sharpFile(file);
    await this.s3PutObject(data, "Uploads/Images" + '/original/' + filename)
    const data1 = await this.sharpFile(file, PIXELS_SMALL);
    await this.s3PutObject(data1, "Uploads/Images/small" + '/' + filename)
    const data2 = await this.sharpFile(file, PIXELS_MEDIUM);
    await this.s3PutObject(data2, "Uploads/Images/medium" + '/' + filename)
    await this.saveFileInDb(filename, 5, 2, 1)
    return ({ message: "Successfully uploaded image", filename: filename })
  }

  static async getFromAws(file, folder) {
    console.log(file)  
    let s3 = await this.awsConfig();



   let folders= typeof folder == "undefined"?  "original" : folder;
   console.log(folders)
      const params = {
          Bucket: process.env.BUCKET_NAME,
          Key:"Uploads/Images/"+ folders + '/' + file
      };
      
      return await s3.getObject(params).promise();
    }
  static async saveFileInDb(filename, storage_type, environment, is_default_asset) {
    var sqlQuery = 'insert into files(id, storage_type, environment, is_default_asset) values (?,?,?,?)';
    let params = [filename, storage_type, environment, is_default_asset]
    return await connection.query(sqlQuery, params)

  }

  //Upload File To Local
  static async uploadImageToLocal(res,file) {
    if (!file) {
      return res.status(400).json({ error: 'bad_request', message: 'Image should not be empty.' });
    }
    const filename = await this.createRandomFilename(file);
    console.log(filename);
    // if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
        
    //upload origional image
    await sharp(file.buffer).rotate().toFile(path.join(__dirname, '../../public/uploads/' + FOLDER_ORIG + '/' + filename))

    // console.log("Path:: ", path.join(__dirname, '../../public/uploads/' + FOLDER_ORIG + '/' + filename))
    // //upload small image
    // await sharp(file.buffer).resize(PIXELS_SMALL).rotate().toFile(path.join(__dirname, '../../public/uploads/' + FOLDER_SMALL + '/' + filename))

    // //upload medium image
    // await sharp(file.buffer).resize(PIXELS_MEDIUM).rotate().toFile(path.join(__dirname, '../../public/uploads/' + FOLDER_MEDIUM + '/' + filename))
    // await this.saveFileInDb(filename, 5, 4, 1)
    return ({
      filename: filename, message: 'File uploaded successfully'
    });
    
  }


  static async getFromLocal(res, filename, folder) {
  
    folder = folder || "orig";
    console.log(folder);
    return res.sendFile(path.join(__dirname, `../../public/uploads/` + `${folder}` + `/${filename}`));
  }
}
module.exports = UploadService;