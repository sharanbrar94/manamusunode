var express = require('express');
var router = express.Router();
const multer = require('multer');
const UploadController = require('./upload.controller')
const upload = multer({
  fileFilter(req, file, cb) {
     console.log(file)  
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
    }
  }
});

//upload image

router.post('/upload/aws', upload.single('file'), UploadController.uploadImageToAws)
router.get('/aws/file',  UploadController.getFromAws)

router.post('/upload/local', upload.single('file'), UploadController.uploadImageToLocal)
router.get('/local/file', UploadController.getFromLocal)


module.exports = router;
