const express = require("express");
const router = express.Router();
const { checkSchema } = require("express-validator");
const NotificationController = require("./notification.controller");
//const NotificationSchema = require("./notification.validation.json");
const auth = require("../../middleware/auth");
const valid = require("../../config/validator");

router.post("/notification", NotificationController.sendNotification);
router.get(
  "/notification",
  auth.checkToken,
  NotificationController.getNotification
);

module.exports = router;
