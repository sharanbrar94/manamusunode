module.exports = class NotificationService {
  static async saveNotification(data) {
    const query = `INSERT INTO notifications SET ?`;
    return await connection.query(query, [data]);
  }

  static async addNotificationUsers(notification_tos) {
    const query = `INSERT INTO notification_tos(notification_id, to_user_id) VALUES ?`;
    return await connection.query(query, [notification_tos]);
  }

  static async getNotification(user_id,page,limit) {
    let pagination = await this.getPageCount(page, limit)
    let count= await connection.query(`select * from notification_tos where to_user_id=${user_id}`)
    const query = `SELECT title,type,from_user_id,( CASE WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.created_at) != 0 THEN  IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.created_at)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.created_at)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.created_at)) ,' year ago'))  WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.created_at) != 0 THEN  IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.created_at)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.created_at)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.created_at)) ,' month ago'))  WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at)) != 0 THEN  IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at))) ,' hours ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at))) ,' hour ago')),  IF(DATEDIFF(UTC_TIMESTAMP, notifications.created_at) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, notifications.created_at) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, notifications.created_at) ,' day ago'))) WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at)) ,' min ago') ELSE CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at)) ,'s ago') END ) as time_since, message,is_read, created_at from notifications join notification_tos on notifications.id = notification_tos.notification_id WHERE to_user_id = ${user_id} order by created_at desc limit ? offset ?`;
    console.log(query);
    let result = await connection.query(query,[pagination.limit, pagination.offset]);
    await connection.query(`update notification_tos set is_read=1 where to_user_id=${user_id}`)
    
    return {count:count.length,notifications:result};
  }

  static async getUnreadCount(user_id) {
    const query = `SELECT count(is_read) as unread_count from notification_tos WHERE to_user_id = ${user_id} and is_read=0 `;
    return await connection.query(query)
  }

  static async getTime(user_id) {

    let query = `select notifications.id, ( CASE WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.created_at) != 0 THEN  IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.created_at)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.created_at)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.created_at)) ,' year ago'))  WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.created_at) != 0 THEN  IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.created_at)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.created_at)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.created_at)) ,' month ago'))  WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at)) != 0 THEN  IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at))) ,' hours ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at))) ,' hour ago')),  IF(DATEDIFF(UTC_TIMESTAMP, notifications.created_at) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, notifications.created_at) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, notifications.created_at) ,' day ago'))) WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at)) ,' min ago') ELSE CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, notifications.created_at)) ,'s ago') END  ) as  time_since from notifications left join notification_tos on notification_tos.notification_id=notifications.id where notification_tos.to_user_id=${user_id}`
    return await connection.query(query)
  }

  static async getPageCount(page, limit) {
    var offset = 0
    let limit_count = parseInt((limit != undefined && limit != isNaN) ? limit : 20)
    let page_count = parseInt((page != undefined && page != isNaN) ? page : 1)
    if (page_count && page_count > 0 && limit_count) {
      offset = (page_count - 1) * limit_count;
    }
    return ({ limit: limit_count, offset: offset })
  }
};

