const NotificationService = require("../notification/notification.service");
const logger = require("../../config/logger");
const _ = require("lodash");

module.exports = class NotificationController {
  static async sendNotification(req, res) {
    try {
      const { title, message, users } = req.body;
      const inserterdData = await NotificationService.saveNotification({
        title,
        message,
      });

      const result = _.map(users, (ele) => {
        return [inserterdData.insertId, ele.user_id];
      });

      await NotificationService.addNotificationUsers(result);

      const body = {
        title,
        message,
      };

      //fcm notification
      require("../pushnotification/push").broadcast(body);

      res.status(200).json({ message: "notfication saved." });
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong." + err.message,
      });
    }
  }

  static async getNotification(req, res) {
    try {
      const{page,limit}=req.query
    let id=req.user.id; //test
    console.log(id);
      const result = await NotificationService.getNotification(id,page,limit);

      res.status(200).json(result);
    } catch (err) {
      logger.error(`message -  ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong.",
      });
    }
  }
  // "parameters": [
  //   {
  //     "name": "user_id",
  //     "in": "query",
  //     "description": "user_id",
  //     "required": true,
  //     "format": "int64"
  //   }
  // ],
};
