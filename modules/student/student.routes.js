const express = require('express');
const router = express.Router();
const { checkSchema } = require('express-validator');
const valid = require('../../config/validator')
let auth = require("../../middleware/auth")
let studentSchema = require('./student.validation.json');


const StudentController = require('./student.controller');

//request module
router.post('/student/request', auth.checkToken,StudentController.createRequest);
router.put('/student/request', auth.checkToken,StudentController.updateRequest);
router.get('/student/request', auth.checkToken,StudentController.getRequest);
router.get('/student/request-detail',auth.checkOptionalToken, StudentController.getRequestDetail);
router.post('/student/delete-request', auth.checkToken,StudentController.deleteRequest);
router.post('/student/decline-response', auth.checkToken,StudentController.declineResponse);
router.post('/student/accept-response', auth.checkToken,StudentController.acceptResponse);

//teacher post module
router.get('/student/subjects', StudentController.getSubjects);
router.get('/student/filter', StudentController.getTeacherPost);
router.get('/student/all-requests', StudentController.getAllRequests);

//booking module
router.post('/student/booking', auth.checkStudentToken,checkSchema(studentSchema.addBooking),StudentController.addBooking);
router.get('/student/booking', auth.checkStudentToken,StudentController.getBookingList);
router.post('/student/add-time-slots', auth.checkStudentToken,StudentController.addTimeSlot);
router.post('/student/cancel-time-slots', auth.checkStudentToken,StudentController.cancelTimeSlot);
router.get('/student/booking-detail', auth.checkStudentToken,StudentController.bookingDetail);
router.post('/student/edit-make-offer', auth.checkStudentToken,StudentController.editMakeOffer);
router.post('/student/delete-booking', auth.checkStudentToken,StudentController.deleteBooking);

//payment module
router.post('/student/make-payment-amount', auth.checkStudentToken,StudentController.makePaymentAmount);
router.post('/student/make-payment', auth.checkStudentToken,StudentController.makePayment);
router.get('/student/payments', auth.checkStudentToken,StudentController.getPayments);
router.get('/student/payment-detail', auth.checkStudentToken,StudentController.getPaymentDetail);

//calendar module
router.get('/student/calendar', auth.checkStudentToken,StudentController.studentCalendar);

//complain module
router.post('/student/time-slot-complain', auth.checkStudentToken,StudentController.addTimeSlotComplain);
router.post('/student/booking-complain', auth.checkStudentToken,StudentController.addBookingComplain);

//rating module
router.post('/student/rating', auth.checkStudentToken,StudentController.addRating);
router.put('/student/rating', auth.checkStudentToken,StudentController.editRating);


/*======currency module=======*/
router.get('/student/currency-list',StudentController.getCurrencyList);
router.post('/student/complete-course',auth.checkStudentToken,StudentController.completeBooking);











module.exports = router;