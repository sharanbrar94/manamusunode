const e = require('express');
const logger = require('../../config/logger')
const StudentService = require('./student.service');
const axios = require('axios');
const { ConversationList } = require('twilio/lib/rest/conversations/v1/conversation');
class StudentController {

  /*======Request module======*/
  static async createRequest(req, res, next) {
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let title = req.body.title
      let subject = req.body.subject
      let teaching_standards = req.body.teaching_standards
      let language = req.body.language
      let hourly_rate = req.body.hourly_rate
      let currency = req.body.currency
      let description = req.body.description
      let time_slot = req.body.time_slot
      let user = await StudentService.createRequest(user_id, title, subject, teaching_standards, language, hourly_rate, currency, description, time_slot,lang);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async updateRequest(req, res, next) {
    try {
      const lang = req.header("lang");
      let user_id = req.user.id
      let request_id = req.body.request_id
      let title = req.body.title
      let subject = req.body.subject
      let teaching_standards = req.body.teaching_standards
      let language = req.body.language
      let hourly_rate = req.body.hourly_rate
      let currency = req.body.currency
      let description = req.body.description
      let time_slot = req.body.time_slot
      let user = await StudentService.updateRequest(user_id, request_id, title, subject, teaching_standards, language, hourly_rate, currency, description, time_slot,lang);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getRequest(req, res, next) {
    try {
      let limit = req.query.limit;
      let page = req.query.page;
      let user_id = req.user.id
      let currency = req.query.currency
      let request = await StudentService.getRequest(limit, page, user_id, currency);
      return res.status(200).json(request)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getRequestDetail(req, res, next) {
    try {
      const lang = req.header("lang");
      let request_id = req.query.request_id
      let currency = req.query.currency
      let request = {}
      request.requestDetail = await StudentService.getRequestDetail(request_id, currency,lang)
      request.language = await StudentService.getRequestLanguage(request_id)
      request.timeSlot = await StudentService.getRequestTimeSlot(request_id)
      request.responses = await StudentService.getResponses(request_id)
      return res.status(200).json(request)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async deleteRequest(req, res, next) {
    try {
      let lang = req.header("lang");
      let request_id = req.body.request_id
      var deleteRequest = await StudentService.deleteRequest(request_id,lang)
      return res.status(200).json(deleteRequest)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async declineResponse(req, res, next) {
    try {
      const lang = req.header("lang");
      let response_id = req.body.response_id
      let request_id = req.body.request_id
      var response = await StudentService.declineResponse(response_id,request_id,lang)
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async acceptResponse(req, res, next) {
    try {
      let lang = req.header("lang");
      let response_id = req.body.response_id
      let request_id = req.body.request_id
      let user_id = req.user.id
      console.log("user_id",user_id)
      console.log("user",req.user)
      var response = await StudentService.acceptResponse(response_id, user_id,request_id,lang)
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  /*========Teacher post filter module=========*/
  static async getSubjects(req, res, next) {
    try {
      let subjects = await StudentService.getSubjects();
      return res.status(200).json(subjects)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getTeacherPost(req, res, next) {
    try {
      let limit = req.query.limit;
      let page = req.query.page;
      let gender = req.query.gender
      let nationality = req.query.nationality
      let subject = req.query.subject
      let teaching_standard = req.query.teaching_standard
      let language = req.query.language
      let keyword = req.query.keyword
      let search_currency = req.query.search_currency
      let currency = req.query.currency
      var post = await StudentService.getTeacherPost(limit, page, gender, nationality, subject, teaching_standard, language, keyword, currency, search_currency)
      return res.status(200).json(post)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getAllRequests(req, res, next) {
    try {
      let limit = req.query.limit;
      let page = req.query.page;
      let gender = req.query.gender
      let nationality = req.query.nationality
      let subject = req.query.subject
      let teaching_standard = req.query.teaching_standard
      let language = req.query.language
      let keyword = req.query.keyword
      let currency = req.query.currency
      let search_currency = req.query.search_currency
      var requests = await StudentService.getAllRequests(limit, page, gender, nationality, subject, teaching_standard, language, keyword, currency, search_currency)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  /*=======Booking Module======*/
  static async addBooking(req, res, next) {
    try {
      const lang = req.header("lang");
      let post_id = req.body.post_id;
      let user_id = req.user.id;
      let time_slot = req.body.time_slot
      let hourly_rate = req.body.hourly_rate
      let currency = req.body.currency
      var requests = await StudentService.addBooking(post_id, user_id, time_slot, hourly_rate, currency,lang)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getBookingList(req, res, next) {
    try {
      let user_id = req.user.id;
      let status = req.query.status
      let page = req.query.page
      let limit = req.query.limit
      var requests = await StudentService.getBookingList(user_id, status, page, limit)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async addTimeSlot(req, res, next) {
    try {
      const lang = req.header("lang");
      let booking_id = req.body.booking_id
      let time_slot = req.body.time_slot
      var requests = await StudentService.addTimeSlot(booking_id, time_slot,lang)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async cancelTimeSlot(req, res, next) {
    try {
      const lang = req.header("lang");
      let booking_id = req.body.booking_id
      let time_slot_id = req.body.time_slot_id
      let user_id = req.user.id
      var requests = await StudentService.cancelTimeSlot(user_id,booking_id, time_slot_id,lang)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async bookingDetail(req, res, next) {
    try {
      const lang = req.header("lang");
      let booking_id = req.query.booking_id
      let post_id = req.query.post_id
      let user_id = req.user.id
      var requests = await StudentService.bookingDetail(booking_id, post_id, user_id,lang)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async editMakeOffer(req, res, next) {
    try {
      const lang = req.header("lang");
      let booking_id = req.body.booking_id
      let hourly_rate = req.body.hourly_rate
      var requests = await StudentService.editMakeOffer(booking_id, hourly_rate,lang)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async deleteBooking(req, res, next){
    try {
      const lang = req.header("lang");
      let booking_id = req.body.booking_id
      var requests = await StudentService.deleteBooking(booking_id,lang)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  //payment module
  static async makePaymentAmount(req, res, next) {
    try {
      let booking_id = req.body.booking_id
      var requests = await StudentService.makePaymentAmount(booking_id)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async makePayment(req, res, next) {
    try {
      const lang = req.header("lang");
      let token = req.body.token
      let booking_id = req.body.booking_id
      let user_id = req.user.id
      let amount = req.body.amount
      let chat_id = req.body.chat_id
      var requests = await StudentService.makePayment(token, booking_id, user_id, amount, chat_id,lang)
      return res.status(200).json(requests)
    }
    catch (err) {
      if(err.raw){
        return res.status(400).json({ error: "bad_request", error_description: err.raw.message?err.raw.message :err.raw.code });
      }
      else{
        return res.status(400).json({ error: "bad_request", error_description: err });
      }
      
    }
  }
  static async getPayments(req, res, next){
    try {
      let user_id = req.user.id
      let page = req.query.page
      let limit = req.query.limit
      var requests = await StudentService.getPayments(user_id,page,limit)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async getPaymentDetail(req, res, next){
    try {
      let payment_id = req.query.payment_id
      var requests = await StudentService.getPaymentDetail(payment_id)
      return res.status(200).json(requests)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }

  /*=======Calendar management======*/
  static async studentCalendar(req, res, next) {
    try {
      let month = req.query.month
      let year = req.query.year
      let user_id = req.user.id
      var result = await StudentService.studentCalendar(month, year, user_id)
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }

  /*=======Complain module======*/
  static async addTimeSlotComplain(req, res, next) {
    try {
      const lang = req.header("lang");
      let time_slot_id = req.body.time_slot_id
      let subject_id = req.body.subject_id
      let description = req.body.description
      var result = await StudentService.addTimeSlotComplain(time_slot_id, subject_id, description,lang)
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async addBookingComplain(req, res, next) {
    try {
      const lang = req.header("lang");
      let booking_id = req.body.booking_id
      let subject_id = req.body.subject_id
      let description = req.body.description
      let user_id = req.user.id
      var result = await StudentService.addBookingComplain(booking_id, subject_id, description,lang,user_id)
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  
  /*=======rating module======*/
  static async addRating(req, res, next){
    try {
      const lang = req.header("lang");
      let booking_id = req.body.booking_id
      let from_user_id = req.user.id
      let communicate_ability = req.body.communicate_ability
      let class_preparation = req.body.class_preparation
      let teacher_knowledge = req.body.teacher_knowledge
      let tutor_easy = req.body.tutor_easy
      let class_quality = req.body.class_quality
      var result = await StudentService.addRating(booking_id, from_user_id, communicate_ability,class_preparation,teacher_knowledge,tutor_easy,class_quality,lang)
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  static async editRating(req, res, next){
    try {
      const lang = req.header("lang");
      let rating_id = req.body.rating_id
      let communicate_ability = req.body.communicate_ability
      let class_preparation = req.body.class_preparation
      let teacher_knowledge = req.body.teacher_knowledge
      let tutor_easy = req.body.tutor_easy
      let class_quality = req.body.class_quality
      var result = await StudentService.editRating(rating_id,communicate_ability,class_preparation,teacher_knowledge,tutor_easy,class_quality,lang)
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }
  
  static async getCurrencyList(req, res, next) {
    try {
      //convert usd to jpy
      // let jpyResponse = await axios.get('https://currencyapi.net/api/v1/rates?key=hxUtKZiRWxZJCoCrfEu33iAKa05dOrfSMZ4o')
      // console.log(response)
      // let fromUsd = 'USD'
      // let toJpy = 'JPY'
      // let jpyRate = jpyResponse.data.rates.JPY

      //convert usd to sgd
      let jpyResponse = await axios.get('http://api.currencylayer.com/live?access_key=3e4a81c7dd472f2cdacfc883c09dfe97')
      console.log(jpyResponse)
      let fromUsd = 'USD'
      let toJpy = 'SGD'
      let jpyRate = jpyResponse.data.quotes.USDJPY

      // await StudentService.updateCurrency(fromUsd, toJpy, jpyRate)
      // return res.status(200).json({ message: "currency upated" })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }

  static async completeBooking(req, res, next){
    try {
      const lang = req.header("lang");
      let booking_id = req.body.booking_id
      var result = await StudentService.completeBooking(booking_id,lang)
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ error: "bad_request", error_description: err });
    }
  }

  


}
module.exports = StudentController;