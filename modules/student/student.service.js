const { sign } = require('jsonwebtoken');
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const Common = require('../../middleware/common')
const logger = require('../../config/logger');
const { common } = require('azure-storage');
const moment  = require('moment');
const stripe = require('stripe')('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');
//const stripe = require('stripe')('sk_test_51JAVmXG8oPs9xsvBJhSw3b1t8rIj5ytAmWnXYVPTBHRPa0XAgAUogARsw4Z0R1rhGWWIavikDiMMPgtRfxY9ZESP008bXrCUSx');

class StudentService {
    /*=======request module======*/
    static async createRequest(user_id,title,subject,teaching_standards,language,hourly_rate,currency,description,time_slot,lang) {
        let getLangMsg = await Common.localization(lang)
        var getUserData = await Common.userData(user_id)
        if(getUserData.is_user_verified == 1){
            await Common.checkValidAmount(currency,hourly_rate,lang)
            let sqlQuery = `insert into requests(user_id,title,subject,teaching_standard,hourly_rate,currency,description) values(?,?,?,?,?,?,?)`
            let params = [user_id,title, subject, teaching_standards, hourly_rate,currency, description]
            let request = await connection.query(sqlQuery, params)
            if(language){
                for (var i in language) {
                    await connection.query('insert into request_languages (request_id,language_id) values (?,?)', [request.insertId, language[i]])
                }
            }
            if(time_slot){
                for (var j in time_slot) {
                    var requestDays = await connection.query('insert into request_days (request_id,date) values (?,?)', [request.insertId, time_slot[j].date])
                    for (var k in time_slot[j].time) {
                        await connection.query('insert into request_day_slots (request_id,request_day_id,start_time,end_time) values (?,?,?,?)', [request.insertId,requestDays.insertId,time_slot[j].time[k].start_time,time_slot[j].time[k].end_time])
                    }
                }
            }
            let query = `select requests.*,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id  where requests.id=?`
            var getrequest = await connection.query(query, [request.insertId])

            
            return ({ message:getLangMsg.request_created ,requestDetail:getrequest[0] })
        }
        else{
            throw(getLangMsg.verification_pending)
        }
    }
    static async updateRequest(user_id,request_id,title,subject,teaching_standard,language,hourly_rate,currency,description,time_slot,lang){
        let responsesQuery = `select * from teacher_responses where request_id=? and status != 2`
        let getResponses = await connection.query(responsesQuery, [request_id])
        if(getResponses.length > 0 ){
            throw("No access to edit")
        }
        else{
            await Common.checkValidAmount(currency,hourly_rate,lang)
            let sqlQuery = 'update requests set user_id=?,title=?,subject=?,teaching_standard=?,hourly_rate=?,currency=?,description=? where id=?'
            let params = [user_id,title,subject,teaching_standard,hourly_rate,currency,description,request_id]
            await connection.query(sqlQuery, params)
            if(language){
                await connection.query('delete from request_languages where request_id = ?', [request_id])
                for (var i in language) {
                    await connection.query('insert into request_languages (request_id,language_id) values (?,?)', [request_id, language[i]])
                }
            }
            if(time_slot){
                await connection.query('delete from request_days where request_id = ?', [request_id])
                await connection.query('delete from request_day_slots where request_id = ?', [request_id])
                for (var j in time_slot) {
                    var requestDays = await connection.query('insert into request_days (request_id,date) values (?,?)', [request_id, time_slot[j].date])
                    for (var k in time_slot[j].time) {
                        await connection.query('insert into request_day_slots (request_id,request_day_id,start_time,end_time) values (?,?,?,?)', [request_id,requestDays.insertId,time_slot[j].time[k].start_time,time_slot[j].time[k].end_time])
                    }
                }
            }
            let getLangMsg = await Common.localization(lang)
            return { message: getLangMsg.request_updated }
        }
        
        
        
    }
    static async getRequest(limit,page,user_id,currency){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select requests.*,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id where requests.user_id=? and requests.deleted_at is null`
        sqlQuery += ` order by requests.id DESC LIMIT ${limit} OFFSET ${offset}`
        let requests = await connection.query(sqlQuery, [user_id])
        
        let countSqlQuery = `select requests.*,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id where requests.user_id=? and requests.deleted_at is null`
        let count = await connection.query(countSqlQuery, [user_id])
        return {count:count.length,requests:requests}
    }
    static async getRequestDetail(request_id,currency,user_id,lang){
        let sqlQuery = `select requests.*,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id  where requests.id=?`
        var post = await connection.query(sqlQuery, [request_id])
        post[0].original_currency = post[0].currency
        post[0].original_hourly_rate = post[0].hourly_rate
        if(currency != post[0].currency){
            var from = post[0].currency
            let to = currency
            let amount = post[0].hourly_rate
            post[0].hourly_rate = await Common.getCurrencyRate(from,to,amount)
            post[0].currency = currency
        }
        post[0].created_at = await Common.timeSince(post[0].created_at,lang)
        return post[0]
    }
    static async getRequestLanguage(request_id){
        let sqlQuery = `select l.name,l.id from request_languages left join languages as l on request_languages.language_id=l.id where request_languages.request_id=?`
        return await connection.query(sqlQuery, [request_id])
    }
    static async getRequestTimeSlot(request_id){
        let sqlQuery = `select * from request_days where request_id=?`
        var request_days =  await connection.query(sqlQuery, [request_id])
        var arr = []
        await Promise.all(request_days.map(async (x) => {
            var request = {}
            let optionQuery = `select * from request_day_slots where request_day_id = ${x.id}`
            const options = await connection.query(optionQuery)
            
            request.date = x.date;
            request.time_slot = options
            arr.push(request)
        }));
        return arr
    }
    static async getResponses(request_id){
        let sqlQuery = `select teacher_responses.*,IFNULL((select Avg(rating) from booking_reviews where booking_reviews.to_user_id = teacher_responses.user_id),0) as teacher_rating,u.description as teacher_description,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from teacher_responses left join users as u on teacher_responses.user_id=u.id where teacher_responses.request_id=?`
        let responses =  await connection.query(sqlQuery, [request_id])
        var arr = []
        await Promise.all(responses.map(async (x) => {
            var booking = {}
            let optionQuery = `select r.date,d.start_time,d.end_time,d.id from teacher_accepted_time_slots left join request_day_slots as d on teacher_accepted_time_slots.request_time_slot_id = d.id left join request_days as r on d.request_day_id = r.id where teacher_accepted_time_slots.teacher_id = ${x.user_id} and teacher_accepted_time_slots.request_id = ${request_id}`
            const options = await connection.query(optionQuery)
            
            responses = x;
            responses.accepted_time_slots = options
            arr.push(responses)
        }));
        return arr
    }
    static async deleteRequest(request_id,lang){
        var current_date = new Date()
        let getLangMsg = await Common.localization(lang)
        let responsesQuery = `select * from teacher_responses where request_id=? and status != 2`
        let getResponses = await connection.query(responsesQuery, [request_id])
        if(getResponses.length > 0 ){
            throw("No access to delete")
        }
        else{
            let sqlQuery = `update requests set deleted_at=? where id=?`
            await connection.query(sqlQuery, [current_date,request_id])
            return { message: getLangMsg.request_deleted }
        }
    }
    static async declineResponse(response_id,request_id,lang){
        let sqlQuery = 'update teacher_responses set status=? where id=?'
        let params = [2,response_id]
        await connection.query(sqlQuery, params)

         //get response detail
         let resQuery = `select * from teacher_responses where id=?`
         var getResponse = await connection.query(resQuery, [response_id])

        //send email//
        var emailData = await Common.getRequestDetail(request_id)
        let getUrl = process.env.WEBSITE_URL
        let teacherEmail = await Common.userData(getResponse[0].user_id)
        await Common.declineResponseByStudent(teacherEmail,emailData,getUrl)

        let getLangMsg = await Common.localization(lang)
        return ({ message: getLangMsg.response_declined,responses:await this.getResponses(request_id)})
    }
    static async acceptResponse(response_id,user_id,request_id,lang){
        let sqlQuery = 'update teacher_responses set status=? where id=?'
        let params = [1,response_id]
        await connection.query(sqlQuery, params)

        //get response detail
        let resQuery = `select * from teacher_responses where id=?`
        var getResponse = await connection.query(resQuery, [response_id])

        //get request detail
        let reqQuery = `select * from requests where id=?`
        var getRequest = await connection.query(reqQuery, [getResponse[0].request_id])

        //get request timeslots
        // let timeQuery = `select * from request_days where request_id=?`
        // var getTimeRequest = await connection.query(timeQuery, [getResponse[0].request_id])
        

        let timeQuery = `select r.date,d.start_time,d.end_time,d.id from teacher_accepted_time_slots left join request_day_slots as d on teacher_accepted_time_slots.request_time_slot_id = d.id left join request_days as r on d.request_day_id = r.id where teacher_accepted_time_slots.teacher_id = ${getResponse[0].user_id} and teacher_accepted_time_slots.request_id = ${getResponse[0].request_id}`
        var getTimeRequest = await connection.query(timeQuery)
        

        //get languages
        let lanQuery = `select * from  request_languages where request_id=?`
        var getLan = await connection.query(lanQuery, [getResponse[0].request_id])
        
        //create post
        let postQuery = `insert into posts(user_id,title,subject,teaching_standard,hourly_rate,description,type,currency) values(?,?,?,?,?,?,?,?)`
        let postParams = [getResponse[0].user_id,getRequest[0].title, getRequest[0].subject, getRequest[0].teaching_standard, getResponse[0].hourly_rate, getRequest[0].description,'request',getRequest[0].currency]
        let posts = await connection.query(postQuery, postParams)
        if(getLan){
            for (var i in getLan) {
                
                await connection.query('insert into post_languages (post_id,language_id) values (?,?)', [posts.insertId, getLan[i].language_id])
            }
        }
        //add booking
        let randomString = await Common.generateRandomNumber(8)
        let booking_unique_id = 'BK-'+randomString

        let bookingsqlQuery = `insert into booking_requests(post_id,user_id,hourly_rate,status,booking_unique_id,currency) values(?,?,?,?,?,?)`
        let bookparams = [posts.insertId,user_id,getResponse[0].hourly_rate,2,booking_unique_id,getRequest[0].currency]
        let booking = await connection.query(bookingsqlQuery, bookparams)
        if(getTimeRequest){
            for (var j in getTimeRequest) {
                var booking_date = getTimeRequest[j].date
                var totalHours = await this.calculateHours(getTimeRequest[j].start_time,getTimeRequest[j].end_time)

                await connection.query('insert into booking_request_slots (booking_request_id,user_id,date,start_time,end_time,hourly_rate,status,hours_difference) values (?,?,?,?,?,?,?,?)', [booking.insertId,user_id,booking_date,getTimeRequest[j].start_time,getTimeRequest[j].end_time,getResponse[0].hourly_rate,1,totalHours])
            }
        }
        await connection.query('update requests set status=? where id=?', [1,getResponse[0].request_id])
        //let respsqlQuery = 'update teacher_responses set status=? where id=?'
        
        //send email//
        let teacherEmail = await Common.userData(getResponse[0].user_id)
        let studentEmail = await Common.userData(user_id)
        let getUrl = process.env.WEBSITE_URL
        var emailData = await this.getBookingDetaill(booking.insertId)
        await Common.acceptResponseByStudent(studentEmail,teacherEmail,emailData.booking_detail,getUrl)

        let getLangMsg = await Common.localization(lang)

        return ({ message: getLangMsg.response_accepted,booking_id:booking.insertId,responses:await this.getResponses(request_id)})
    }
    static async getSubjects(){
        let sqlQuery = `select * from subjects where deleted_at is null`
        return await connection.query(sqlQuery)
    }
    static async getTeacherPost(limit,page,gender,nationality,subject,teaching_standard,language,keyword,currency,search_currency){
        console.log(search_currency)
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select posts.*,IFNULL((select Avg(rating) from booking_reviews where booking_reviews.to_user_id=posts.user_id),0) as post_rating,u.first_name,u.last_name,u.profile_image,u.verified_status from posts left join users as u on posts.user_id = u.id left join post_languages as pl on posts.id = pl.post_id where posts.deleted_at is null and posts.type = 'post' and posts.status = 1 and u.is_deactivated = 0 and u.is_blocked = 0 and u.deleted_at is null`
        if(search_currency){
            sqlQuery += ` and posts.currency like '%${search_currency}%'`
        }
        if(language){
            sqlQuery += ` and pl.language_id IN (${language})`
        }
        if(gender){
            sqlQuery += ` and u.gender = '${gender}'`
        }
        if(nationality){
            sqlQuery += ` and u.nationality = ${nationality}`
        }
        if(subject){
            sqlQuery += ` and posts.subject = ${subject}`
        }
        if(teaching_standard){
            sqlQuery += ` and posts.teaching_standard like '%${teaching_standard}%'`
        }
        if(keyword){
            sqlQuery += ` and posts.title like '%${keyword}%'`
        }
        sqlQuery += ` GROUP BY posts.id order by posts.id DESC LIMIT ${limit} OFFSET ${offset}`
        let posts = await connection.query(sqlQuery)
        posts.forEach(async x=>{
            if(currency != x.currency){
                var from = x.currency
                let to = currency
                let amount = x.hourly_rate
                x.hourly_rate = await Common.getCurrencyRate(from,to,amount)
                x.currency = currency
                
            }
        })

        let countSqlQuery = `select posts.*,(select Avg(rating) from reviews where reviews.post_id=posts.id) as post_rating,u.first_name,u.last_name,u.profile_image from posts left join users as u on posts.user_id = u.id left join post_languages as pl on posts.id = pl.post_id where posts.deleted_at is null and posts.type = 'post' and posts.status = 1 and u.is_deactivated = 0 and u.is_blocked = 0 and u.deleted_at is null`
        if(search_currency){
            countSqlQuery += ` and posts.currency like '%${search_currency}%'`
        }
        if(language){
            countSqlQuery += ` and pl.language_id IN (${language})`
        }
        if(gender){
            countSqlQuery += ` and u.gender = '${gender}'`
        }
        if(nationality){
            countSqlQuery += ` and u.nationality = ${nationality}`
        }
        if(subject){
            countSqlQuery += ` and posts.subject = ${subject}`
        }
        if(teaching_standard){
            countSqlQuery += ` and posts.teaching_standard like '%${teaching_standard}%'`
        }
        if(keyword){
            countSqlQuery += ` and posts.title like '%${keyword}%'`
        }
        countSqlQuery += ` GROUP BY posts.id order by posts.id DESC ` 
        let count = await connection.query(countSqlQuery)
        return {count:count.length,posts:posts}
    }
    static async getAllRequests(limit,page,gender,nationality,subject,teaching_standard,language,keyword,currency,search_currency){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select requests.*,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id left join request_languages as pl on requests.id = pl.request_id where requests.deleted_at is null and requests.request_status = 1 and u.is_deactivated = 0 and u.is_blocked = 0 and u.deleted_at is null`
        if(search_currency){
            sqlQuery += ` and requests.currency like '%${search_currency}%'`
        }
        if(language){
            sqlQuery += ` and pl.language_id IN (${language})`
        }
        if(gender){
            sqlQuery += ` and u.gender = '${gender}'`
        }
        if(nationality){
            sqlQuery += ` and u.nationality = ${nationality}`
        }
        if(subject){
            sqlQuery += ` and requests.subject = ${subject}`
        }
        if(teaching_standard){
            sqlQuery += ` and requests.teaching_standard like '%${teaching_standard}%'`
        }
        if(keyword){
            sqlQuery += ` and requests.title like '%${keyword}%'`
        }
        sqlQuery += ` GROUP BY requests.id order by requests.id DESC LIMIT ${limit} OFFSET ${offset}`
        let requests = await connection.query(sqlQuery)
        requests.forEach(async x=>{
            if(currency != x.currency){
                var from = x.currency
                let to = currency
                let amount = x.hourly_rate
                x.hourly_rate = await Common.getCurrencyRate(from,to,amount)
                x.currency = currency
            }
        })

        let countSqlQuery = `select requests.*,u.first_name,u.last_name,u.profile_image from requests left join users as u on requests.user_id = u.id left join request_languages as pl on requests.id = pl.request_id where requests.deleted_at is null and requests.request_status = 1 and u.is_deactivated = 0 and u.is_blocked = 0 and u.deleted_at is null`
        if(search_currency){
            countSqlQuery += ` and requests.currency like '%${search_currency}%'`
        }
        if(language){
            countSqlQuery += ` and pl.language_id IN (${language})`
        }
        if(gender){
            countSqlQuery += ` and u.gender = '${gender}'`
        }
        if(nationality){
            countSqlQuery += ` and u.nationality = ${nationality}`
        }
        if(subject){
            countSqlQuery += ` and requests.subject = ${subject}`
        }
        if(teaching_standard){
            countSqlQuery += ` and requests.teaching_standard like '%${teaching_standard}%'`
        }
        if(keyword){
            countSqlQuery += ` and requests.title like '%${keyword}%'`
        }
        countSqlQuery += ` GROUP BY requests.id order by requests.id DESC `
        let count = await connection.query(countSqlQuery)
        console.log("count",count)
        return {count:count.length,requests:requests}
    }
    /*==========Booking Module=========*/
    static async addBooking(post_id,user_id,time_slot,hourly_rate,currency,lang){
        let getLangMsg = await Common.localization(lang)
        var getUserData = await Common.userData(user_id)
        if(getUserData.is_user_verified == 1){
            let query = `select * from posts where id = ${post_id}`
            let postDetail = await connection.query(query)
            
            if(postDetail[0].deleted_at == null){
                let getBookingDetail = await connection.query(`select * from booking_requests where user_id = ${user_id} and post_id = ${post_id} and status != 3`)
                if(getBookingDetail.length > 0){
                    throw("Booking already done")
                }
                else{
                    
                    await Common.checkValidAmount(currency,hourly_rate,lang)

                    let get_hourly_rate = postDetail[0].hourly_rate
                    if(currency != postDetail[0].currency){
                        var from = postDetail[0].currency
                        let to = currency
                        let amount = postDetail[0].hourly_rate
                        let get_hourly_rates = await Common.getCurrencyRate(from,to,amount)
                        get_hourly_rate = Number((get_hourly_rates).toFixed(2));
                    }
                    if(hourly_rate){
                        get_hourly_rate = Number((hourly_rate).toFixed(2));
                    }
                    let randomString = await Common.generateRandomNumber(8)
                    let booking_unique_id = 'BK-'+randomString
                    let sqlQuery = `insert into booking_requests(post_id,user_id,hourly_rate,currency,booking_unique_id) values(?,?,?,?,?)`
                    let params = [post_id,user_id,get_hourly_rate,currency,booking_unique_id]
                    let booking = await connection.query(sqlQuery, params)
                    var total_amount  = 0
                    if(time_slot){
                        
                        for (var j in time_slot) {
                            var booking_date = time_slot[j].date
                            for (var k in time_slot[j].time) {
                                var totalHours = await this.calculateHours(time_slot[j].time[k].start_time,time_slot[j].time[k].end_time)
                                total_amount += totalHours*get_hourly_rate
                                await connection.query('insert into booking_request_slots (user_id,booking_request_id,date,start_time,end_time,hourly_rate,payment_status,hours_difference) values (?,?,?,?,?,?,?,?)', [user_id,booking.insertId,booking_date,time_slot[j].time[k].start_time,time_slot[j].time[k].end_time,get_hourly_rate,1,totalHours])
                                
                            }
                        }
                    }
                    let teacherData = await Common.userData(postDetail[0].user_id)
                    
                    //send email
                    var emailData = await this.getBookingDetaill(booking.insertId)
                    let getUrl = process.env.WEBSITE_URL
                    
                    await Common.postApplyByStudent(teacherData,emailData.booking_detail,total_amount,getUrl)

                    return ({ message: getLangMsg.booking_successfully,booking:await this.getBookingDetaill(booking.insertId)})
                }
            }
            else{
                throw("Post not exists more")
            }
        }
        else{
            throw(getLangMsg.verification_pending)
        }
    }
    static async calculateHours(start_time,end_time){
        if(end_time == '00:00'){
            end_time = '24:00'
        }
        else if(end_time == '00:30' && start_time != '00:00'){
            end_time = '24:30'
        }
        console.log("endTime",end_time)
        var startTime=moment(start_time, "HH:mm");
        var endTime=moment(end_time, "HH:mm");
        var duration = moment.duration(endTime.diff(startTime));
        var hours = parseInt(duration.asHours());
        var minutes = parseInt(duration.asMinutes())-hours*60;
        var calulateMin = minutes/60
        var totalHours = hours+calulateMin
        return totalHours
    }
    static async getBookingList(user_id,status,page,limit){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let query = `select booking_requests.*,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id where booking_requests.user_id = ${user_id}`
        if(status){
            if(status == 1){
                query += ` and booking_requests.status = 1`
            }
            if(status == 2){
                query += ` and booking_requests.status = 2`
            }
            else if(status == 3){
                query += ` and booking_requests.status = 3`
            }
            else if(status == 4){
                query += ` and booking_requests.status = 4`
            }
            else if(status == 5){
                query += ` and booking_requests.status = 5`
            }
        }
        query += ` order by booking_requests.updated_at DESC LIMIT ${limit} OFFSET ${offset}`
        let bookings = await connection.query(query)
        var arr = []
        await Promise.all(bookings.map(async (x) => {
            var booking = {}
            let optionQuery = `select * from booking_request_slots where booking_request_id = ${x.id}`
            const options = await connection.query(optionQuery)
            
            booking = x;
            booking.time_slots = options
            arr.push(booking)
        }));

        let countQuery = `select booking_requests.*,p.hourly_rate,p.title as post_title,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id where booking_requests.user_id = ${user_id}`
        if(status){
            if(status == 1){
                countQuery += ` and booking_requests.status = 1`
            }
            if(status == 2){
                countQuery += ` and booking_requests.status = 2`
            }
            else if(status == 3){
                countQuery += ` and booking_requests.status = 3`
            }
            else if(status == 4){
                countQuery += ` and booking_requests.status = 4`
            }
            else if(status == 5){
                countQuery += ` and booking_requests.status = 5`
            }
        }
        let count = await connection.query(countQuery)
        return {count:count.length,booking:arr}
    }
    static async addTimeSlot(booking_id,time_slot,lang){
        let query = `select * from booking_requests where id = ${booking_id}`
        const getBookingDetail = await connection.query(query)
        if(getBookingDetail.length > 0){
            let hourly_rate = getBookingDetail[0].hourly_rate
            if(time_slot){
                for (var j in time_slot) {
                    var totalHours = await this.calculateHours(time_slot[j].start_time,time_slot[j].end_time)
                    await connection.query('insert into booking_request_slots (booking_request_id,user_id,date,start_time,end_time,payment_status,hourly_rate,hours_difference) values (?,?,?,?,?,?,?,?)', [booking_id,getBookingDetail[0].user_id,time_slot[j].date,time_slot[j].start_time,time_slot[j].end_time,1,hourly_rate,totalHours])
                }
            }
            let getLangMsg = await Common.localization(lang)
            return ({ message: getLangMsg.request_send_successfully,booking:await this.getBookingDetaill(booking_id)})
        }
        else{
            throw("Booking not more exists")
        }
    }
    static async cancelTimeSlot(user_id,booking_id,time_slot_id,lang){
        var currentTime = await Common.getCurrentDate()
        let getLangMsg = await Common.localization(lang)
        let getBookingSlotDetail = await connection.query(`select * from booking_request_slots where id = ${time_slot_id}`)
        if(getBookingSlotDetail.length > 0){
            if(getBookingSlotDetail[0].start_log_time == null){
                if(getBookingSlotDetail[0].payment_status == 1){
                    let sqlQuery = 'update booking_request_slots set status=? where booking_request_id=? and id=?'
                    let params = [2,booking_id,time_slot_id]
                    await connection.query(sqlQuery, params)
                }
                else{
                    let getBookingDetail = await connection.query(`select * from booking_request_slots where user_id = ${user_id} and MONTH(refund_date)=MONTH(now())`)
                    let totalRefund = getBookingDetail.length
                    if(totalRefund < 3){
                        
                        var classTime = await Common.utcDate(getBookingSlotDetail[0].start_time,getBookingSlotDetail[0].date)
                        console.log("classTime"+classTime)
                        
                        let getPaymentId = await connection.query(`select * from payments where id = ${getBookingSlotDetail[0].payment_id}`)
                        
                        var date_ob = new Date();
                        var date = await Common.changeDateFormat(date_ob)
                        
                        var rate = getBookingSlotDetail[0].hourly_rate
                        let rateHrs = getBookingSlotDetail[0].hours_difference
                        let total_amount = parseInt(rate*rateHrs)
                        if(getPaymentId[0].currency != 'JPY'){
                            total_amount = total_amount*parseInt(100)
                        }
                        if(currentTime < classTime ){
                            const refund = await stripe.refunds.create({
                                charge: getPaymentId[0].txn_id,
                                amount :total_amount
                            });
                            await connection.query(`update booking_request_slots set refund_status=1,status=2,refund_date='${date}' where id=${time_slot_id}`)
                        }
                        else{
                            await connection.query(`update booking_request_slots set status=2 where id=${time_slot_id}`)
                        }
                    }
                    else{
                        await connection.query(`update booking_request_slots set status=2 where id=${time_slot_id}`)
                        return ({ message:getLangMsg.class_cancelled_successfully,error_message:"No refund more than 3",booking:await this.getBookingDetaill(booking_id)})
                    }
                }
                return ({ message:getLangMsg.class_cancelled_successfully,booking:await this.getBookingDetaill(booking_id)})
            }
            else{
                throw(getLangMsg.class_already_started)
            }
        }
        else{
            throw("Booking not more exists")
        }
        
    } 
    static async bookingDetail(booking_id,post_id,user_id,lang){
        let getLangMsg = await Common.localization(lang)
        let query = `select booking_requests.*,p.user_id as teacher_id,p.id as post_id,p.title as post_title,p.teaching_standard,p.subject,p.type as post_type,u.id as teacher_id,us.first_name as student_first_name,us.last_name as student_last_name,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join users as us on booking_requests.user_id = us.id where (p.user_id = ${user_id} or booking_requests.user_id = ${user_id})`
        if(post_id){
            query += ` and booking_requests.post_id = ${post_id} and booking_requests.user_id = ${user_id}`
        }
        if(booking_id){
            query += ` and booking_requests.id = ${booking_id}`
        }
        let bookingDetail = await connection.query(query)
        if(bookingDetail.length > 0){
            let accpetedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 2`
            const getAcceptedTimeSlots = await connection.query(accpetedTimeSlots)

            let paymentPendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 1`
            const getPaymentPendingTimeSlots = await connection.query(paymentPendingTimeSlots)

            let pendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 0`
            const getPendingTimeSlots = await connection.query(pendingTimeSlots)

            let cancelledTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (status = 2 or status = 4) and payment_status=2 and refund_status=1`
            const getCancelledTimeSlots = await connection.query(cancelledTimeSlots)

            let declinedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (((status = 2 or status = 4) and payment_status = 1) or ((status = 2 or status = 4) and payment_status=2 and refund_status=0))`
            const getDeclinedTimeslots = await connection.query(declinedTimeSlots)

            let completedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 3`
            const getCompletedTimeSlots = await connection.query(completedTimeSlots)

            let postLang = `select l.name,l.id from post_languages left join languages as l on post_languages.language_id=l.id where post_languages.post_id=?`
            const getPostLan = await connection.query(postLang, [post_id])

            let convId = ''
            let conId = `select * from message where booking_id=?`
            const conversation_id = await connection.query(conId, [booking_id]) 
            if(conversation_id.length != 0){
                convId = conversation_id[0].conversation_id
            }

            let bookingReviews = await Common.bookingReviews(booking_id)

            return {booking_detail:bookingDetail[0],accepted_time_slots:getAcceptedTimeSlots,payment_pending_timeslots:getPaymentPendingTimeSlots,pending_time_slots:getPendingTimeSlots,cancelled_time_slots:getCancelledTimeSlots,declined_time_slots:getDeclinedTimeslots,completed_time_slots:getCompletedTimeSlots,post_languages:getPostLan,conversation_id:convId,reviews:bookingReviews}
        }
        else{
            throw(getLangMsg.delete_booking)
        }
    }
    static async editMakeOffer(booking_id,hourly_rate,lang){
        let getLangMsg = await Common.localization(lang)
        
        let bookingQuery = `select * from booking_requests where id = ${booking_id}`
        const getBooking = await connection.query(bookingQuery)
        if(getBooking.length > 0){
            var getUserData = await Common.userData(getBooking[0].user_id)
            if(getUserData.is_user_verified == 1){
                if(getBooking[0].status == 1){
                    await Common.checkValidAmount(getBooking[0].currency,hourly_rate,lang)
                    let sqlQuery = 'update booking_requests set hourly_rate=? where id=?'
                    let params = [hourly_rate,booking_id]
                    await connection.query(sqlQuery, params)
        
                    let slotsSqlQuery = 'update booking_request_slots set hourly_rate=? where booking_request_id=?'
                    let slotsParams = [hourly_rate,booking_id]
                    await connection.query(slotsSqlQuery, slotsParams)
        
                    return ({ message: getLangMsg.hourly_rate_changed})
                }
                else{
                    throw(getLangMsg.error_changing_hourly_rate)
                }
            }
            else{
                throw(getLangMsg.verification_pending)
            }
        }
        else{
            throw("Booking not more exists")
        }
        
        
        
    }
    static async deleteBooking(booking_id,lang){
        let getLangMsg = await Common.localization(lang)
        await connection.query('delete from booking_requests where id = ? and (status = ? or status = ?) ', [booking_id,1,2])
        return ({ message: getLangMsg.delete_booking})
    }
    //payment module
    static async makePaymentAmount(booking_id){
        let accpetedTimeSlots = `select * from booking_request_slots where booking_request_id = ${booking_id} and status = 1 and payment_status = 1`
        const getAcceptedTimeSlots = await connection.query(accpetedTimeSlots)
        let total_amount = 0
        getAcceptedTimeSlots.forEach(async x=>{
            var rate = x.hourly_rate
            let rateHrs = x.hours_difference
            total_amount += rate*rateHrs
        })
        //let total_amount = getAcceptedTimeSlots[0].total_amount
        return ({ booking_amount: Number((total_amount).toFixed(2))})
    }
    static async makePayment(cardToken,booking_id,user_id,amount,chat_id,lang){
        
        let getLangMsg = await Common.localization(lang)
        let post = `select * from booking_requests where id = ${booking_id}`
        let getCurrency = await connection.query(post)
        if(getCurrency.length > 0){
            let currency = getCurrency[0].currency
            let user = `select * from users where id = ${user_id}`
            const getEmail = await connection.query(user)

            let postquery = `select * from posts where id = ${getCurrency[0].post_id}`
            let getPost = await connection.query(postquery)
            if(getPost.deleted_at == null){
                let accpetedTimeSlots = `select * from booking_request_slots where booking_request_id = ${booking_id} and status = 1 and payment_status = 1`
                const getAcceptedTimeSlots = await connection.query(accpetedTimeSlots)
                if(getAcceptedTimeSlots.length > 0){
                    let total_amount = 0
                    getAcceptedTimeSlots.forEach(async x=>{
                        var rate = x.hourly_rate
                        let rateHrs = x.hours_difference
                        total_amount += rate*rateHrs
                    })
                    
                    if(Number((total_amount).toFixed(2))!= amount){
                        throw(getLangMsg.invalid_amount)
                    }

                    if(currency != 'JPY'){
                        var getAmount = Number((amount).toFixed(2))*100
                    }
                    else{
                        var getAmount = parseInt(amount)
                    }
                    
                    const charge = await stripe.charges.create({
                        amount: getAmount,
                        currency: currency,
                        description: `Booking id : ${booking_id} Student Email : ${getEmail[0].email}`,
                        source: cardToken,
                    });
                    let randomString = await Common.generateRandomNumber(8)
                    let invoice_unique_id = 'INV-'+randomString

                    const payment = await connection.query('insert into payments (booking_id,txn_id,currency,amount,payment_status,invoice_id) values (?,?,?,?,?,?)', [booking_id,charge.id,currency,amount,2,invoice_unique_id])
                    if(payment.insertId){
                        var current_date = new Date()
                        var getDate = moment(current_date).format('YYYY-MM-DD hh:mm:ss');

                        await connection.query(`update booking_request_slots set payment_id=${payment.insertId},payment_status = 2 where booking_request_id = ${booking_id} and status = 1 and payment_status = 1`)

                        await connection.query(`update message set booking_type=14 where booking_id = ${booking_id} and booking_type = 1`)

                        await connection.query(`update booking_requests set status=4,updated_at='${getDate}' where id = ${booking_id}`)

                        let getChat = `select * from message where id=?`
                        let chatDetail = await connection.query(getChat, [chat_id])

                        //send email
                        //send email
                        var emailData = await this.getBookingDetaill(booking_id)
                        console.log(emailData)
                        let getUrl = process.env.WEBSITE_URL
                        let teacherData = await Common.userData(getPost[0].user_id)
                        let studentData = await Common.userData(user_id)

                        await Common.makePaymentByStudent(teacherData,emailData.booking_detail,currency,amount,getUrl,studentData.first_name)

                        await Common.makePaymentEmailToStudent(studentData,emailData.booking_detail,currency,amount,getUrl)
                        
                        return ({ message: getLangMsg.payment_done,chatDetail:chatDetail[0]})
                    }
                    else{
                        throw("Error while payment")
                    }
                }
                else{
                    throw("No pending classes exists")
                }

                
            }
            else{
                throw("Post not more exists")
            }
        }
        else{
            throw("Booking not more exists")
        }
        
    }
    static async getPayments(user_id,page,limit){
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let query = `select payments.*,b.id as booking_id,b.booking_unique_id,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.profile_image as teacher_profile_image,bu.first_name as student_first_name,bu.last_name as student_last_name,bu.profile_image as student_profile_image from payments left join booking_requests as b on payments.booking_id = b.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as bu on b.user_id = bu.id where payments.deleted_at is null and b.user_id = ${user_id} `
        query += ` order by payments.id DESC LIMIT ${limit} OFFSET ${offset}`
        let payments = await connection.query(query)
        return ({count:payments.length,payments:payments})
    }
    static async getPaymentDetail(payment_id){
        let query = `select payments.*,p.title as post_title,b.created_at as booking_date,b.booking_unique_id as booking_id,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.country as teacher_country,bu.first_name as student_first_name,bu.last_name as student_last_name,bu.country as student_country from payments left join booking_requests as b on payments.booking_id = b.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as bu on b.user_id = bu.id where payments.id = ${payment_id} `
        // let query = `select payments.*,p.title as post_title,b.created_at as booking_date,tu.location as teacher_location,tu.country as teacher_country,tu.state as teacher_state,tu.city as teacher_city,tu.zipcode as teacher_zip_code,bu.location as student_location,bu.country as student_country,bu.state as student_state,bu.city as student_city,bu.zipcode as student_zip_code from payments left join booking_requests as b on payments.booking_id = b.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as bu on b.user_id = bu.id where payments.id = ${payment_id} `
        let payments = await connection.query(query)
        let timeSlotQuery = `select booking_request_slots.*,p.title as post_title,b.created_at as booking_date,py.invoice_id,py.created_at as payment_date,tu.country as teacher_country,su.country as student_country from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id left join payments as py on booking_request_slots.payment_id = py.id left join posts as p on b.post_id = p.id left join users as tu on p.user_id = tu.id left join users as su on b.user_id = su.id where booking_request_slots.payment_id = ${payment_id} `
        let timeSlotDetail = await connection.query(timeSlotQuery)
        return ({payment_detail:payments[0],time_slot_detail:timeSlotDetail})
    }
    static async studentCalendar(month,year,user_id){
        let sqlQuery = 'select DATE(booking_request_slots.date) DateOnly from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id where YEAR(`date`) = ? AND MONTH(`date`) = ? and b.user_id = ? GROUP BY DateOnly ORDER BY DateOnly ASC'
        let params = [year,month,user_id]
        let getDates = await connection.query(sqlQuery, params)
        var arr = []
        await Promise.all(getDates.map(async (x) => {
            var request = {}
            let bookingDates = 'select booking_request_slots.booking_request_id,booking_request_slots.date,b.status,p.title as post_title from booking_request_slots left join booking_requests as b on booking_request_slots.booking_request_id = b.id left join posts as p on b.post_id = p.id where booking_request_slots.date = ? and b.user_id = ? Group BY booking_request_slots.booking_request_id'
            let datesParams = [x.DateOnly,user_id]
            const datesOptions = await connection.query(bookingDates,datesParams)
            request.date = x.DateOnly;
            request.classes = datesOptions
            
            await Promise.all(datesOptions.map(async (y) => {
                let classesQuery = 'select * from booking_request_slots where booking_request_slots.booking_request_id = ? and booking_request_slots.date = ?'
                let classParams = [y.booking_request_id,y.date]
                const options = await connection.query(classesQuery,classParams)
                
                y.slots = options
            }));
            
            
            arr.push(request)
        }));
        return arr
        
    }

    /*=======Complain module=======*/
    static async addTimeSlotComplain(time_slot_id,subject_id,description,lang){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = 'update booking_request_slots set complain_subject_id=?,complain_description=? where id=?'
        let params = [subject_id,description,time_slot_id]
        await connection.query(sqlQuery, params)
        return ({ message: getLangMsg.complain_registered})
    }
    static async addBookingComplain(booking_id,subject_id,description,lang,user_id){
        let getLangMsg = await Common.localization(lang)

        let sqlQuery = 'update booking_requests set complain_subject_id=?,complain_description=? where id=?'
        let params = [subject_id,description,booking_id]
        await connection.query(sqlQuery, params)
        return ({ message: getLangMsg.complain_registered,booking_detail:await this.getBookingDetaill(booking_id)})
    }

    /*======rating module========*/
    static async addRating(booking_id, from_user_id, communicate_ability,class_preparation,teacher_knowledge,tutor_easy,class_quality,lang){
        let getLangMsg = await Common.localization(lang)

        let booking = await connection.query(`select p.user_id from booking_requests left join posts as p on booking_requests.post_id = p.id where booking_requests.id = ${booking_id}`)
        let to_user_id = booking[0].user_id

        let bookingSlots = await connection.query(`select SUM(total_log_hour) as log_hours from booking_request_slots where booking_request_id = ${booking_id} GROUP BY booking_request_id`)
        let total_hours = bookingSlots[0].log_hours

        let sum = communicate_ability+class_preparation+teacher_knowledge+tutor_easy+class_quality

        let average = sum/5
        
        let sqlQuery = `insert into booking_reviews(booking_id,to_user_id,from_user_id,communicate_ability,class_preparation,class_quality,teacher_knowledge,tutor_easy,total_hours,rating) values(?,?,?,?,?,?,?,?,?,?)`
        let params = [booking_id,to_user_id,from_user_id,communicate_ability,class_preparation,class_quality,teacher_knowledge,tutor_easy,total_hours,average]
        let addReview = await connection.query(sqlQuery, params)

        return ({ message: getLangMsg.review_added,review: await Common.reviewById(addReview.insertId)})
    }
    static async editRating(rating_id,communicate_ability,class_preparation,teacher_knowledge,tutor_easy,class_quality,lang){
        let getLangMsg = await Common.localization(lang)

        let sum = communicate_ability+class_preparation+teacher_knowledge+tutor_easy+class_quality
        let average = sum/5

        let sqlQuery = 'update booking_reviews set communicate_ability=?,class_preparation=?,teacher_knowledge=?,tutor_easy=?,class_quality=?,rating=? where id=?'
        let params = [communicate_ability,class_preparation,teacher_knowledge,tutor_easy,class_quality,average,rating_id]
        await connection.query(sqlQuery, params)
        return ({ message: getLangMsg.review_updated,review: await Common.reviewById(rating_id)})
    }
    static async updateCurrency(from,to,rate){
        let sqlQuery = `insert into exchange_rates(from_currency,to_currency,rate) values(?,?,?)`
        let params = [from,to,rate]
        return await connection.query(sqlQuery, params)
    }
    static async completeBooking(booking_id,lang){
        let getLangMsg = await Common.localization(lang)
        var current_date = new Date()
        var getDate = moment(current_date).format('YYYY-MM-DD hh:mm:ss');

        let bookingQuery = `select p.user_id as teacher_id from booking_requests left join posts as p on booking_requests.post_id = p.id where booking_requests.id = ${booking_id}`
        const getBooking = await connection.query(bookingQuery)
        let teacher_id = getBooking[0].teacher_id

        let sqlQuery = 'update booking_requests set status=5,updated_at=? where id=?'
        let params = [getDate,booking_id]
        await connection.query(sqlQuery, params)

        await connection.query(`update booking_request_slots set status=2 where booking_request_id=${booking_id} and end_log_time is null`)

        let total_hours = await connection.query(`select Sum(total_log_hour) as total_hours from booking_request_slots where booking_request_id=${booking_id}`)

        let teacher_hours = await Common.userData(teacher_id)
        console.log("teacher_hours",teacher_hours)

        let getTotalHours = teacher_hours.total_hours+total_hours[0].total_hours
        
        let rank = 1

        if(getTotalHours > parseInt(1200)){
            rank = 2
        }
        if(getTotalHours > parseInt(2400)){
            rank = 3
        }
        if(getTotalHours > parseInt(4200)){
            rank = 4
        }
        if(getTotalHours > parseInt(6600)){
            rank = 5
        }
        if(getTotalHours > parseInt(9300)){
            rank = 6
        }

        // await connection.query(`update users set total_hours=${getTotalHours} , `rank` = ${rank} where 'id'=${teacher_id}`)

        let userSql = 'update users set total_hours=? , `rank` = ? where `id`=?'
        let userParam = [getTotalHours,rank,teacher_id]
        await connection.query(userSql, userParam)

        //Send email//
        // let subject = "Booking Completed"
        // let text = "Booking Completed"
        // await Common.bookingEmail(teacher_hours.email,subject,text)
        
        return ({message:getLangMsg.booking_completed,booking_detail:await this.getBookingDetaill(booking_id)})
    }
    static async getBookingDetaill(booking_id,post_id,user_id,lang){
        let query = `select booking_requests.*,p.user_id as teacher_id,p.id as post_id,p.title as post_title,p.teaching_standard,p.subject,p.type as post_type,u.id as teacher_id,us.first_name as student_first_name,us.last_name as student_last_name,u.first_name as teacher_first_name,u.last_name as teacher_last_name,u.profile_image as teacher_profile_image from booking_requests left join posts as p on booking_requests.post_id = p.id left join users as u on p.user_id = u.id left join users as us on booking_requests.user_id = us.id `
        if(post_id){
            query += ` and booking_requests.post_id = ${post_id} and booking_requests.user_id = ${user_id}`
        }
        if(booking_id){
            query += ` where booking_requests.id = ${booking_id}`
        }
        let bookingDetail = await connection.query(query)
        if(bookingDetail.length > 0){
            let accpetedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 2`
            const getAcceptedTimeSlots = await connection.query(accpetedTimeSlots)

            let paymentPendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 1 and payment_status = 1`
            const getPaymentPendingTimeSlots = await connection.query(paymentPendingTimeSlots)

            let pendingTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 0`
            const getPendingTimeSlots = await connection.query(pendingTimeSlots)

            let cancelledTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (status = 2 or status = 4) and payment_status=2 and refund_status=1`
            const getCancelledTimeSlots = await connection.query(cancelledTimeSlots)

            let declinedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and (((status = 2 or status = 4) and payment_status = 1) or ((status = 2 or status = 4) and payment_status=2 and refund_status=0))`
            const getDeclinedTimeslots = await connection.query(declinedTimeSlots)

            let completedTimeSlots = `select * from booking_request_slots where booking_request_id = ${bookingDetail[0].id} and status = 3`
            const getCompletedTimeSlots = await connection.query(completedTimeSlots)

            let postLang = `select l.name,l.id from post_languages left join languages as l on post_languages.language_id=l.id where post_languages.post_id=?`
            const getPostLan = await connection.query(postLang, [post_id])

            let convId = ''
            let conId = `select * from message where booking_id=?`
            const conversation_id = await connection.query(conId, [booking_id]) 
            if(conversation_id.length != 0){
                convId = conversation_id[0].conversation_id
            }

            let bookingReviews = await Common.bookingReviews(booking_id)

            return {booking_detail:bookingDetail[0],accepted_time_slots:getAcceptedTimeSlots,payment_pending_timeslots:getPaymentPendingTimeSlots,pending_time_slots:getPendingTimeSlots,cancelled_time_slots:getCancelledTimeSlots,declined_time_slots:getDeclinedTimeslots,completed_time_slots:getCompletedTimeSlots,post_languages:getPostLan,conversation_id:convId,reviews:bookingReviews}
        }
        
    }
    
}
module.exports = StudentService;