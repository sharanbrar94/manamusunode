const { TestScheduler } = require('jest');
const lib = require('../lib');

describe('getProduct', () => {
    it('should return the product with the given id', () => {
        const result = lib.getProduct(1);

        expect(result).toMatchObject({ id: 1, price: 10 });
        expect(result).toHaveProperty('id', 1)
    })
})

describe('registerUser', () => {

    it('should throw err if username is falsy', () => {
        const args = [null, undefined, NaN, '', 0, false];
        args.forEach(a => {
            expect(() => {
                lib.registerUser(a)
            }).toThrow();
        })
    })
    it('should return user object if valid username is passed', () => {
        const result = lib.registerUser('mosh');
        expect(result).toMatchObject({ username: 'mosh' });
        expect(result.id).toBeGreaterThan(0);
    })
})

describe('getCurrencies',()=>{
    it('should return the currencies',()=>{
        const result= lib.getCurrencies();
        expect(result).toEqual(expect.arrayContaining(['EUR','AUD','USD']))
    })
})